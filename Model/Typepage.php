<?php

/**
 * Class Group
 */
class Typepage extends AppModel
{


    /**
     * The validates
     *
     * @var array $validate
     */
	public $validate = array(
		'name' => array(
			'rule' 		=> 'notEmpty', 
			'required' 	=> true, 
			'message' 	=> 'Vous devez entrer un nom de template'
		),
       'code' => array(
            'rule'      => 'notEmpty', 
            'required'  => true, 
            'message'   => 'Vous devez entrer un code de template'
        )
	);


}