-- MySQL dump 10.13  Distrib 5.6.42, for Linux (x86_64)
--
-- Host: localhost    Database: ddhfrance-com
-- ------------------------------------------------------
-- Server version	5.6.42-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `acos`
--

DROP TABLE IF EXISTS `acos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT '',
  `foreign_key` int(10) unsigned DEFAULT NULL,
  `alias` varchar(255) DEFAULT '',
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acos`
--

LOCK TABLES `acos` WRITE;
/*!40000 ALTER TABLE `acos` DISABLE KEYS */;
/*!40000 ALTER TABLE `acos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `actualites`
--

DROP TABLE IF EXISTS `actualites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `actualites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content_type_id` varchar(128) NOT NULL,
  `etat_id` varchar(128) NOT NULL,
  `page_id` int(11) NOT NULL,
  `acces_membre` tinyint(1) DEFAULT '0',
  `type` varchar(128) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  `h1` varchar(256) NOT NULL,
  `sous_titre` varchar(256) DEFAULT NULL,
  `vignette` text NOT NULL,
  `url` varchar(256) NOT NULL,
  `url_r` varchar(256) NOT NULL,
  `meta_description` text NOT NULL,
  `meta_title` varchar(256) NOT NULL,
  `meta_keywords` text NOT NULL,
  `meta_robots` varchar(128) NOT NULL,
  `resume` text NOT NULL,
  `body` text NOT NULL,
  `publication_date` int(11) DEFAULT NULL,
  `publication_date_fin` int(11) DEFAULT NULL,
  `agenda_id` int(11) DEFAULT NULL,
  `even_date_deb` datetime NOT NULL,
  `even_date_fin` datetime NOT NULL,
  `all_day` tinyint(1) NOT NULL DEFAULT '0',
  `suscribe` tinyint(1) NOT NULL DEFAULT '0',
  `suscribe_nbr` int(11) NOT NULL DEFAULT '0',
  `color` varchar(128) DEFAULT NULL,
  `adresse` text NOT NULL,
  `recurence` varchar(128) NOT NULL,
  `updated` datetime NOT NULL,
  `created` datetime NOT NULL,
  `push_accueil` tinyint(1) NOT NULL,
  `auteur_id` int(11) DEFAULT NULL,
  `from` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `actualites`
--

LOCK TABLES `actualites` WRITE;
/*!40000 ALTER TABLE `actualites` DISABLE KEYS */;
/*!40000 ALTER TABLE `actualites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `adresses`
--

DROP TABLE IF EXISTS `adresses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adresses` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `civilite_id` int(11) NOT NULL,
  `nom` varchar(128) NOT NULL,
  `prenom` varchar(128) NOT NULL,
  `nom_entreprise` varchar(256) NOT NULL,
  `adresse_1` varchar(256) NOT NULL,
  `adresse_2` varchar(256) NOT NULL,
  `cp` varchar(16) NOT NULL,
  `ville` varchar(256) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `mobile` varchar(32) DEFAULT NULL,
  `country_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `adresses`
--

LOCK TABLES `adresses` WRITE;
/*!40000 ALTER TABLE `adresses` DISABLE KEYS */;
/*!40000 ALTER TABLE `adresses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `agendas`
--

DROP TABLE IF EXISTS `agendas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agendas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `description` text NOT NULL,
  `page_id` int(11) NOT NULL,
  `last_even_id` int(11) DEFAULT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `agendas`
--

LOCK TABLES `agendas` WRITE;
/*!40000 ALTER TABLE `agendas` DISABLE KEYS */;
/*!40000 ALTER TABLE `agendas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aros`
--

DROP TABLE IF EXISTS `aros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aros` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT '',
  `foreign_key` int(10) unsigned DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aros`
--

LOCK TABLES `aros` WRITE;
/*!40000 ALTER TABLE `aros` DISABLE KEYS */;
/*!40000 ALTER TABLE `aros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aros_acos`
--

DROP TABLE IF EXISTS `aros_acos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aros_acos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `aro_id` int(10) unsigned NOT NULL,
  `aco_id` int(10) unsigned NOT NULL,
  `_create` char(2) NOT NULL DEFAULT '0',
  `_read` char(2) NOT NULL DEFAULT '0',
  `_update` char(2) NOT NULL DEFAULT '0',
  `_delete` char(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aros_acos`
--

LOCK TABLES `aros_acos` WRITE;
/*!40000 ALTER TABLE `aros_acos` DISABLE KEYS */;
/*!40000 ALTER TABLE `aros_acos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `attribute_families`
--

DROP TABLE IF EXISTS `attribute_families`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attribute_families` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `lft` int(11) NOT NULL,
  `rght` int(11) NOT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `name_admin` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attribute_families`
--

LOCK TABLES `attribute_families` WRITE;
/*!40000 ALTER TABLE `attribute_families` DISABLE KEYS */;
/*!40000 ALTER TABLE `attribute_families` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `attributes`
--

DROP TABLE IF EXISTS `attributes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attributes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_family_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `lft` int(11) NOT NULL,
  `rght` int(11) NOT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attributes`
--

LOCK TABLES `attributes` WRITE;
/*!40000 ALTER TABLE `attributes` DISABLE KEYS */;
/*!40000 ALTER TABLE `attributes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content_assocs`
--

DROP TABLE IF EXISTS `content_assocs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_assocs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model` varchar(128) DEFAULT NULL,
  `model_id` int(11) NOT NULL,
  `model_asso` varchar(128) DEFAULT NULL,
  `model_asso_id` int(11) NOT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_assocs`
--

LOCK TABLES `content_assocs` WRITE;
/*!40000 ALTER TABLE `content_assocs` DISABLE KEYS */;
/*!40000 ALTER TABLE `content_assocs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content_modules`
--

DROP TABLE IF EXISTS `content_modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model` varchar(128) NOT NULL,
  `model_id` int(11) NOT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `module` varchar(128) NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=366 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_modules`
--

LOCK TABLES `content_modules` WRITE;
/*!40000 ALTER TABLE `content_modules` DISABLE KEYS */;
INSERT INTO `content_modules` VALUES (327,'Page',10,0,'txtimg','{\"title\":\"\",\"txt\":\"<h3>M\\u00e9rule (Serpula lacrymans)<\\/h3><p> Les zones contamin\\u00e9es sont inject\\u00e9es et pulv\\u00e9ris\\u00e9es. <br> Toutes les pr\\u00e9cautions, pr\\u00e9parations et \\u00e9vacuations des bois contamin\\u00e9s seront effectu\\u00e9es conform\\u00e9ment aux r\\u00e9glementations de travail et d\'hygi\\u00e8ne en vigueur. Traitement contre les champignons lignivores (m\\u00e9rule etc...) ou note d\'information technique 180 du C.S.T.C. <\\/p>\",\"img\":{\"File\":{\"id\":\"46\",\"name\":\"certificat\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"png\",\"type\":\"image\",\"slug\":\"certificat568107244572e\",\"nom_fichier\":\"certificat568107244572e.png\",\"poids\":\"56\",\"dimenssions\":{\"width\":422,\"height\":210,\"reco\":\"normal\"},\"file_folder_id\":null,\"url\":\"\\/files\\/2015\\/12\\/certificat568107244572e.png\",\"created\":\"2015-12-28 10:55:48\",\"updated\":\"2015-12-28 10:55:48\"},\"Folder\":{\"id\":null,\"name\":null,\"parent_id\":null,\"lft\":null,\"rght\":null},\"Extention\":{\"id\":\"png\",\"icone\":\"picture\",\"type\":\"image\"}},\"params\":{\"prop\":\"img-right\"},\"position\":2}'),(328,'Page',10,1,'galerie','{\"title\":\"\",\"files\":[{\"File\":{\"id\":\"24\",\"name\":\"Traitement du bois et des ma\\u00e7onneries\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"png\",\"type\":\"image\",\"slug\":\"champignon_556614991b6e10\",\"nom_fichier\":\"champignon_556614991b6e10.png\",\"poids\":\"151\",\"dimenssions\":{\"width\":300,\"height\":225,\"reco\":\"normal\"},\"file_folder_id\":\"4\",\"url\":\"\\/files\\/2015\\/12\\/champignon_556614991b6e10.png\",\"created\":\"2015-12-04 09:06:41\",\"updated\":\"2015-12-04 09:06:41\"},\"Folder\":{\"id\":\"4\",\"name\":\"galerie humidit\\u00e9\",\"parent_id\":null,\"lft\":\"7\",\"rght\":\"8\"},\"Extention\":{\"id\":\"png\",\"icone\":\"picture\",\"type\":\"image\"}},{\"File\":{\"id\":\"22\",\"name\":\"Traitement du bois et des ma\\u00e7onneries\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"png\",\"type\":\"image\",\"slug\":\"champignon_35661498dbd4e5\",\"nom_fichier\":\"champignon_35661498dbd4e5.png\",\"poids\":\"139\",\"dimenssions\":{\"width\":300,\"height\":225,\"reco\":\"normal\"},\"file_folder_id\":\"4\",\"url\":\"\\/files\\/2015\\/12\\/champignon_35661498dbd4e5.png\",\"created\":\"2015-12-04 09:06:37\",\"updated\":\"2015-12-04 09:06:37\"},\"Folder\":{\"id\":\"4\",\"name\":\"galerie humidit\\u00e9\",\"parent_id\":null,\"lft\":\"7\",\"rght\":\"8\"},\"Extention\":{\"id\":\"png\",\"icone\":\"picture\",\"type\":\"image\"}},{\"File\":{\"id\":\"23\",\"name\":\"Traitement du bois et des ma\\u00e7onneries\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"png\",\"type\":\"image\",\"slug\":\"champignon_45661498ff35d3\",\"nom_fichier\":\"champignon_45661498ff35d3.png\",\"poids\":\"143\",\"dimenssions\":{\"width\":300,\"height\":225,\"reco\":\"normal\"},\"file_folder_id\":\"4\",\"url\":\"\\/files\\/2015\\/12\\/champignon_45661498ff35d3.png\",\"created\":\"2015-12-04 09:06:39\",\"updated\":\"2015-12-04 09:06:39\"},\"Folder\":{\"id\":\"4\",\"name\":\"galerie humidit\\u00e9\",\"parent_id\":null,\"lft\":\"7\",\"rght\":\"8\"},\"Extention\":{\"id\":\"png\",\"icone\":\"picture\",\"type\":\"image\"}},{\"File\":{\"id\":\"20\",\"name\":\"Traitement du bois et des ma\\u00e7onneries\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"png\",\"type\":\"image\",\"slug\":\"champignon_15661498ae7c74\",\"nom_fichier\":\"champignon_15661498ae7c74.png\",\"poids\":\"142\",\"dimenssions\":{\"width\":300,\"height\":225,\"reco\":\"normal\"},\"file_folder_id\":\"4\",\"url\":\"\\/files\\/2015\\/12\\/champignon_15661498ae7c74.png\",\"created\":\"2015-12-04 09:06:35\",\"updated\":\"2015-12-04 09:06:35\"},\"Folder\":{\"id\":\"4\",\"name\":\"galerie humidit\\u00e9\",\"parent_id\":null,\"lft\":\"7\",\"rght\":\"8\"},\"Extention\":{\"id\":\"png\",\"icone\":\"picture\",\"type\":\"image\"}},{\"File\":{\"id\":\"21\",\"name\":\"Traitement du bois et des ma\\u00e7onneries\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"png\",\"type\":\"image\",\"slug\":\"champignon_25661498c65256\",\"nom_fichier\":\"champignon_25661498c65256.png\",\"poids\":\"128\",\"dimenssions\":{\"width\":300,\"height\":225,\"reco\":\"normal\"},\"file_folder_id\":\"4\",\"url\":\"\\/files\\/2015\\/12\\/champignon_25661498c65256.png\",\"created\":\"2015-12-04 09:06:36\",\"updated\":\"2015-12-04 09:06:36\"},\"Folder\":{\"id\":\"4\",\"name\":\"galerie humidit\\u00e9\",\"parent_id\":null,\"lft\":\"7\",\"rght\":\"8\"},\"Extention\":{\"id\":\"png\",\"icone\":\"picture\",\"type\":\"image\"}}],\"position\":1}'),(340,'Page',1,0,'galerie','{\"title\":\"\",\"files\":[{\"File\":{\"id\":\"41\",\"name\":\"Rats \\/ Souris\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"fotolia_74612505_m5680fed70595b\",\"nom_fichier\":\"fotolia_74612505_m5680fed70595b.jpg\",\"poids\":\"363\",\"dimenssions\":{\"width\":1000,\"height\":562,\"reco\":\"normal\"},\"file_folder_id\":\"1\",\"url\":\"\\/files\\/2015\\/12\\/fotolia_74612505_m5680fed70595b.jpg\",\"created\":\"2015-12-28 10:20:23\",\"updated\":\"2015-12-28 10:20:23\"},\"Folder\":{\"id\":\"1\",\"name\":\"diapo accueil\",\"parent_id\":null,\"lft\":\"1\",\"rght\":\"2\"},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},{\"File\":{\"id\":\"42\",\"name\":\"Fourmis\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"fotolia_92281529_m5680ff7a0ef2d\",\"nom_fichier\":\"fotolia_92281529_m5680ff7a0ef2d.jpg\",\"poids\":\"168\",\"dimenssions\":{\"width\":1000,\"height\":562,\"reco\":\"normal\"},\"file_folder_id\":\"1\",\"url\":\"\\/files\\/2015\\/12\\/fotolia_92281529_m5680ff7a0ef2d.jpg\",\"created\":\"2015-12-28 10:23:06\",\"updated\":\"2015-12-28 10:23:06\"},\"Folder\":{\"id\":\"1\",\"name\":\"diapo accueil\",\"parent_id\":null,\"lft\":\"1\",\"rght\":\"2\"},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},{\"File\":{\"id\":\"43\",\"name\":\"Cafards \\/ Blattes\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"fotolia_85997612_m5681001c5c410\",\"nom_fichier\":\"fotolia_85997612_m5681001c5c410.jpg\",\"poids\":\"389\",\"dimenssions\":{\"width\":1000,\"height\":562,\"reco\":\"normal\"},\"file_folder_id\":\"1\",\"url\":\"\\/files\\/2015\\/12\\/fotolia_85997612_m5681001c5c410.jpg\",\"created\":\"2015-12-28 10:25:48\",\"updated\":\"2015-12-28 10:25:48\"},\"Folder\":{\"id\":\"1\",\"name\":\"diapo accueil\",\"parent_id\":null,\"lft\":\"1\",\"rght\":\"2\"},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},{\"File\":{\"id\":\"47\",\"name\":\"Nid de gu\\u00eapes\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"abeilles56a107061afe2\",\"nom_fichier\":\"abeilles56a107061afe2.jpg\",\"poids\":\"308\",\"dimenssions\":{\"width\":1000,\"height\":562,\"reco\":\"normal\"},\"file_folder_id\":\"1\",\"url\":\"\\/files\\/2016\\/01\\/abeilles56a107061afe2.jpg\",\"created\":\"2016-01-21 17:27:50\",\"updated\":\"2016-01-21 17:27:50\"},\"Folder\":{\"id\":\"1\",\"name\":\"diapo accueil\",\"parent_id\":null,\"lft\":\"1\",\"rght\":\"2\"},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},{\"File\":{\"id\":\"54\",\"name\":\"Notre flotte de v\\u00e9hicules\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"flotte56a2560b6bb36\",\"nom_fichier\":\"flotte56a2560b6bb36.jpg\",\"poids\":\"328\",\"dimenssions\":{\"width\":1000,\"height\":562,\"reco\":\"normal\"},\"file_folder_id\":null,\"url\":\"\\/files\\/2016\\/01\\/flotte56a2560b6bb36.jpg\",\"created\":\"2016-01-22 17:17:15\",\"updated\":\"2016-01-22 17:17:15\"},\"Folder\":{\"id\":null,\"name\":null,\"parent_id\":null,\"lft\":null,\"rght\":null},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}}],\"position\":1}'),(341,'Page',4,0,'txtimg','{\"title\":\"\",\"txt\":\"<h2>D\\u00e9ratisation :  rats - souris...<\\/h2>\\n<p>\\n\\t Traitements tous locaux, tous lieux.\\n<\\/p>\\n<p>\\n\\t Utilisation de produits RODONTICIDES homologu\\u00e9s pour \\u00e9liminer rats, souris, ...\\n<\\/p>\\n<p>\\n\\t <strong>DDH<\\/strong> met au service de ses clients une large gamme de produits comportant des mati\\u00e8res actives diff\\u00e9rentes mises au point en laboratoire.<br>\\n\\t Cette diversit\\u00e9 permet d\'\\u00e9viter tout ph\\u00e9nom\\u00e8ne d\'accoutumance.\\n<\\/p>\\n<p>\\n\\tNos assistantes sont \\u00e0 votre service pour vous expliquer les proc\\u00e9dures de traitements et les d\\u00e9lais d\'interventions.\\n<\\/p>\\n<p style=\\\"text-align: center;\\\">\\n\\t<strong><span style=\\\"color:#ff0000;\\\">Traitements rapides et garantis.<\\/span><\\/strong><br>\\n<\\/p>\",\"img\":{\"File\":{\"id\":\"45\",\"name\":\"Fotolia_74612505_M\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"fotolia_74612505_m56810168ab83d\",\"nom_fichier\":\"fotolia_74612505_m56810168ab83d.jpg\",\"poids\":\"363\",\"dimenssions\":{\"width\":1000,\"height\":562,\"reco\":\"normal\"},\"file_folder_id\":null,\"url\":\"\\/files\\/2015\\/12\\/fotolia_74612505_m56810168ab83d.jpg\",\"created\":\"2015-12-28 10:31:20\",\"updated\":\"2015-12-28 10:31:20\"},\"Folder\":{\"id\":null,\"name\":null,\"parent_id\":null,\"lft\":null,\"rght\":null},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},\"params\":{\"prop\":\"img-right\"},\"position\":1}'),(342,'Page',4,1,'txtimg','{\"title\":\"\",\"txt\":\"<h2>D\\u00e9sinsectisation :  blattes (cafards) - fourmis - moustiques - puces<br>\\n Nids de gu\\u00e9pes<\\/h2><p>\\n\\t Chaque insecte se traite de fa\\u00e7on diff\\u00e9rente.<br>\\n\\t <br>\\n\\t <strong>DDH <\\/strong>identifie le genre et vous propose la solution la plus efficace, tout en utilisant une m\\u00e9thode adapt\\u00e9e.<br>\\n\\t <br>\\n\\t <strong>Destruction de nids de gu\\u00eapes<\\/strong> sans d\\u00e9g\\u00e2ts, produit inodore et non toxique<br>\\n\\t <br>\\n\\t <strong>Traitement en pulv\\u00e9risation et micro-n\\u00e9bulisation :<\\/strong><br>\\n\\t Traitement des punaises, fourmis, araign\\u00e9es et autres insectes rampants et volants.<br>\\n\\t <br>\\n\\t <strong>Traitement au gel alimentaire :<\\/strong><br>\\n\\t Traitement des blattes. Non toxique et inodore, ce produit est le gel n\\u00b01 mondial pour la lutte contre les blattes (cafards).<br>\\n\\t Ce produit est r\\u00e9serv\\u00e9 aux professionnels de l\'hygi\\u00e8ne publique.\\n<\\/p>\",\"img\":{\"File\":{\"id\":\"57\",\"name\":\"nid et blatte\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"nid_et_blatte56a257232857b\",\"nom_fichier\":\"nid_et_blatte56a257232857b.jpg\",\"poids\":\"791\",\"dimenssions\":{\"width\":1000,\"height\":1201,\"reco\":\"trop grande\"},\"file_folder_id\":null,\"url\":\"\\/files\\/2016\\/01\\/nid_et_blatte56a257232857b.jpg\",\"created\":\"2016-01-22 17:21:55\",\"updated\":\"2016-01-22 17:21:55\"},\"Folder\":{\"id\":null,\"name\":null,\"parent_id\":null,\"lft\":null,\"rght\":null},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},\"params\":{\"prop\":\"img-left\"},\"position\":2}'),(343,'Page',4,2,'txtimg','{\"title\":\"\",\"txt\":\"<h2>D\\u00e9sinfection : Nos diff\\u00e9rents traitements<\\/h2> <p> <strong>Traitement en micro-n\\u00e9bulisation :<\\/strong><\\/p><p> -Traitement acaricide, fongicide, virucide, anti-microbien<\\/p><p> -D\\u00e9sodorisation<\\/p>\",\"img\":{\"File\":{\"id\":\"46\",\"name\":\"certificat\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"png\",\"type\":\"image\",\"slug\":\"certificat568107244572e\",\"nom_fichier\":\"certificat568107244572e.png\",\"poids\":\"56\",\"dimenssions\":{\"width\":422,\"height\":210,\"reco\":\"normal\"},\"file_folder_id\":null,\"url\":\"\\/files\\/2015\\/12\\/certificat568107244572e.png\",\"created\":\"2015-12-28 10:55:48\",\"updated\":\"2015-12-28 10:55:48\"},\"Folder\":{\"id\":null,\"name\":null,\"parent_id\":null,\"lft\":null,\"rght\":null},\"Extention\":{\"id\":\"png\",\"icone\":\"picture\",\"type\":\"image\"}},\"params\":{\"prop\":\"img-right\"},\"position\":6}'),(344,'Page',4,3,'galerie','{\"title\":\"\",\"files\":[{\"File\":{\"id\":\"19\",\"name\":\"Les 3D\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"pb0901495660751917000\",\"nom_fichier\":\"pb0901495660751917000.jpg\",\"poids\":\"65\",\"dimenssions\":{\"width\":300,\"height\":225,\"reco\":\"normal\"},\"file_folder_id\":\"3\",\"url\":\"\\/files\\/2015\\/12\\/pb0901495660751917000.jpg\",\"created\":\"2015-12-03 18:00:09\",\"updated\":\"2015-12-03 18:00:09\"},\"Folder\":{\"id\":\"3\",\"name\":\"galerie 3d\",\"parent_id\":null,\"lft\":\"5\",\"rght\":\"6\"},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},{\"File\":{\"id\":\"18\",\"name\":\"Les 3D\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"pb07014556607516ab040\",\"nom_fichier\":\"pb07014556607516ab040.jpg\",\"poids\":\"55\",\"dimenssions\":{\"width\":300,\"height\":225,\"reco\":\"normal\"},\"file_folder_id\":\"3\",\"url\":\"\\/files\\/2015\\/12\\/pb07014556607516ab040.jpg\",\"created\":\"2015-12-03 18:00:06\",\"updated\":\"2015-12-03 18:00:06\"},\"Folder\":{\"id\":\"3\",\"name\":\"galerie 3d\",\"parent_id\":null,\"lft\":\"5\",\"rght\":\"6\"},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},{\"File\":{\"id\":\"17\",\"name\":\"Les 3D\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"pb070142566075137f7ca\",\"nom_fichier\":\"pb070142566075137f7ca.jpg\",\"poids\":\"54\",\"dimenssions\":{\"width\":300,\"height\":225,\"reco\":\"normal\"},\"file_folder_id\":\"3\",\"url\":\"\\/files\\/2015\\/12\\/pb070142566075137f7ca.jpg\",\"created\":\"2015-12-03 18:00:03\",\"updated\":\"2015-12-03 18:00:03\"},\"Folder\":{\"id\":\"3\",\"name\":\"galerie 3d\",\"parent_id\":null,\"lft\":\"5\",\"rght\":\"6\"},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},{\"File\":{\"id\":\"15\",\"name\":\"Les 3D\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"pb1401645660750fc532d\",\"nom_fichier\":\"pb1401645660750fc532d.jpg\",\"poids\":\"77\",\"dimenssions\":{\"width\":300,\"height\":225,\"reco\":\"normal\"},\"file_folder_id\":\"3\",\"url\":\"\\/files\\/2015\\/12\\/pb1401645660750fc532d.jpg\",\"created\":\"2015-12-03 17:59:59\",\"updated\":\"2015-12-03 17:59:59\"},\"Folder\":{\"id\":\"3\",\"name\":\"galerie 3d\",\"parent_id\":null,\"lft\":\"5\",\"rght\":\"6\"},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},{\"File\":{\"id\":\"14\",\"name\":\"Les 3D\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"pb1401635660750d4a455\",\"nom_fichier\":\"pb1401635660750d4a455.jpg\",\"poids\":\"84\",\"dimenssions\":{\"width\":300,\"height\":225,\"reco\":\"normal\"},\"file_folder_id\":\"3\",\"url\":\"\\/files\\/2015\\/12\\/pb1401635660750d4a455.jpg\",\"created\":\"2015-12-03 17:59:57\",\"updated\":\"2015-12-03 17:59:57\"},\"Folder\":{\"id\":\"3\",\"name\":\"galerie 3d\",\"parent_id\":null,\"lft\":\"5\",\"rght\":\"6\"},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},{\"File\":{\"id\":\"56\",\"name\":\"Les 3D\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"3d_copie56a2568e2c0fb\",\"nom_fichier\":\"3d_copie56a2568e2c0fb.jpg\",\"poids\":\"76\",\"dimenssions\":{\"width\":410,\"height\":307,\"reco\":\"normal\"},\"file_folder_id\":\"3\",\"url\":\"\\/files\\/2016\\/01\\/3d_copie56a2568e2c0fb.jpg\",\"created\":\"2016-01-22 17:19:26\",\"updated\":\"2016-01-22 17:19:26\"},\"Folder\":{\"id\":\"3\",\"name\":\"galerie 3d\",\"parent_id\":null,\"lft\":\"5\",\"rght\":\"6\"},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}}],\"position\":4}'),(345,'Page',4,4,'txt','{\"title\":\"\",\"txt\":\"<p>L\'\\u00e9quipe de <strong>DDH<\\/strong> situ\\u00e9e \\u00e0 <strong>TOURCOING<\\/strong> dans <strong>le Nord<\\/strong> (<strong>59<\\/strong>) r\\u00e9alise des devis gratuits et personnalis\\u00e9s pour toujours mieux vous servir.<br> Pour en conna\\u00eetre d\'avantage sur les services disponibles , n\'h\\u00e9sitez pas \\u00e0 nous <strong>contacter par t\\u00e9l\\u00e9phone<\\/strong> au <span style=\\\"font-size:21px;\\\">03 20 26 40 51 <\\/span>ou gr\\u00e2ce au <strong><a href=\\\"\\/contact\\/p-3\\\">formulaire de contact<\\/a><\\/strong>.<\\/p>\",\"position\":5}'),(346,'Page',5,0,'txtimg','{\"title\":\"\",\"txt\":\"<h2>Traitement des remont\\u00e9es capillaires :<\\/h2> <p> Qu\'est ce qu\'une remont\\u00e9e capillaire?<br> <br> C\'est l\'humidit\\u00e9 ascensionnelle provenant du sol et remontant dans le r\\u00e9seau capillaire des mat\\u00e9riaux 24 heures sur 24 - 365 jours par an.<br> <br> Elle appara\\u00eet au niveau des bas de murs de l\'ensemble du b\\u00e2timent, murs de fa\\u00e7ades, murs de refend, cloisons, sol int\\u00e9rieur et ext\\u00e9rieur.<\\/p><p>Le traitement consiste en l\'injection apr\\u00e8s d\\u00e9cr\\u00e9pissage et percement du mur, d\'une r\\u00e9sine qui va recr\\u00e9er la base \\u00e9tanche du mur.<span><\\/span><br><\\/p><br>\",\"img\":{\"File\":{\"id\":\"26\",\"name\":\"shutterstock_10211947\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"shutterstock_1021194756614a3f7f248\",\"nom_fichier\":\"shutterstock_1021194756614a3f7f248.jpg\",\"poids\":\"1063\",\"dimenssions\":{\"width\":1000,\"height\":800,\"reco\":\"normal\"},\"file_folder_id\":null,\"url\":\"\\/files\\/2015\\/12\\/shutterstock_1021194756614a3f7f248.jpg\",\"created\":\"2015-12-04 09:09:35\",\"updated\":\"2015-12-04 09:09:35\"},\"Folder\":{\"id\":null,\"name\":null,\"parent_id\":null,\"lft\":null,\"rght\":null},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},\"params\":{\"prop\":\"img-right\"},\"position\":2}'),(347,'Page',5,1,'txt','{\"title\":\"\",\"txt\":\"<p>L\'\\u00e9quipe de <strong>DDH<\\/strong> situ\\u00e9e \\u00e0 <strong>TOURCOING<\\/strong> dans <strong>le Nord<\\/strong> (<strong>59<\\/strong>) r\\u00e9alise des devis gratuits et personnalis\\u00e9s pour toujours mieux vous servir.<br> Pour plus d\'informations sur nos prestations, n\'h\\u00e9sitez pas \\u00e0 nous <strong>joindre par t\\u00e9l\\u00e9phone<\\/strong> au <span style=\\\"font-size:21px;\\\">03 20 26 40 51 <\\/span><span><\\/span>ou gr\\u00e2ce au <strong><a href=\\\"\\/contact\\/p-3\\\">formulaire de contact<\\/a><\\/strong>.<\\/p>\",\"position\":3}'),(348,'Page',6,0,'txt','{\"title\":\"\",\"txt\":\"<h2> Les \\u00e9conomies<\\/h2>\\n<p>\\n\\t Pendant les p\\u00e9riodes froides, une ventilation mal r\\u00e9gl\\u00e9e ou mal entretenue contribue \\u00e0 augmenter la d\\u00e9perdition thermique.\\n<\\/p>\\n<h2> La salubrit\\u00e9<\\/h2>\\n<p>\\n\\t Une installation mal r\\u00e9gl\\u00e9e va entra\\u00eener des probl\\u00e8mes au niveau du taux d\'humidit\\u00e9, favoriser le d\\u00e9veloppement des moisissures et contribuer \\u00e0 la d\\u00e9gradation du logement.\\n<\\/p>\\n<h2> L\'indisposition<\\/h2>\\n<p>\\n\\t Mal r\\u00e9gl\\u00e9s, les bouches et les extracteurs deviennent bruyants et sont souvent condamn\\u00e9s.\\n<\\/p>\\n<h3> Les avantages procur\\u00e9s par le bon entretien d\'une installation :<\\/h3>\\n<div id=\\\"vert\\\">\\n<ul>\\n\\t<li>Vous assurez la p\\u00e9rennit\\u00e9 et la rentabilit\\u00e9 de votre investissement initial<\\/li>\\n\\t<li>Vous assurez une meilleure qualit\\u00e9 de l\'air (lutte contre les allergies)<\\/li>\\n\\t<li>Vous luttez contre les d\\u00e9sordres et d\\u00e9sagr\\u00e9ments de l\'humidit\\u00e9 (d\\u00e9collement du papier peint, moisissures, cloquage des peintures\\u2026)<\\/li>\\n\\t<li>Vous luttez contre le d\\u00e9veloppement des acariens et des bact\\u00e9ries.<\\/li>\\n\\t<li>Vous contr\\u00f4lez par notre interm\\u00e9diaire la conformit\\u00e9 des installations (exemple : hottes branch\\u00e9es sur le syst\\u00e8me VMC).<\\/li>\\n<\\/ul><\\/div>\",\"position\":4}'),(349,'Page',6,1,'galerie','{\"title\":\"\",\"files\":[{\"File\":{\"id\":\"50\",\"name\":\"VMC 3\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"vmc_356a253806ac9e\",\"nom_fichier\":\"vmc_356a253806ac9e.jpg\",\"poids\":\"1996\",\"dimenssions\":{\"width\":2560,\"height\":1920,\"reco\":\"trop grande\"},\"file_folder_id\":\"5\",\"url\":\"\\/files\\/2016\\/01\\/vmc_356a253806ac9e.jpg\",\"created\":\"2016-01-22 17:06:24\",\"updated\":\"2016-01-22 17:06:24\"},\"Folder\":{\"id\":\"5\",\"name\":\"galerie VMC\",\"parent_id\":null,\"lft\":\"9\",\"rght\":\"10\"},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},{\"File\":{\"id\":\"49\",\"name\":\"VMC 2\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"vmc_256a253734b5f8\",\"nom_fichier\":\"vmc_256a253734b5f8.jpg\",\"poids\":\"137\",\"dimenssions\":{\"width\":640,\"height\":480,\"reco\":\"normal\"},\"file_folder_id\":\"5\",\"url\":\"\\/files\\/2016\\/01\\/vmc_256a253734b5f8.jpg\",\"created\":\"2016-01-22 17:06:11\",\"updated\":\"2016-01-22 17:06:11\"},\"Folder\":{\"id\":\"5\",\"name\":\"galerie VMC\",\"parent_id\":null,\"lft\":\"9\",\"rght\":\"10\"},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},{\"File\":{\"id\":\"48\",\"name\":\"VMC 1\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"vmc_156a253700e3dc\",\"nom_fichier\":\"vmc_156a253700e3dc.jpg\",\"poids\":\"153\",\"dimenssions\":{\"width\":640,\"height\":480,\"reco\":\"normal\"},\"file_folder_id\":\"5\",\"url\":\"\\/files\\/2016\\/01\\/vmc_156a253700e3dc.jpg\",\"created\":\"2016-01-22 17:06:08\",\"updated\":\"2016-01-22 17:06:08\"},\"Folder\":{\"id\":\"5\",\"name\":\"galerie VMC\",\"parent_id\":null,\"lft\":\"9\",\"rght\":\"10\"},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},{\"File\":{\"id\":\"51\",\"name\":\"VMC 4\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"vmc_456a253e436a37\",\"nom_fichier\":\"vmc_456a253e436a37.jpg\",\"poids\":\"1087\",\"dimenssions\":{\"width\":1000,\"height\":750,\"reco\":\"normal\"},\"file_folder_id\":\"5\",\"url\":\"\\/files\\/2016\\/01\\/vmc_456a253e436a37.jpg\",\"created\":\"2016-01-22 17:08:04\",\"updated\":\"2016-01-22 17:08:04\"},\"Folder\":{\"id\":\"5\",\"name\":\"galerie VMC\",\"parent_id\":null,\"lft\":\"9\",\"rght\":\"10\"},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},{\"File\":{\"id\":\"27\",\"name\":\"Ventillation avant nettoyage\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"png\",\"type\":\"image\",\"slug\":\"ventilation_156614b50b17e3\",\"nom_fichier\":\"ventilation_156614b50b17e3.png\",\"poids\":\"51\",\"dimenssions\":{\"width\":157,\"height\":177,\"reco\":\"normal\"},\"file_folder_id\":\"5\",\"url\":\"\\/files\\/2015\\/12\\/ventilation_156614b50b17e3.png\",\"created\":\"2015-12-04 09:14:08\",\"updated\":\"2015-12-04 09:14:08\"},\"Folder\":{\"id\":\"5\",\"name\":\"galerie VMC\",\"parent_id\":null,\"lft\":\"9\",\"rght\":\"10\"},\"Extention\":{\"id\":\"png\",\"icone\":\"picture\",\"type\":\"image\"}},{\"File\":{\"id\":\"28\",\"name\":\"Ventillation apr\\u00e8s nettoyage\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"png\",\"type\":\"image\",\"slug\":\"ventilation_256614b53102a7\",\"nom_fichier\":\"ventilation_256614b53102a7.png\",\"poids\":\"56\",\"dimenssions\":{\"width\":180,\"height\":177,\"reco\":\"normal\"},\"file_folder_id\":\"5\",\"url\":\"\\/files\\/2015\\/12\\/ventilation_256614b53102a7.png\",\"created\":\"2015-12-04 09:14:11\",\"updated\":\"2015-12-04 09:14:11\"},\"Folder\":{\"id\":\"5\",\"name\":\"galerie VMC\",\"parent_id\":null,\"lft\":\"9\",\"rght\":\"10\"},\"Extention\":{\"id\":\"png\",\"icone\":\"picture\",\"type\":\"image\"}}],\"position\":4}'),(350,'Page',6,2,'galerie','{\"title\":\"\",\"files\":[{\"File\":{\"id\":\"32\",\"name\":\"Ventilation statique ou naturelle\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"png\",\"type\":\"image\",\"slug\":\"ventilation_656614b5cc1b14\",\"nom_fichier\":\"ventilation_656614b5cc1b14.png\",\"poids\":\"40\",\"dimenssions\":{\"width\":137,\"height\":177,\"reco\":\"normal\"},\"file_folder_id\":\"5\",\"url\":\"\\/files\\/2015\\/12\\/ventilation_656614b5cc1b14.png\",\"created\":\"2015-12-04 09:14:20\",\"updated\":\"2015-12-04 09:14:20\"},\"Folder\":{\"id\":\"5\",\"name\":\"galerie VMC\",\"parent_id\":null,\"lft\":\"9\",\"rght\":\"10\"},\"Extention\":{\"id\":\"png\",\"icone\":\"picture\",\"type\":\"image\"}},{\"File\":{\"id\":\"31\",\"name\":\"Ventilation statique ou naturelle\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"png\",\"type\":\"image\",\"slug\":\"ventilation_556614b5bef8bb\",\"nom_fichier\":\"ventilation_556614b5bef8bb.png\",\"poids\":\"78\",\"dimenssions\":{\"width\":178,\"height\":177,\"reco\":\"normal\"},\"file_folder_id\":\"5\",\"url\":\"\\/files\\/2015\\/12\\/ventilation_556614b5bef8bb.png\",\"created\":\"2015-12-04 09:14:19\",\"updated\":\"2015-12-04 09:14:19\"},\"Folder\":{\"id\":\"5\",\"name\":\"galerie VMC\",\"parent_id\":null,\"lft\":\"9\",\"rght\":\"10\"},\"Extention\":{\"id\":\"png\",\"icone\":\"picture\",\"type\":\"image\"}},{\"File\":{\"id\":\"33\",\"name\":\"Ventilation statique ou naturelle\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"png\",\"type\":\"image\",\"slug\":\"ventilation_756614b605daf8\",\"nom_fichier\":\"ventilation_756614b605daf8.png\",\"poids\":\"113\",\"dimenssions\":{\"width\":336,\"height\":177,\"reco\":\"normal\"},\"file_folder_id\":null,\"url\":\"\\/files\\/2015\\/12\\/ventilation_756614b605daf8.png\",\"created\":\"2015-12-04 09:14:24\",\"updated\":\"2015-12-04 09:14:24\"},\"Folder\":{\"id\":null,\"name\":null,\"parent_id\":null,\"lft\":null,\"rght\":null},\"Extention\":{\"id\":\"png\",\"icone\":\"picture\",\"type\":\"image\"}}],\"position\":5}'),(351,'Page',6,3,'txt','{\"title\":\"\",\"txt\":\"<p><strong><a href=\\\"\\/\\\">DDH<\\/a><\\/strong>, entreprise situ\\u00e9e \\u00e0 <strong>TOURCOING<\\/strong> dans <strong>le Nord<\\/strong> (<strong>59<\\/strong>) r\\u00e9alise des devis gratuits et personnalis\\u00e9s pour toujours mieux vous servir.<br> Pour plus d\'informations sur nos prestations, n\'h\\u00e9sitez pas \\u00e0 nous <strong>joindre par t\\u00e9l\\u00e9phone<\\/strong> au <span style=\\\"font-size:21px;\\\">03 20 26 40 51 <\\/span><span><\\/span>ou gr\\u00e2ce au <strong><a href=\\\"\\/contact\\/p-3\\\">formulaire de contact<\\/a><\\/strong>.<\\/p>\",\"position\":3}'),(356,'Page',2,0,'txt','{\"title\":\"\",\"txt\":\"<iframe src=\\\"https:\\/\\/www.google.com\\/maps\\/embed?pb=!1m18!1m12!1m3!1d2526.7059858636517!2d3.1534688159122553!3d50.70684037673811!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c329277feea8f9%3A0x5b0d71df491e5674!2sDDH!5e0!3m2!1sfr!2sfr!4v1449217572203\\\" width=\\\"100%\\\" height=\\\"450\\\" frameborder=\\\"0\\\" style=\\\"border:0\\\" allowfullscreen><\\/iframe>\"}'),(357,'Page',3,0,'img','{\"title\":\"\",\"img\":{\"File\":{\"id\":\"54\",\"name\":\"flotte\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"flotte56a2560b6bb36\",\"nom_fichier\":\"flotte56a2560b6bb36.jpg\",\"poids\":\"328\",\"dimenssions\":{\"width\":1000,\"height\":562,\"reco\":\"normal\"},\"file_folder_id\":null,\"url\":\"\\/files\\/2016\\/01\\/flotte56a2560b6bb36.jpg\",\"created\":\"2016-01-22 17:17:15\",\"updated\":\"2016-01-22 17:17:15\"},\"Folder\":{\"id\":null,\"name\":null,\"parent_id\":null,\"lft\":null,\"rght\":null},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},\"position\":1}'),(362,'Page',8,0,'txtimg','{\"title\":\"Dispositif anti-pigeons\",\"txt\":\"<h2><strong>Dispositif anti-pigeons<\\/strong><\\/h2><strong><\\/strong>Vous recherchez un <strong>syst\\u00e8me efficace<\\/strong> pour \\u00e9loigner les <strong>pigeons<\\/strong>?\\n<p>\\n\\tVous en avez assez de l\'<strong>odeur <\\/strong>qu\'ils d\\u00e9gagent ou du <strong>bruit<\\/strong> qu\'ils font?\\n<\\/p><p>\\n\\tQuel que soit le probl\\u00e8me, l\'\\u00e9quipe DDH vous propose divers <strong>dispositifs anti-pigeons<\\/strong> r\\u00e9sistants et fiables:\\n<\\/p><ul>\\n\\t<li><strong>les pics<\\/strong>: \\u00e0 placer sur le bord d\'une fen\\u00eatre ou d\'une corniche<\\/li>\\n\\t<strong><\\/strong>\\n\\t<li><strong>les filets<\\/strong><\\/li>\\n\\t<li>etc.<\\/li>\\n<\\/ul>\",\"img\":{\"File\":{\"id\":\"44\",\"name\":\"shutterstock_48675082\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"shutterstock_48675082568100a7a8d6c\",\"nom_fichier\":\"shutterstock_48675082568100a7a8d6c.jpg\",\"poids\":\"601\",\"dimenssions\":{\"width\":1000,\"height\":667,\"reco\":\"normal\"},\"file_folder_id\":null,\"url\":\"\\/files\\/2015\\/12\\/shutterstock_48675082568100a7a8d6c.jpg\",\"created\":\"2015-12-28 10:28:07\",\"updated\":\"2015-12-28 10:28:07\"},\"Folder\":{\"id\":null,\"name\":null,\"parent_id\":null,\"lft\":null,\"rght\":null},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},\"params\":{\"prop\":\"img-right\"},\"position\":5}'),(363,'Page',8,1,'txtimg','{\"title\":\"\",\"txt\":\"<h2> Activit\\u00e9s annexes<\\/h2><ul><li>Vente de produits au d\\u00e9tail<\\/li><li>Entretien des terrasses<\\/li><li> D\\u00e9graissage des hottes de cuisine<\\/li><li>D\\u00e9barras - Nettoyage: DDH propose un service de nettoyage et d\\u00e9barras. Nous sommes sp\\u00e9cialis\\u00e9s dans les conditions extr\\u00eames (expulsions, d\\u00e9c\\u00e8s, squates\\u2026)<u><\\/u><u><\\/u>Nous intervenons dans tous les locaux (Maisons, jardins, immeubles, caves, chantiers\\u2026)<u><\\/u><u><\\/u>Apr\\u00e8s notre passage, votre local sera vide, propre et d\\u00e9sinfect\\u00e9<\\/li><li>Fourniture, pose et entretien des d\\u00e9sinsectiseurs \\u00e9l\\u00e9ctriques<\\/li><li>D\\u00e9bouchage et nettoyage des colonnes vide-ordures <\\/li><\\/ul><p> <strong>* Liste non exhaustive, n\'h\\u00e9sitez pas \\u00e0 nous contacter.<\\/strong><\\/p>\",\"img\":{\"File\":{\"id\":\"52\",\"name\":\"activit\\u00e9s annexes\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"activites_annexes56a254c03f591\",\"nom_fichier\":\"activites_annexes56a254c03f591.jpg\",\"poids\":\"485\",\"dimenssions\":{\"width\":1632,\"height\":1224,\"reco\":\"trop grande\"},\"file_folder_id\":null,\"url\":\"\\/files\\/2016\\/01\\/activites_annexes56a254c03f591.jpg\",\"created\":\"2016-01-22 17:11:44\",\"updated\":\"2016-01-22 17:11:44\"},\"Folder\":{\"id\":null,\"name\":null,\"parent_id\":null,\"lft\":null,\"rght\":null},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},\"params\":{\"prop\":\"img-right\"},\"position\":5}'),(364,'Page',8,2,'galerie','{\"title\":\"\",\"files\":[{\"File\":{\"id\":\"38\",\"name\":\"Divers\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"hotteavant56614d2ac1200\",\"nom_fichier\":\"hotteavant56614d2ac1200.jpg\",\"poids\":\"72\",\"dimenssions\":{\"width\":250,\"height\":160,\"reco\":\"normal\"},\"file_folder_id\":\"6\",\"url\":\"\\/files\\/2015\\/12\\/hotteavant56614d2ac1200.jpg\",\"created\":\"2015-12-04 09:22:02\",\"updated\":\"2015-12-04 09:22:02\"},\"Folder\":{\"id\":\"6\",\"name\":\"galerie divers\",\"parent_id\":null,\"lft\":\"11\",\"rght\":\"12\"},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},{\"File\":{\"id\":\"37\",\"name\":\"Divers\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"png\",\"type\":\"image\",\"slug\":\"terrasse_toit56614d29a5519\",\"nom_fichier\":\"terrasse_toit56614d29a5519.png\",\"poids\":\"93\",\"dimenssions\":{\"width\":250,\"height\":160,\"reco\":\"normal\"},\"file_folder_id\":\"6\",\"url\":\"\\/files\\/2015\\/12\\/terrasse_toit56614d29a5519.png\",\"created\":\"2015-12-04 09:22:01\",\"updated\":\"2015-12-04 09:22:01\"},\"Folder\":{\"id\":\"6\",\"name\":\"galerie divers\",\"parent_id\":null,\"lft\":\"11\",\"rght\":\"12\"},\"Extention\":{\"id\":\"png\",\"icone\":\"picture\",\"type\":\"image\"}},{\"File\":{\"id\":\"35\",\"name\":\"D\\u00e9barras\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"png\",\"type\":\"image\",\"slug\":\"debarras56614d24cc9be\",\"nom_fichier\":\"debarras56614d24cc9be.png\",\"poids\":\"86\",\"dimenssions\":{\"width\":250,\"height\":160,\"reco\":\"normal\"},\"file_folder_id\":\"6\",\"url\":\"\\/files\\/2015\\/12\\/debarras56614d24cc9be.png\",\"created\":\"2015-12-04 09:21:56\",\"updated\":\"2015-12-04 09:21:56\"},\"Folder\":{\"id\":\"6\",\"name\":\"galerie divers\",\"parent_id\":null,\"lft\":\"11\",\"rght\":\"12\"},\"Extention\":{\"id\":\"png\",\"icone\":\"picture\",\"type\":\"image\"}},{\"File\":{\"id\":\"36\",\"name\":\"Divers\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"png\",\"type\":\"image\",\"slug\":\"electrique56614d27cf922\",\"nom_fichier\":\"electrique56614d27cf922.png\",\"poids\":\"96\",\"dimenssions\":{\"width\":250,\"height\":160,\"reco\":\"normal\"},\"file_folder_id\":\"6\",\"url\":\"\\/files\\/2015\\/12\\/electrique56614d27cf922.png\",\"created\":\"2015-12-04 09:21:59\",\"updated\":\"2015-12-04 09:21:59\"},\"Folder\":{\"id\":\"6\",\"name\":\"galerie divers\",\"parent_id\":null,\"lft\":\"11\",\"rght\":\"12\"},\"Extention\":{\"id\":\"png\",\"icone\":\"picture\",\"type\":\"image\"}}],\"position\":3}'),(365,'Page',8,3,'txt','{\"title\":\"\",\"txt\":\"<p>L\'\\u00e9quipe de l\'entreprise <b>HDD<\\/b> situ\\u00e9e \\u00e0 <strong>TOURCOING<\\/strong> dans <strong>le Nord<\\/strong> (<strong>59<\\/strong>) vous propose un devis gratuit et personnalis\\u00e9 par rapport \\u00e0 vos attentes, pour une satisfaction compl\\u00e8te de votre part.<br> Pour en conna\\u00eetre d\'avantage sur les services disponibles , n\'h\\u00e9sitez pas \\u00e0 nous <strong>joindre par t\\u00e9l\\u00e9phone<\\/strong> au <strong>0<\\/strong><b>56 333 558<\\/b><span><\\/span><span style=\\\"font-size:21px;\\\"> <\\/span><span><\\/span>ou en remplissant le<a href=\\\"\\/contact\\/p-3\\\"> <\\/a><strong><a href=\\\"\\/contact\\/p-3\\\">formulaire de contact<\\/a><\\/strong>.<\\/p>\",\"position\":2}');
/*!40000 ALTER TABLE `content_modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content_types`
--

DROP TABLE IF EXISTS `content_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_types` (
  `id` varchar(128) NOT NULL COMMENT 'Nom de la class model',
  `name` varchar(128) NOT NULL,
  `model` varchar(256) NOT NULL,
  `class_model` varchar(256) NOT NULL,
  `controleur` varchar(256) NOT NULL,
  `prefixe_url` varchar(256) NOT NULL,
  `sufixe_url` varchar(128) DEFAULT NULL,
  `construct_url` tinyint(1) DEFAULT NULL,
  `description` varchar(256) NOT NULL,
  `icon` varchar(128) NOT NULL,
  `ordre` int(11) NOT NULL DEFAULT '0',
  `arborescence` tinyint(1) DEFAULT NULL,
  `associable` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_types`
--

LOCK TABLES `content_types` WRITE;
/*!40000 ALTER TABLE `content_types` DISABLE KEYS */;
INSERT INTO `content_types` VALUES ('page','Page','Page','Page','pages','p-','',1,'Une page','file',2,1,1),('home','Accueil','Home','Home','homes','','',NULL,'La page d\'accueil','home',1,1,0),('contact','Contact','ContactManager.Contact','Contact','contact_manager/contacts','contact-','',1,'Une page contact','envelope',3,1,1),('actualite','Actualite','Blog.Actualite','Actualite','blog/actualites','news-','',NULL,'Une actualite','',0,0,1),('image','Image','','','','','',NULL,'','',0,0,0),('file','Fichier','','','','','',NULL,'','',0,0,0),('marque','Marque','Marque','Catalogue.Marque','catalogue/marques','',NULL,NULL,'','',0,NULL,0);
/*!40000 ALTER TABLE `content_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `countries`
--

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `etats`
--

DROP TABLE IF EXISTS `etats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `etats` (
  `id` varchar(128) NOT NULL,
  `name` varchar(128) NOT NULL,
  `class` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `etats`
--

LOCK TABLES `etats` WRITE;
/*!40000 ALTER TABLE `etats` DISABLE KEYS */;
INSERT INTO `etats` VALUES ('draft','brouillon','danger'),('publish','en ligne','success');
/*!40000 ALTER TABLE `etats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `even_inscrits`
--

DROP TABLE IF EXISTS `even_inscrits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `even_inscrits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `even_id` int(11) NOT NULL,
  `first_name` varchar(128) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `email` varchar(128) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `even_id` (`even_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `even_inscrits`
--

LOCK TABLES `even_inscrits` WRITE;
/*!40000 ALTER TABLE `even_inscrits` DISABLE KEYS */;
/*!40000 ALTER TABLE `even_inscrits` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `extentions`
--

DROP TABLE IF EXISTS `extentions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `extentions` (
  `id` varchar(10) CHARACTER SET latin1 NOT NULL,
  `icone` varchar(128) NOT NULL,
  `type` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `extentions`
--

LOCK TABLES `extentions` WRITE;
/*!40000 ALTER TABLE `extentions` DISABLE KEYS */;
INSERT INTO `extentions` VALUES ('png','picture','image'),('jpg','picture','image'),('gif','picture','image'),('jpeg','picture','image'),('pdf','pdf','fichier'),('doc','word','fichier'),('xls','excel','fichier'),('ppt','powerpoint','fichier'),('docx','word','fichier'),('xlsx','excel','fichier'),('zip','archive','fichier'),('mp3','audio','audio'),('mp4','video','video'),('flv','video','video');
/*!40000 ALTER TABLE `extentions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `file_assocs`
--

DROP TABLE IF EXISTS `file_assocs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `file_assocs` (
  `id` int(11) NOT NULL,
  `file_id` int(11) DEFAULT NULL,
  `model` varchar(128) DEFAULT NULL,
  `model_id` int(11) DEFAULT NULL,
  `group` varchar(256) DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `default` tinyint(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `file_assocs`
--

LOCK TABLES `file_assocs` WRITE;
/*!40000 ALTER TABLE `file_assocs` DISABLE KEYS */;
/*!40000 ALTER TABLE `file_assocs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `file_folders`
--

DROP TABLE IF EXISTS `file_folders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `file_folders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `lft` int(11) NOT NULL,
  `rght` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `file_folders`
--

LOCK TABLES `file_folders` WRITE;
/*!40000 ALTER TABLE `file_folders` DISABLE KEYS */;
INSERT INTO `file_folders` VALUES (1,'diapo accueil',NULL,1,2),(2,'3d',NULL,3,4),(3,'galerie 3d',NULL,5,6),(4,'galerie humidité',NULL,7,8),(5,'galerie VMC',NULL,9,10),(6,'galerie divers',NULL,11,12);
/*!40000 ALTER TABLE `file_folders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `file_olds`
--

DROP TABLE IF EXISTS `file_olds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `file_olds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `description` text NOT NULL,
  `lien` varchar(256) NOT NULL,
  `embed` text NOT NULL,
  `extention_id` varchar(10) DEFAULT NULL,
  `type` varchar(256) DEFAULT NULL,
  `slug` varchar(256) DEFAULT NULL,
  `nom_fichier` varchar(256) DEFAULT NULL,
  `poids` decimal(10,0) DEFAULT NULL,
  `dossier` varchar(256) DEFAULT NULL,
  `url` varchar(256) DEFAULT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `file_olds`
--

LOCK TABLES `file_olds` WRITE;
/*!40000 ALTER TABLE `file_olds` DISABLE KEYS */;
/*!40000 ALTER TABLE `file_olds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `files`
--

DROP TABLE IF EXISTS `files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `description` text NOT NULL,
  `lien` varchar(256) NOT NULL,
  `embed` text NOT NULL,
  `extention_id` varchar(10) DEFAULT NULL,
  `type` varchar(256) DEFAULT NULL,
  `slug` varchar(256) DEFAULT NULL,
  `nom_fichier` varchar(256) DEFAULT NULL,
  `poids` decimal(10,0) DEFAULT NULL,
  `dimenssions` text NOT NULL,
  `file_folder_id` int(11) DEFAULT NULL,
  `url` varchar(256) DEFAULT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=58 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `files`
--

LOCK TABLES `files` WRITE;
/*!40000 ALTER TABLE `files` DISABLE KEYS */;
INSERT INTO `files` VALUES (4,'water_p','','','','jpg','image','water_p566061539d749','water_p566061539d749.jpg',54,'{\"width\":900,\"height\":506,\"reco\":\"normal\"}',1,'/files/2015/12/water_p566061539d749.jpg','2015-12-03 16:35:47','2015-12-03 16:35:47'),(5,'wasp hive wallpapers 2560x1440','','','','jpg','image','wasp_hive_wallpapers_2560x1440566061555f4bd','wasp_hive_wallpapers_2560x1440566061555f4bd.jpg',82,'{\"width\":900,\"height\":506,\"reco\":\"normal\"}',1,'/files/2015/12/wasp_hive_wallpapers_2560x1440566061555f4bd.jpg','2015-12-03 16:35:49','2015-12-03 16:35:49'),(6,'331054','','','','jpg','image','33105456606159b09e6','33105456606159b09e6.jpg',50,'{\"width\":900,\"height\":506,\"reco\":\"normal\"}',1,'/files/2015/12/33105456606159b09e6.jpg','2015-12-03 16:35:53','2015-12-03 16:35:53'),(7,'447f8875c5_46677_cafard (c) slamolo flickr cc by nc 2','','','','jpg','image','447f8875c5_46677_cafard_c_slamolo_flickr_cc_by_nc_256606161dd1f8','447f8875c5_46677_cafard_c_slamolo_flickr_cc_by_nc_256606161dd1f8.jpg',119,'{\"width\":900,\"height\":506,\"reco\":\"normal\"}',1,'/files/2015/12/447f8875c5_46677_cafard_c_slamolo_flickr_cc_by_nc_256606161dd1f8.jpg','2015-12-03 16:36:01','2015-12-03 16:36:01'),(8,'shutterstock_1396866','','','','jpg','image','shutterstock_1396866566071d9cd0ea','shutterstock_1396866566071d9cd0ea.jpg',629,'{\"width\":2272,\"height\":1704,\"reco\":\"trop grande\"}',2,'/files/2015/12/shutterstock_1396866566071d9cd0ea.jpg','2015-12-03 17:46:17','2015-12-03 17:46:17'),(9,'shutterstock_2829088','','','','jpg','image','shutterstock_28290885660748986257','shutterstock_28290885660748986257.jpg',59,'{\"width\":1000,\"height\":667,\"reco\":\"normal\"}',2,'/files/2015/12/shutterstock_28290885660748986257.jpg','2015-12-03 17:57:45','2015-12-03 17:57:45'),(10,'shutterstock_3128184','','','','jpg','image','shutterstock_3128184566074c002928','shutterstock_3128184566074c002928.jpg',104,'{\"width\":1000,\"height\":639,\"reco\":\"normal\"}',2,'/files/2015/12/shutterstock_3128184566074c002928.jpg','2015-12-03 17:58:40','2015-12-03 17:58:40'),(20,'champignon-1','','','','png','image','champignon_15661498ae7c74','champignon_15661498ae7c74.png',142,'{\"width\":300,\"height\":225,\"reco\":\"normal\"}',4,'/files/2015/12/champignon_15661498ae7c74.png','2015-12-04 09:06:35','2015-12-04 09:06:35'),(12,'shutterstock_16542133','','','','jpg','image','shutterstock_16542133566074ea46736','shutterstock_16542133566074ea46736.jpg',588,'{\"width\":1000,\"height\":667,\"reco\":\"normal\"}',2,'/files/2015/12/shutterstock_16542133566074ea46736.jpg','2015-12-03 17:59:22','2015-12-03 17:59:22'),(13,'pb090151','','','','jpg','image','pb09015156607509bf796','pb09015156607509bf796.jpg',63,'{\"width\":300,\"height\":225,\"reco\":\"normal\"}',3,'/files/2015/12/pb09015156607509bf796.jpg','2015-12-03 17:59:53','2015-12-03 17:59:53'),(14,'pb140163','','','','jpg','image','pb1401635660750d4a455','pb1401635660750d4a455.jpg',84,'{\"width\":300,\"height\":225,\"reco\":\"normal\"}',3,'/files/2015/12/pb1401635660750d4a455.jpg','2015-12-03 17:59:57','2015-12-03 17:59:57'),(15,'pb140164','','','','jpg','image','pb1401645660750fc532d','pb1401645660750fc532d.jpg',77,'{\"width\":300,\"height\":225,\"reco\":\"normal\"}',3,'/files/2015/12/pb1401645660750fc532d.jpg','2015-12-03 17:59:59','2015-12-03 17:59:59'),(16,'gel-advion-photo','','','','jpg','image','gel_advion_photo566075116455f','gel_advion_photo566075116455f.jpg',38,'{\"width\":300,\"height\":225,\"reco\":\"normal\"}',3,'/files/2015/12/gel_advion_photo566075116455f.jpg','2015-12-03 18:00:01','2015-12-03 18:00:01'),(17,'pb070142','','','','jpg','image','pb070142566075137f7ca','pb070142566075137f7ca.jpg',54,'{\"width\":300,\"height\":225,\"reco\":\"normal\"}',3,'/files/2015/12/pb070142566075137f7ca.jpg','2015-12-03 18:00:03','2015-12-03 18:00:03'),(18,'pb070145','','','','jpg','image','pb07014556607516ab040','pb07014556607516ab040.jpg',55,'{\"width\":300,\"height\":225,\"reco\":\"normal\"}',3,'/files/2015/12/pb07014556607516ab040.jpg','2015-12-03 18:00:06','2015-12-03 18:00:06'),(19,'pb090149','','','','jpg','image','pb0901495660751917000','pb0901495660751917000.jpg',65,'{\"width\":300,\"height\":225,\"reco\":\"normal\"}',3,'/files/2015/12/pb0901495660751917000.jpg','2015-12-03 18:00:09','2015-12-03 18:00:09'),(21,'champignon-2','','','','png','image','champignon_25661498c65256','champignon_25661498c65256.png',128,'{\"width\":300,\"height\":225,\"reco\":\"normal\"}',4,'/files/2015/12/champignon_25661498c65256.png','2015-12-04 09:06:36','2015-12-04 09:06:36'),(22,'champignon-3','','','','png','image','champignon_35661498dbd4e5','champignon_35661498dbd4e5.png',139,'{\"width\":300,\"height\":225,\"reco\":\"normal\"}',4,'/files/2015/12/champignon_35661498dbd4e5.png','2015-12-04 09:06:37','2015-12-04 09:06:37'),(23,'champignon-4','','','','png','image','champignon_45661498ff35d3','champignon_45661498ff35d3.png',143,'{\"width\":300,\"height\":225,\"reco\":\"normal\"}',4,'/files/2015/12/champignon_45661498ff35d3.png','2015-12-04 09:06:39','2015-12-04 09:06:39'),(24,'champignon-5','','','','png','image','champignon_556614991b6e10','champignon_556614991b6e10.png',151,'{\"width\":300,\"height\":225,\"reco\":\"normal\"}',4,'/files/2015/12/champignon_556614991b6e10.png','2015-12-04 09:06:41','2015-12-04 09:06:41'),(25,'shutterstock_2947540','','','','jpg','image','shutterstock_2947540566149d7bd752','shutterstock_2947540566149d7bd752.jpg',61,'{\"width\":700,\"height\":465,\"reco\":\"normal\"}',NULL,'/files/2015/12/shutterstock_2947540566149d7bd752.jpg','2015-12-04 09:07:51','2015-12-04 09:07:51'),(26,'shutterstock_10211947','','','','jpg','image','shutterstock_1021194756614a3f7f248','shutterstock_1021194756614a3f7f248.jpg',1063,'{\"width\":1000,\"height\":800,\"reco\":\"normal\"}',NULL,'/files/2015/12/shutterstock_1021194756614a3f7f248.jpg','2015-12-04 09:09:35','2015-12-04 09:09:35'),(27,'ventilation-1','','','','png','image','ventilation_156614b50b17e3','ventilation_156614b50b17e3.png',51,'{\"width\":157,\"height\":177,\"reco\":\"normal\"}',5,'/files/2015/12/ventilation_156614b50b17e3.png','2015-12-04 09:14:08','2015-12-04 09:14:08'),(28,'ventilation-2','','','','png','image','ventilation_256614b53102a7','ventilation_256614b53102a7.png',56,'{\"width\":180,\"height\":177,\"reco\":\"normal\"}',5,'/files/2015/12/ventilation_256614b53102a7.png','2015-12-04 09:14:11','2015-12-04 09:14:11'),(29,'ventilation-3','','','','png','image','ventilation_356614b5a0e17f','ventilation_356614b5a0e17f.png',64,'{\"width\":239,\"height\":177,\"reco\":\"normal\"}',5,'/files/2015/12/ventilation_356614b5a0e17f.png','2015-12-04 09:14:18','2015-12-04 09:14:18'),(30,'ventilation-4','','','','png','image','ventilation_456614b5ac7e59','ventilation_456614b5ac7e59.png',25,'{\"width\":189,\"height\":177,\"reco\":\"normal\"}',5,'/files/2015/12/ventilation_456614b5ac7e59.png','2015-12-04 09:14:18','2015-12-04 09:14:18'),(31,'ventilation-5','','','','png','image','ventilation_556614b5bef8bb','ventilation_556614b5bef8bb.png',78,'{\"width\":178,\"height\":177,\"reco\":\"normal\"}',5,'/files/2015/12/ventilation_556614b5bef8bb.png','2015-12-04 09:14:19','2015-12-04 09:14:19'),(32,'ventilation-6','','','','png','image','ventilation_656614b5cc1b14','ventilation_656614b5cc1b14.png',40,'{\"width\":137,\"height\":177,\"reco\":\"normal\"}',5,'/files/2015/12/ventilation_656614b5cc1b14.png','2015-12-04 09:14:20','2015-12-04 09:14:20'),(33,'ventilation-7','','','','png','image','ventilation_756614b605daf8','ventilation_756614b605daf8.png',113,'{\"width\":336,\"height\":177,\"reco\":\"normal\"}',NULL,'/files/2015/12/ventilation_756614b605daf8.png','2015-12-04 09:14:24','2015-12-04 09:14:24'),(34,'Fotolia_55642623_S','','','','jpg','image','fotolia_55642623_s56614beee1f2d','fotolia_55642623_s56614beee1f2d.jpg',83,'{\"width\":566,\"height\":849,\"reco\":\"normal\"}',NULL,'/files/2015/12/fotolia_55642623_s56614beee1f2d.jpg','2015-12-04 09:16:46','2015-12-04 09:16:46'),(35,'debarras','','','','png','image','debarras56614d24cc9be','debarras56614d24cc9be.png',86,'{\"width\":250,\"height\":160,\"reco\":\"normal\"}',6,'/files/2015/12/debarras56614d24cc9be.png','2015-12-04 09:21:56','2015-12-04 09:21:56'),(36,'electrique','','','','png','image','electrique56614d27cf922','electrique56614d27cf922.png',96,'{\"width\":250,\"height\":160,\"reco\":\"normal\"}',6,'/files/2015/12/electrique56614d27cf922.png','2015-12-04 09:21:59','2015-12-04 09:21:59'),(37,'terrasse-toit','','','','png','image','terrasse_toit56614d29a5519','terrasse_toit56614d29a5519.png',93,'{\"width\":250,\"height\":160,\"reco\":\"normal\"}',6,'/files/2015/12/terrasse_toit56614d29a5519.png','2015-12-04 09:22:01','2015-12-04 09:22:01'),(38,'hotteavant','','','','jpg','image','hotteavant56614d2ac1200','hotteavant56614d2ac1200.jpg',72,'{\"width\":250,\"height\":160,\"reco\":\"normal\"}',6,'/files/2015/12/hotteavant56614d2ac1200.jpg','2015-12-04 09:22:02','2015-12-04 09:22:02'),(39,'shutterstock_27584080','','','','jpg','image','shutterstock_2758408056614d48d70aa','shutterstock_2758408056614d48d70aa.jpg',1051,'{\"width\":1000,\"height\":667,\"reco\":\"normal\"}',NULL,'/files/2015/12/shutterstock_2758408056614d48d70aa.jpg','2015-12-04 09:22:32','2015-12-04 09:22:32'),(40,'shutterstock_3143560','','','','jpg','image','shutterstock_31435605680fdfe95e9f','shutterstock_31435605680fdfe95e9f.jpg',382,'{\"width\":1000,\"height\":562,\"reco\":\"normal\"}',1,'/files/2015/12/shutterstock_31435605680fdfe95e9f.jpg','2015-12-28 10:16:46','2015-12-28 10:16:46'),(41,'Fotolia_74612505_M','','','','jpg','image','fotolia_74612505_m5680fed70595b','fotolia_74612505_m5680fed70595b.jpg',363,'{\"width\":1000,\"height\":562,\"reco\":\"normal\"}',1,'/files/2015/12/fotolia_74612505_m5680fed70595b.jpg','2015-12-28 10:20:23','2015-12-28 10:20:23'),(42,'Fotolia_92281529_M','','','','jpg','image','fotolia_92281529_m5680ff7a0ef2d','fotolia_92281529_m5680ff7a0ef2d.jpg',168,'{\"width\":1000,\"height\":562,\"reco\":\"normal\"}',1,'/files/2015/12/fotolia_92281529_m5680ff7a0ef2d.jpg','2015-12-28 10:23:06','2015-12-28 10:23:06'),(43,'Fotolia_85997612_M','','','','jpg','image','fotolia_85997612_m5681001c5c410','fotolia_85997612_m5681001c5c410.jpg',389,'{\"width\":1000,\"height\":562,\"reco\":\"normal\"}',1,'/files/2015/12/fotolia_85997612_m5681001c5c410.jpg','2015-12-28 10:25:48','2015-12-28 10:25:48'),(44,'shutterstock_48675082','','','','jpg','image','shutterstock_48675082568100a7a8d6c','shutterstock_48675082568100a7a8d6c.jpg',601,'{\"width\":1000,\"height\":667,\"reco\":\"normal\"}',NULL,'/files/2015/12/shutterstock_48675082568100a7a8d6c.jpg','2015-12-28 10:28:07','2015-12-28 10:28:07'),(45,'Fotolia_74612505_M','','','','jpg','image','fotolia_74612505_m56810168ab83d','fotolia_74612505_m56810168ab83d.jpg',363,'{\"width\":1000,\"height\":562,\"reco\":\"normal\"}',NULL,'/files/2015/12/fotolia_74612505_m56810168ab83d.jpg','2015-12-28 10:31:20','2015-12-28 10:31:20'),(46,'certificat','','','','png','image','certificat568107244572e','certificat568107244572e.png',56,'{\"width\":422,\"height\":210,\"reco\":\"normal\"}',NULL,'/files/2015/12/certificat568107244572e.png','2015-12-28 10:55:48','2015-12-28 10:55:48'),(47,'abeilles','','','','jpg','image','abeilles56a107061afe2','abeilles56a107061afe2.jpg',308,'{\"width\":1000,\"height\":562,\"reco\":\"normal\"}',1,'/files/2016/01/abeilles56a107061afe2.jpg','2016-01-21 17:27:50','2016-01-21 17:27:50'),(48,'VMC 1','','','','jpg','image','vmc_156a253700e3dc','vmc_156a253700e3dc.jpg',153,'{\"width\":640,\"height\":480,\"reco\":\"normal\"}',5,'/files/2016/01/vmc_156a253700e3dc.jpg','2016-01-22 17:06:08','2016-01-22 17:06:08'),(49,'VMC 2','','','','jpg','image','vmc_256a253734b5f8','vmc_256a253734b5f8.jpg',137,'{\"width\":640,\"height\":480,\"reco\":\"normal\"}',5,'/files/2016/01/vmc_256a253734b5f8.jpg','2016-01-22 17:06:11','2016-01-22 17:06:11'),(50,'VMC 3','','','','jpg','image','vmc_356a253806ac9e','vmc_356a253806ac9e.jpg',1996,'{\"width\":2560,\"height\":1920,\"reco\":\"trop grande\"}',5,'/files/2016/01/vmc_356a253806ac9e.jpg','2016-01-22 17:06:24','2016-01-22 17:06:24'),(51,'VMC 4','','','','jpg','image','vmc_456a253e436a37','vmc_456a253e436a37.jpg',1087,'{\"width\":1000,\"height\":750,\"reco\":\"normal\"}',5,'/files/2016/01/vmc_456a253e436a37.jpg','2016-01-22 17:08:04','2016-01-22 17:08:04'),(52,'activités annexes','','','','jpg','image','activites_annexes56a254c03f591','activites_annexes56a254c03f591.jpg',485,'{\"width\":1632,\"height\":1224,\"reco\":\"trop grande\"}',NULL,'/files/2016/01/activites_annexes56a254c03f591.jpg','2016-01-22 17:11:44','2016-01-22 17:11:44'),(53,'flotte voitures pour page accueil et contact ','','','','jpg','image','flotte_voitures_pour_page_accueil_et_contact56a254f72bb87','flotte_voitures_pour_page_accueil_et_contact56a254f72bb87.jpg',330,'{\"width\":1280,\"height\":622,\"reco\":\"trop grande\"}',NULL,'/files/2016/01/flotte_voitures_pour_page_accueil_et_contact56a254f72bb87.jpg','2016-01-22 17:12:39','2016-01-22 17:12:39'),(54,'flotte','','','','jpg','image','flotte56a2560b6bb36','flotte56a2560b6bb36.jpg',328,'{\"width\":1000,\"height\":562,\"reco\":\"normal\"}',NULL,'/files/2016/01/flotte56a2560b6bb36.jpg','2016-01-22 17:17:15','2016-01-22 17:17:15'),(55,'3D','','','','jpg','image','3d56a25660ede7b','3d56a25660ede7b.jpg',28,'{\"width\":410,\"height\":307,\"reco\":\"normal\"}',3,'/files/2016/01/3d56a25660ede7b.jpg','2016-01-22 17:18:40','2016-01-22 17:18:40'),(56,'3D - Copie','','','','jpg','image','3d_copie56a2568e2c0fb','3d_copie56a2568e2c0fb.jpg',76,'{\"width\":410,\"height\":307,\"reco\":\"normal\"}',3,'/files/2016/01/3d_copie56a2568e2c0fb.jpg','2016-01-22 17:19:26','2016-01-22 17:19:26'),(57,'nid et blatte','','','','jpg','image','nid_et_blatte56a257232857b','nid_et_blatte56a257232857b.jpg',791,'{\"width\":1000,\"height\":1201,\"reco\":\"trop grande\"}',NULL,'/files/2016/01/nid_et_blatte56a257232857b.jpg','2016-01-22 17:21:55','2016-01-22 17:21:55');
/*!40000 ALTER TABLE `files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form_elements`
--

DROP TABLE IF EXISTS `form_elements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form_elements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(512) NOT NULL,
  `short_name` varchar(16) NOT NULL,
  `type` varchar(16) DEFAULT NULL,
  `sub_type` varchar(64) DEFAULT NULL,
  `mandatory` tinyint(1) NOT NULL,
  `configuration` text NOT NULL,
  `position` int(11) NOT NULL,
  `created` datetime NOT NULL COMMENT 'CakePHP',
  `updated` datetime NOT NULL COMMENT 'CakePHP',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form_elements`
--

LOCK TABLES `form_elements` WRITE;
/*!40000 ALTER TABLE `form_elements` DISABLE KEYS */;
/*!40000 ALTER TABLE `form_elements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form_entries`
--

DROP TABLE IF EXISTS `form_entries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form_entries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `user_ip` varchar(15) NOT NULL,
  `form_id` int(11) NOT NULL,
  `element_id` int(11) NOT NULL,
  `data` text NOT NULL,
  `collection_id` varchar(13) NOT NULL,
  `created` datetime NOT NULL COMMENT 'CakePHP',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form_entries`
--

LOCK TABLES `form_entries` WRITE;
/*!40000 ALTER TABLE `form_entries` DISABLE KEYS */;
/*!40000 ALTER TABLE `form_entries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `forms`
--

DROP TABLE IF EXISTS `forms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `elements_number` int(11) NOT NULL DEFAULT '0',
  `page_id` int(11) DEFAULT NULL,
  `etat_id` varchar(128) DEFAULT NULL,
  `created` datetime NOT NULL COMMENT 'CakePHP',
  `updated` datetime NOT NULL COMMENT 'CakePHP',
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `forms`
--

LOCK TABLES `forms` WRITE;
/*!40000 ALTER TABLE `forms` DISABLE KEYS */;
/*!40000 ALTER TABLE `forms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `lft` int(11) NOT NULL,
  `rght` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groups`
--

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
INSERT INTO `groups` VALUES (1,NULL,1,2,'Administrateurs','2015-01-19 11:37:29'),(2,NULL,0,0,'Redacteurs','2015-12-02 22:15:17');
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `homes`
--

DROP TABLE IF EXISTS `homes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `homes` (
  `id` int(11) NOT NULL,
  `content_type_id` varchar(128) NOT NULL,
  `etat_id` int(11) DEFAULT NULL,
  `menu_id` int(11) DEFAULT NULL,
  `name` varchar(128) NOT NULL DEFAULT '',
  `h1` varchar(256) NOT NULL,
  `url` varchar(1) DEFAULT '/',
  `url_r` varchar(1) NOT NULL DEFAULT '/',
  `meta_description` text NOT NULL,
  `meta_title` varchar(256) NOT NULL DEFAULT '',
  `meta_keywords` text NOT NULL,
  `meta_robots` varchar(128) NOT NULL DEFAULT '',
  `resume` text NOT NULL,
  `body` text NOT NULL,
  `menu_item_id` int(11) DEFAULT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `homes`
--

LOCK TABLES `homes` WRITE;
/*!40000 ALTER TABLE `homes` DISABLE KEYS */;
/*!40000 ALTER TABLE `homes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `i18n`
--

DROP TABLE IF EXISTS `i18n`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `i18n` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `locale` varchar(6) NOT NULL,
  `model` varchar(255) NOT NULL,
  `foreign_key` int(10) NOT NULL,
  `field` varchar(255) NOT NULL,
  `content` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `i18n`
--

LOCK TABLES `i18n` WRITE;
/*!40000 ALTER TABLE `i18n` DISABLE KEYS */;
/*!40000 ALTER TABLE `i18n` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter_campaigns`
--

DROP TABLE IF EXISTS `newsletter_campaigns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter_campaigns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(128) DEFAULT NULL COMMENT 'sms ou email',
  `etat_id` varchar(128) DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  `template_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `list_id` int(11) NOT NULL,
  `default_from_name` varchar(128) NOT NULL,
  `default_from_email` varchar(256) NOT NULL,
  `default_from_telephone` varchar(128) DEFAULT NULL,
  `default_from_subject` varchar(256) NOT NULL,
  `how_on_list` text NOT NULL,
  `send` tinyint(1) NOT NULL DEFAULT '0',
  `nbr` int(11) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter_campaigns`
--

LOCK TABLES `newsletter_campaigns` WRITE;
/*!40000 ALTER TABLE `newsletter_campaigns` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsletter_campaigns` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter_emails`
--

DROP TABLE IF EXISTS `newsletter_emails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter_emails` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `email` varchar(128) CHARACTER SET utf16 NOT NULL,
  `first_name` varchar(128) NOT NULL,
  `last_name` varchar(128) NOT NULL,
  `user_id` int(11) NOT NULL,
  `unsuscribe_link` text NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `liste_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter_emails`
--

LOCK TABLES `newsletter_emails` WRITE;
/*!40000 ALTER TABLE `newsletter_emails` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsletter_emails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter_list_emails`
--

DROP TABLE IF EXISTS `newsletter_list_emails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter_list_emails` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `list_id` int(10) NOT NULL,
  `email_id` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter_list_emails`
--

LOCK TABLES `newsletter_list_emails` WRITE;
/*!40000 ALTER TABLE `newsletter_list_emails` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsletter_list_emails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter_lists`
--

DROP TABLE IF EXISTS `newsletter_lists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter_lists` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `default` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter_lists`
--

LOCK TABLES `newsletter_lists` WRITE;
/*!40000 ALTER TABLE `newsletter_lists` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsletter_lists` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter_templates`
--

DROP TABLE IF EXISTS `newsletter_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `folder_name` varchar(256) NOT NULL,
  `is_uploaded` int(1) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter_templates`
--

LOCK TABLES `newsletter_templates` WRITE;
/*!40000 ALTER TABLE `newsletter_templates` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsletter_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_olds`
--

DROP TABLE IF EXISTS `page_olds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_olds` (
  `id` int(11) NOT NULL,
  `menu_item_id` int(11) DEFAULT NULL,
  `content_type_id` varchar(128) NOT NULL,
  `etat_id` tinyint(1) DEFAULT NULL,
  `menu_id` tinyint(1) DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  `h1` varchar(256) NOT NULL,
  `url` varchar(256) NOT NULL,
  `url_r` varchar(256) NOT NULL DEFAULT '',
  `meta_description` text NOT NULL,
  `meta_title` varchar(256) NOT NULL,
  `meta_keywords` text NOT NULL,
  `meta_robots` varchar(128) NOT NULL,
  `resume` text NOT NULL,
  `body` text NOT NULL,
  `updated` datetime NOT NULL,
  `created` datetime NOT NULL,
  `icone` varchar(128) DEFAULT NULL,
  `template` varchar(128) DEFAULT NULL,
  `slug` varchar(256) NOT NULL,
  `rght` int(11) NOT NULL,
  `lft` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_olds`
--

LOCK TABLES `page_olds` WRITE;
/*!40000 ALTER TABLE `page_olds` DISABLE KEYS */;
/*!40000 ALTER TABLE `page_olds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content_type_id` varchar(128) NOT NULL DEFAULT 'page',
  `etat_id` varchar(128) NOT NULL DEFAULT 'draft',
  `publication_date` date NOT NULL,
  `menu_id` tinyint(1) DEFAULT NULL,
  `acces_membre` tinyint(1) DEFAULT '0',
  `name` varchar(128) NOT NULL,
  `h1` varchar(256) NOT NULL,
  `sous_titre` varchar(256) NOT NULL,
  `url` varchar(256) NOT NULL,
  `url_r` varchar(256) NOT NULL DEFAULT '',
  `vignette` text NOT NULL,
  `meta_description` text NOT NULL,
  `meta_title` varchar(256) NOT NULL,
  `meta_keywords` text NOT NULL,
  `meta_robots` varchar(128) NOT NULL,
  `section` varchar(128) DEFAULT NULL,
  `resume` text NOT NULL,
  `body` text NOT NULL,
  `rss_name` varchar(128) DEFAULT NULL,
  `rss_description` text,
  `rss_niveau` varchar(20) DEFAULT NULL,
  `updated` datetime NOT NULL,
  `created` datetime NOT NULL,
  `icone` varchar(128) DEFAULT NULL,
  `template` varchar(128) DEFAULT NULL,
  `slug` varchar(256) NOT NULL,
  `position` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rght` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (1,'page','publish','2015-12-03',1,0,'Accueil','Accueil','Votre protection environnementale','/accueil/p-1','','','Découvrez l\'univers de DDH, dératisation, désinsectisation, désinfection et pleins d\'autres prestations à Tourcoing en France','Dératisation - Tourcoing Lille Nord 59 - DDH France','','',NULL,'','<p>Entreprise située à <strong>Tourcoing</strong> près de<strong> Lille </strong>dans le département du Nord <strong>(59)</strong>,<strong> DDH </strong>met à disposition de nombreux services en matière d<strong>\'hygiène antiparasitaire</strong>.</p><p> Nous intervenons rapidement à votre domicile et prenons en charge toutes prestations de<a href=\"/les-3d/p-4\"> <strong>dératisation</strong>, <strong>désinsectisation,</strong> </a><strong><a href=\"/les-3d/p-4\">désinfection</a>,<strong> nettoyage, débarras</strong></strong><strong>, traitement des bois et des maçonneries</strong>. Nous vous proposons également des solutions <strong><a href=\"/divers/p-8\">anti-pigeons</a></strong>.</p><p><img style=\"float: left; margin: 0px 10px 10px 0px;\" alt=\"\" src=\"/files/2015/12/certificat568107244572e.png\"></p><p>Pour répondre à tous vos besoins, nous luttons efficacement et durablement contre les<strong> rongeurs</strong> (souris, rats) et les <strong>insectes</strong> (blattes/cafards, puces, punaises ou fourmis, ...) car ces invasions sont très génantes et qu\'elles peuvent porter atteinte à votre santé. Notre équipe de professionnels possède le matériel et les produits nécessaires pour vous assurer un résultat optimal.</p><p>Le <strong></strong><strong></strong><strong><a href=\"/traitement-bois-maconneries/p-10\">traitement des bois et maçonneries</a></strong> consiste aux <strong>traitements des champignons</strong> (mérule, etc) et des i<strong>nsectes à larves xylophages </strong>(vrillettes, ...).</p><p>Nous pouvons également vous apporter des solutions contre vos <strong>problèmes d\'<a href=\"/humidite/p-5\">humidité</a> </strong>(remontées capillaires).<br></p><p> Nous mettons également à votre services la compétence de nos applicateurs pour traiter votre environnement contre les <strong>nids de guêpes</strong><strong>.</strong></p><p> Afin d\'assurer la pérennité de votre habitat, nous vous garantissons la <strong>maintenance</strong> et l<strong>\'entretien</strong> des systèmes de ventillation :<a href=\"/vmc-vs/p-6\"> </a><strong><a href=\"/vmc-vs/p-6\"> Ventilations Mécaniques Controlées (VMC)</a> </strong>et <strong><a href=\"/vmc-vs/p-6\">Ventilations Statiques (VS)</a></strong><strong>.</strong></p><p> Nous vous invitons à <strong><a href=\"/contact/p-3\"> contacter</a></strong> nos spécialistes pour toutes interventions de dératisation, désinsectisation, désinfection, traitement des bois et maçonnerie au <span style=\"font-size:21px;\">03 20 26 40 51</span>.</p>',NULL,NULL,NULL,'2016-03-17 17:05:17','2015-12-03 14:49:19',NULL,'home','',0,NULL,1,2),(2,'page','publish','2015-12-03',1,0,'Plan','Plan','DDH à Tourcoing, dans le Nord (59): Plan d’accès','/plan/p-2','','','Situez et découvrez l\'univers de DDH, effectuant ces services à Tourcoing près de Lille dans le Nord','Dératisation- Tourcoing Lille Nord 59- Plan d\'accès - DDH Dératiseur','','',NULL,'','<p> <strong>DDH</strong> vous reçoit chaleureusement, dans <strong>le Nord</strong> (<strong>59</strong>) dans son établissement pour pouvoir vous renseigner et répondre à vos attentes:<a href=\"/les-3d/p-4\"> </a><strong><a href=\"/les-3d/p-4\">Les 3D</a></strong>,<a href=\"/vmc-vs/p-6\"> </a><strong><a href=\"/vmc-vs/p-6\">Vmc et Vs</a></strong>, <strong><a href=\"/traitement-bois-maconneries/p-10\">Traitements</a></strong>,<strong> <a href=\"/humidite/p-5\">Humidité</a></strong> et<a href=\"/divers/p-8\"> </a><strong><a href=\"/divers/p-8\">Activités annexes</a></strong>.</p><p> Nous sommes situés au 237 rue Ma Campagne, 59200 TOURCOING.</p><p> Pour une facilité d\'accès chez<strong> DDH</strong>, vous pouvez consulter notre position géographique grâce au plan ci-dessous.</p>  <p> Prenez<strong><a href=\"/contact/p-3\"> contact </a></strong>avec nous pour toute information complémentaire sur nos services.</p>',NULL,NULL,NULL,'2016-03-17 17:05:17','2015-12-03 14:50:43',NULL,'page','plan',6,NULL,3,4),(3,'page','publish','2015-12-03',1,0,'Contact','Contact','Contactez nous pour toutes informations complémentaires','/contact/p-3','','','Contactez et découvrez l\'univers de DDH, effectuant ces services à Tourcoing près de Lille dans le Nord.','Dératisation -Tourcoing Lille Nord Contact 59- DDH France','','',NULL,'','<p>\n	 Après avoir parcouru le site, vous avez encore besoin d\'informations concernant nos différents services (<a href=\"/les-3d/p-4\">\n	</a><strong><a href=\"/les-3d/p-4\">Les 3D</a></strong>,<a href=\"/vmc-vs/p-6\"> </a><strong><a href=\"/vmc-vs/p-6\">Vmc et Vs</a></strong>, <strong><a href=\"/traitement-bois-maconneries/p-10\">Traitement</a><i><a href=\"/traitement-bois-maconneries/p-10\">s</a></i></strong>, <strong><a href=\"/humidite/p-5\">Humidité</a></strong> et <strong><a href=\"/divers/p-8\">Activités annexes</a></strong>)? N\'hésitez pas à nous contacter par téléphone ou via le formulaire de contact ci-dessous, nous serons ravis de vous accompagner dans votre démarche!\n</p><p>\n	<br>\n</p><p style=\"text-align: center;\">\n	<strong><span data-redactor-tag=\"span\" style=\"font-size:24px;\"><a href=\"/\">DDH</a></span></strong>\n</p><p style=\"text-align: center;\">\n	<span style=\"font-size:21px;\" data-redactor-tag=\"span\">\n	237 rue Ma Campagne<br>\n 59200 TOURCOING\n	<br>\n Tel.: 03 20 26 40 51\n	<br>\n Fax.: 03 20 26 48 69<span></span></span>\n</p>',NULL,NULL,NULL,'2016-03-17 17:05:17','2015-12-03 14:50:53',NULL,'contact','contact',7,NULL,5,6),(4,'page','publish','2015-12-03',1,0,'Les 3D','Les 3D','Les 3D','/les-3d/p-4','','','Découvrez la dératisation, la désinsectisation et la désinfection ainsi que l\'univers de DDH, effectuant ces services à Tourcoing près de Lille dans le Nord','Dératisation Désinsectisation Désinfection  - Tourcoing Lille Nord 59 - DDH France','','',NULL,'','',NULL,NULL,NULL,'2016-03-17 17:05:17','2015-12-03 14:51:22',NULL,'page','les-3d',1,NULL,7,8),(5,'page','publish','2015-12-03',1,0,'Humidité','Humidité','Humidité','/humidite/p-5','','','Découvrez le traitement du bois et l\'univers de DDH, effectuant ces services à Tourcoing près de Lille dans le Nord','Traitement du bois - Tourcoing Lille Nord 59 - DDH Dératiseur','','',NULL,'','',NULL,NULL,NULL,'2016-03-17 17:05:17','2015-12-03 14:52:03',NULL,'page','humidite',2,NULL,9,10),(6,'page','publish','2015-12-03',1,0,'VMC & VS','VMC & VS','VMC & VS','/vmc-vs/p-6','','','Découvrez les prestations sur VMC et Vs ainsi que l\'univers de DDH, effectuant ces services à Tourcoing près de Lille dans le Nord','VMC et Vs - Tourcoing Lille Nord 59 - DDH France','','',NULL,'','<p> <strong>DDH</strong> s\'occupe de l\'entretien complet et des réparations de toutes vos installations de ventilation.</p>',NULL,NULL,NULL,'2016-03-17 17:05:17','2015-12-03 14:52:18',NULL,'page','vmc-vs',4,NULL,11,12),(8,'page','publish','2015-12-03',1,0,'Divers','Divers','Activités annexes','/divers/p-8','','','Découvrez les activités annexes et l\'univers de DDH, effectuant ces services à Tourcoing près de Lille dans le Nord','Activités annexes - Tourcoing Lille Nord 59 - DDH Dératiseur','','',NULL,'','',NULL,NULL,NULL,'2016-04-11 13:47:20','2015-12-03 14:53:23',NULL,'page','divers',5,NULL,13,14),(9,'page','publish','2015-12-03',NULL,0,'Mentions légales','Mentions légales','','/mentions-legales/p-9','','','','','','',NULL,'','<p>En vertu de l\'article 6 de la loi n° 2004-575 du 21 juin 2004 pour la confiance dans l\'économie numérique, il est précisé aux utilisateurs du site http://www.ddhfrance.com/<span></span> l\'identité des différents intervenants dans le cadre de sa réalisation et de son suivi :<br><strong>Propriétaire Mr FELIX Régis</strong><br><strong>Webdesign :</strong><a href=\"http://www.kobaltis.com/\" target=\"_blank\"> Agence KOBALTIS</a> – <strong>Développements :</strong><a href=\"http://www.kobaltis.com/\" target=\"_blank\"> KOBALTIS</a><br><strong>Responsable publication : </strong>kobaltis-webmaster@kobaltis.com<br>Le responsable publication est une personne physique ou une personne morale.<br><strong>Hébergeur :</strong> OVH – 2 rue Kellermann 59100 Roubaix – France</p><p><br><br><strong>2. Conditions générales d\'utilisation du site et des services proposés.</strong><br>L\'utilisation du site http://www.ddhfrance.com/<span></span> implique l\'acceptation pleine et entière des conditions générales d\'utilisation ci-après décrites. Ces conditions d\'utilisation sont susceptibles d\'être modifiées ou complétées à tout moment, les utilisateurs du site http://www.ddhfrance.com/<span></span> sont donc invités à les consulter de manière régulière. Ce site est normalement accessible à tout moment aux utilisateurs. Une interruption pour raison de maintenance technique peut être toutefois décidée par Matra Electronique. Le site http://www.ddhfrance.com/<span></span> est mis à jour régulièrement. De la même façon, les mentions légales peuvent être modifiées à tout moment : elles s\'imposent néanmoins à l\'utilisateur qui est invité à s\'y référer le plus souvent possible afin d\'en prendre connaissance.<br>Les copies et téléchargements ne sont autorisées que pour un usage personnel, privé et non-commercial.</p><p><br><br><strong>3. Description des services fournis.</strong> Le site http://www.ddhfrance.com/<span></span> a pour objet de fournir une information concernant l\'ensemble des activités de la société.<br>http://www.ddhfrance.com/<span></span> possible. Toutefois, il ne pourra être tenue responsable des omissions, des inexactitudes et des carences dans la mise à jour, qu\'elles soient de son fait ou du fait des tiers partenaires qui lui fournissent ces informations. Tous les informations indiquées sur le site http://www.ddhfrance.com/<span></span> sont données à titre indicatif, et sont susceptibles d\'évoluer. Par ailleurs, les renseignements figurant sur le site http://www.ddhfrance.com/<span></span> ne sont pas exhaustifs. Ils sont donnés sous réserve de modifications ayant été apportées depuis leur mise en ligne.</p><p><br><br><strong>4. Limitations contractuelles sur les données techniques.</strong><br>KOBALTIS ne pourra être tenu responsable de dommages matériels liés à l\'utilisation du site. De plus, l\'utilisateur du site s\'engage à accéder au site en utilisant un matériel récent, ne contenant pas de virus et avec un navigateur de dernière génération mis-à-jour.</p><p><br><br><strong>5. Propriété intellectuelle et contrefaçons.</strong> <strong>Mr FELIX Régis</strong><span></span> est propriétaire des droits de propriété intellectuelle ou détient les droits d\'usage sur tous les éléments accessibles sur le site, notamment les textes, images, graphismes, logo, icônes, sons, logiciels.<br>Toute reproduction, représentation, modification, publication, adaptation de tout ou partie des éléments du site, quel que soit le moyen ou le procédé utilisé, est interdite, sauf autorisation écrite préalable de : ************.<br>Toute exploitation non autorisée du site ou de l\'un quelconque des éléments qu\'il contient sera considérée comme constitutive d\'une contrefaçon et poursuivie conformément aux dispositions des articles L.335-2 et suivants du Code de Propriété Intellectuelle.</p><p><br><br><strong>6. Limitations de responsabilité.</strong> <strong>Mr FELIX Régis</strong><span></span> ne pourra être tenue responsable des dommages directs et indirects causés au matériel de l\'utilisateur, lors de l\'accès au site http://www.ddhfrance.com/<span></span>, et résultant soit de l\'utilisation d\'un matériel ne répondant pas aux spécifications indiquées au point 4, soit de l\'apparition d\'un bug ou d\'une incompatibilité.<br><strong>Mr FELIX Régis</strong><span></span> ne pourra également être tenue responsable des dommages indirects (tels par exemple qu\'une perte de marché ou perte d\'une chance) consécutifs à l\'utilisation du site http://www.ddhfrance.com/<span></span></p><p><br><br><strong>7. Gestion des données personnelles.</strong> En France, les données personnelles sont notamment protégées par la loi n° 78-87 du 6 janvier 1978, la loi n° 2004-801 du 6 août 2004, l\'article L. 226-13 du Code pénal et la Directive Européenne du 24 octobre 1995.<br>A l\'occasion de l\'utilisation du site http://www.ddhfrance.com/<span></span>, peuvent être recueillies : l\'URL des liens par l\'intermédiaire desquels l\'utilisateur a accédé au site http://www.ddhfrance.com/<span></span>, le fournisseur d\'accès de l\'utilisateur, l\'adresse de protocole Internet (IP) de l\'utilisateur.<br>En tout état de cause http://www.ddhfrance.com/<span></span> ne collecte des informations personnelles relatives à l\'utilisateur que pour le besoin de certains services proposés par le site http://www.ddhfrance.com/<span></span>. L\'utilisateur fournit ces informations en toute connaissance de cause, notamment lorsqu\'il procède par lui-même à leur saisie. Il est alors précisé à l\'utilisateur du site http://www.ddhfrance.com/<span></span> l\'obligation ou non de fournir ces informations.<br>Conformément aux dispositions des articles 38 et suivants de la loi 78-17 du 6 janvier 1978 relative à l\'informatique, aux fichiers et aux libertés, tout utilisateur dispose d\'un droit d\'accès, de rectification et d\'opposition aux données personnelles le concernant, en effectuant sa demande écrite et signée, accompagnée d\'une copie du titre d\'identité avec signature du titulaire de la pièce, en précisant l\'adresse à laquelle la réponse doit être envoyée.<br>Aucune information personnelle de l\'utilisateur du site http://www.ddhfrance.com/<span></span> n\'est publiée à l\'insu de l\'utilisateur, échangée, transférée, cédée ou vendue sur un support quelconque à des tiers.</p><p><br><br><strong>8. Liens hypertextes et cookies.</strong><br>Le site v contient un certain nombre de liens hypertextes vers d\'autres sites, mis en place avec l\'autorisation de <strong>Mr FELIX Régis</strong><span></span>. Cependant, <strong>Mr FELIX Régis</strong><span></span> n\'a pas la possibilité de vérifier le contenu des sites ainsi visités, et n\'assumera en conséquence aucune responsabilité de ce fait.<br>La navigation sur le site http://www.ddhfrance.com/<span></span> est susceptible de provoquer l\'installation de cookie(s) sur l\'ordinateur de l\'utilisateur. Un cookie est un fichier de petite taille, qui ne permet pas l\'identification de l\'utilisateur, mais qui enregistre des informations relatives à la navigation d\'un ordinateur sur un site. Les données ainsi obtenues visent à faciliter la navigation ultérieure sur le site, et ont également vocation à permettre diverses mesures de fréquentation.</p><p><br><br><strong>9. Droit applicable et attribution de juridiction.</strong> Tout litige en relation avec l\'utilisation du site http://www.ddhfrance.com/<span></span> est soumis au droit français. Il est fait attribution exclusive de juridiction aux tribunaux compétents de Lille.</p><p><br><br><strong>10. Les lois concernées.</strong> Loi n° 78-87 du 6 janvier 1978, notamment modifiée par la loi n° 2004-801 du 6 août 2004 relative à l\'informatique, aux fichiers et aux libertés.<br>Loi n° 2004-575 du 21 juin 2004 pour la confiance dans l\'économie numérique.<br>Informations personnelles : « les informations qui permettent, sous quelque forme que ce soit, directement ou non, l\'identification des personnes physiques auxquelles elles s\'appliquent » (article 4 de la loi n° 78-17 du 6 janvier 1978).</p>',NULL,NULL,NULL,'2016-03-17 16:05:33','2015-12-03 17:21:38',NULL,'mention_legale','',8,NULL,15,16),(10,'page','publish','2015-12-28',1,0,'Traitements bois et maçonneries','Traitements bois et maçonneries','','/traitement-bois-maconneries/p-10','','','','','','',NULL,'','<h2>Traitements des bois</h2><p> Insectes à larves xylophages : vrillettes, capricornes, lyctus </p><p> Traitement des champignons : mérule, coprins, pézizes </p><h2>Traitements des maçonneries</h2><p> Sous certaines conditions (fuites, humidité, vétusté) des champignons peuvent se développer au sein de votre maçonnerie. Il est important de traiter ce problème rapidement car les champignons peuvent porter atteinte à la structure de votre habitation.<br> </p>',NULL,NULL,NULL,'2016-03-17 17:05:17','2015-12-28 09:59:36',NULL,'page','traitement-bois-maconneries',3,NULL,17,18);
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parametres`
--

DROP TABLE IF EXISTS `parametres`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parametres` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `raison_sociale` varchar(128) DEFAULT NULL,
  `about` text,
  `contact_name` varchar(128) DEFAULT NULL,
  `contact_email` varchar(128) DEFAULT NULL,
  `telephone` varchar(128) DEFAULT NULL,
  `adresse_1` varchar(128) DEFAULT NULL,
  `adresse_2` varchar(128) DEFAULT NULL,
  `cp` varchar(128) DEFAULT NULL,
  `ville` varchar(128) DEFAULT NULL,
  `ndd` varchar(128) DEFAULT NULL,
  `telephone_noreply` varchar(128) DEFAULT NULL,
  `email_noreply` varchar(128) DEFAULT NULL,
  `email_name` varchar(128) DEFAULT NULL,
  `email_serveur_smtp` varchar(128) DEFAULT NULL,
  `email_serveur_port` varchar(128) DEFAULT NULL,
  `email_serveur_login` varchar(128) DEFAULT NULL,
  `email_serveur_mdp` varchar(128) DEFAULT NULL,
  `lien_facebook` varchar(128) DEFAULT NULL,
  `lien_twitter` varchar(128) DEFAULT NULL,
  `lien_linkedin` varchar(128) DEFAULT NULL,
  `lien_youtube` varchar(128) DEFAULT NULL,
  `lien_pinterest` varchar(128) DEFAULT NULL,
  `lien_google_plus` varchar(128) DEFAULT NULL,
  `active_brochure` tinyint(1) DEFAULT NULL,
  `google_ua` varchar(128) DEFAULT NULL,
  `google_login` varchar(128) DEFAULT NULL,
  `google_mdp` varchar(128) DEFAULT NULL,
  `contact_page` varchar(128) DEFAULT NULL,
  `updated` datetime NOT NULL,
  `siren` varchar(255) DEFAULT NULL,
  `hebergeur` varchar(255) DEFAULT NULL,
  `resp_publication` varchar(255) DEFAULT NULL,
  `url_site_web` varchar(255) DEFAULT NULL,
  `google_tag_manager` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parametres`
--

LOCK TABLES `parametres` WRITE;
/*!40000 ALTER TABLE `parametres` DISABLE KEYS */;
INSERT INTO `parametres` VALUES (1,'DDH',NULL,'Mr Félix Régis','remyddh@orange.fr','03 20 26 40 51','237 rue Ma Campagne',NULL,'59200','TOURCOING',NULL,'03/20/26/40/51','remyddh@orange.fr',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-12-03 17:06:56',NULL,NULL,NULL,NULL,'<script>\n  (function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){\n  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),\n  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)\n  })(window,document,\'script\',\'//www.google-analytics.com/analytics.js\',\'ga\');\n\n  ga(\'create\', \'UA-21345789-9\', \'auto\');\n  ga(\'send\', \'pageview\');\n\n</script>');
/*!40000 ALTER TABLE `parametres` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `partenaire_types`
--

DROP TABLE IF EXISTS `partenaire_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partenaire_types` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `etat_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partenaire_types`
--

LOCK TABLES `partenaire_types` WRITE;
/*!40000 ALTER TABLE `partenaire_types` DISABLE KEYS */;
/*!40000 ALTER TABLE `partenaire_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `partenaires`
--

DROP TABLE IF EXISTS `partenaires`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partenaires` (
  `id` int(11) NOT NULL,
  `partenaire_type_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `url` varchar(255) NOT NULL,
  `etat_id` int(11) NOT NULL,
  `position` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partenaires`
--

LOCK TABLES `partenaires` WRITE;
/*!40000 ALTER TABLE `partenaires` DISABLE KEYS */;
/*!40000 ALTER TABLE `partenaires` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content_type_id` varchar(128) DEFAULT NULL,
  `etat_id` varchar(32) NOT NULL,
  `rubrique_id` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `h1` varchar(255) NOT NULL,
  `resume` varchar(255) NOT NULL,
  `prix_ttc` decimal(10,2) NOT NULL,
  `description` text NOT NULL,
  `vignette` text NOT NULL,
  `galerie` text NOT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `taches`
--

DROP TABLE IF EXISTS `taches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `taches` (
  `id` int(11) NOT NULL,
  `plugin` varchar(256) NOT NULL,
  `controller` varchar(256) NOT NULL,
  `action` varchar(256) NOT NULL,
  `fond` int(11) NOT NULL DEFAULT '1',
  `author` varchar(256) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `etat` int(11) NOT NULL DEFAULT '0' COMMENT '0 : en attente, 1 : tache commencee, 2 taches terminee',
  `donnees` text NOT NULL,
  `resultat` longtext NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `taches`
--

LOCK TABLES `taches` WRITE;
/*!40000 ALTER TABLE `taches` DISABLE KEYS */;
/*!40000 ALTER TABLE `taches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tests`
--

DROP TABLE IF EXISTS `tests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `updated` datetime NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tests`
--

LOCK TABLES `tests` WRITE;
/*!40000 ALTER TABLE `tests` DISABLE KEYS */;
/*!40000 ALTER TABLE `tests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `txts`
--

DROP TABLE IF EXISTS `txts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `txts` (
  `id` varchar(128) CHARACTER SET latin1 NOT NULL,
  `description` text CHARACTER SET latin1 NOT NULL,
  `txt` text CHARACTER SET latin1 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `txts`
--

LOCK TABLES `txts` WRITE;
/*!40000 ALTER TABLE `txts` DISABLE KEYS */;
/*!40000 ALTER TABLE `txts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `typepages`
--

DROP TABLE IF EXISTS `typepages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `typepages` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `typepages`
--

LOCK TABLES `typepages` WRITE;
/*!40000 ALTER TABLE `typepages` DISABLE KEYS */;
INSERT INTO `typepages` VALUES (1,'Page d\'accueil','home','Liste toutes les pages métiers'),(2,'Page classique','page',''),(3,'Page métier','metier',''),(4,'Blog (liste des articles)','blog','Liste toutes les actualités'),(5,'Page contact','contact',''),(6,'Accueil liste des metiers','home_metier','Liste toutes les pages de type métier'),(7,'Page d\'accueil références','realisation','Liste toutes les actualités flaguées \"références\"'),(8,'Mention légale','mention_legale','');
/*!40000 ALTER TABLE `typepages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_groups`
--

DROP TABLE IF EXISTS `user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_groups`
--

LOCK TABLES `user_groups` WRITE;
/*!40000 ALTER TABLE `user_groups` DISABLE KEYS */;
INSERT INTO `user_groups` VALUES (2,2,2),(4,3,2),(26,4,2),(40,607123,1),(42,607130,1),(43,607125,1),(44,5,1);
/*!40000 ALTER TABLE `user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` mediumint(8) unsigned NOT NULL,
  `nom_connexion` varchar(256) NOT NULL,
  `nom` varchar(256) NOT NULL,
  `prenom` varchar(256) NOT NULL,
  `nom_entreprise` varchar(256) NOT NULL,
  `email` varchar(256) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `mobile` varchar(32) NOT NULL,
  `password` char(40) NOT NULL,
  `vignette` text NOT NULL,
  `resume` text NOT NULL,
  `body` text NOT NULL,
  `disabled` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `verified` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (2,1,'Jerome Lebleu','Lebleu','Jerome','','jerome@whatson-web.com','','','8cfaeb35eeb36111e2c70cc824fe92d5d46903db','','','',0,1,'2015-12-02 22:15:17'),(3,1,'Prod Kobaltis','Kobaltis','Prod','','prod@kobaltis.net','','','fe2af495788471d1dcc3146601b19e630e58bf87','','','',0,1,'2015-12-02 22:15:17'),(4,2,'Redacteur Kobaltis','Kobaltis','Redacteur','','redacteur@kobaltis.net','','','fe2af495788471d1dcc3146601b19e630e58bf87','','','',0,1,'2015-12-02 22:15:17'),(5,1,'Rémy Félix','Félix','Rémy','','remyddh@orange.fr','','','215d03ba33fec6297f8786653b75811e17f5d973','','','',0,1,'2016-02-05 17:33:56');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-02-25 11:56:01
