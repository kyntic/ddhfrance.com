<?php

App::import('Component', 'Auth');

/**
 * Class AppAuthComponent
 */
class AppAuthComponent extends AuthComponent
{
	/**
	 * Configuration par défaut
	 *
	 * @var array
	 */
	var $defaults = array(
		'userScope'      => array(),
		'fields'         => null,
		'loginAction'    => null,
		'loginRedirect'  => null,
		'logoutRedirect' => null,
		'autoRedirect'   => true,
		'loginError'     => "Identifiant ou mot de passe incorrects.",
		'authError'      => "Vous n'avez pas accès à cette page.",
		'flashElement'   => 'default',
	);
 
	/**
	 * Configurations possibles en fonction du préfixe de la route
	 *
	 * @var array
	 */
	var $configs = array(
		'admin' => array(
			'loginAction' => array(
	        	'admin' => false, 
	            'controller' => 'users',
	            'action' => 'identification',
	            'plugin' => null
	        ),
	        'authorize' => 'Controller', 
	        'authError' => '<strong>Veuillez vous identifier</strong>',
	        'logoutRedirect' => '/users/identification', 
	        'authenticate' => array(
	            'Form' => array(
	                'fields' => array('username' => 'email'), 
	                'scope'	 => array('User.disabled' => 0, 'User.group_id' => 1) //Groupe des administrateurs
	            )
	        )
		),
		'membre' => array(
			'loginAction' => array(
	        	'membre' 		=> false, 
	            'controller' 	=> 'users',
	            'action' 		=> 'login', 
	            'plugin' 		=> null
	        ),
	        'logoutRedirect' => '/users/login', 
	        'authorize' => 'Controller', 
	        'authError' => '<strong>Cette section n’est pas accessible, veuillez vous identifier. </strong>',
	        'authenticate' => array(
	            'Form' => array(
	                'userModel' => 'User', 
	                'fields' => array('username' => 'email', 'password' => 'password'), 
	                'scope'	 => array('User.disabled' => 0, 'User.verified' => 1)
	            )
	        )
		)
	);
 
	/**
	 * Démarrage du composant.
	 * Autorisation si pas de préfixe dans la Route qui a conduit ici.
	 *
	 * @param object $controller Le contrôleur qui a appelé le composant.
	 */
	function startup(&$controller)
	{
		$prefix = null;
 
		if(empty($controller->params['prefix']))
		{
			$this->allow();
		}
		else
		{
			$prefix = $controller->params['prefix'];
		}

		if(empty($prefix)) $prefix = 'membre';

		//debug($prefix); exit();
 		

		// Cas spécial des actions de login et logout, pour lesquelles le préfixe n'existe pas
		if(in_array($controller->action, array('login', 'logout')))
		{
			$prefix = 'membre';
			
		}

		if(in_array($controller->action, array('identification', 'deconnexion')))
		{
			$prefix = 'admin';
			
		}
 
		$this->_setup($prefix);

		parent::startup($controller);
	}
 
	/**
	 * Définition des variables de config en fonction d'un préfixe
	 *
	 * @param string $prefix
	 */
	function _setup($prefix)
	{

		$settings = $this->defaults;
 
		if(array_key_exists($prefix, $this->configs))
		{
			$settings = array_merge($settings, $this->configs[$prefix]);
		}
 
		$this->_set($settings);
	}
}
?>