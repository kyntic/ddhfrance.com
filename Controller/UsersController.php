<?php

/**
 * controllers/UsersController.php
 */
class UsersController extends AppController
{
    /**
     * Lost password
     *
     * @return void
     */
    public function motdepasseperdu()
    {

        if (!empty($this->request->data)) {

            $data = $this->request->data;

            $user = $this->User->findByEmail($data['User']['email']);

            if (!empty($user)) {

                $this->User->envoyer_compte_perdu($user['User']['id']);
                $this->Session->setFlash('Un mail avec votre nouveau mot de passe vous a été envoyé', 'success');

                $this->redirect('/');

            } else {

                $this->Session->setFlash('Aucun compte n\'est associé à cet email', 'error');
            }
        }
    }

    /**
     * Logout
     *
     * @return void
     */
    public function logout()
    {
        /**
         * Destroy local session
         */
        $this->Auth->logout();

        /**
         * Simple Saml logout
         */
        $this->Saml->logout('/');
    }

    /**
     * Check if user is authenticated by identity federation
     *
     * @return json
     */
    public function isAuthenticatedByFederation()
    {
        $response = array();

        /**
         * Get the user
         */
        $user = AuthComponent::user();

        if ($user == null) {

            $response['error'] = true;

            exit(json_encode($response));
        }

        /**
         * No error | User is authenticated
         */
        $response['error']  = false;
        $response           = $user;

        exit(json_encode($response));
    }


    /******************************************************************************
     *
     * /* ESPACE MEMBRE :
     *
     * /******************************************************************************/
    public function membre_home()
    {

    }


    /******************************************************************************
     *
     * /* ADMINISTRATION :
     *
     * /******************************************************************************/

    /**
     * Admin home
     */
    public function admin_home()
    {

    }

    public function admin_test()
    {
        /**
         * The model we need
         */
        $this->loadModel('Ldap.LdapGroup');

        /**
         * Get the groups
         */
        $groups = $this->LdapGroup->createJsonTree();

        return $groups;
    }

    /**
     * Liste des users
     */
    function admin_index()
    {
        /**
         * The model we need
         */
        $this->loadModel('Group');

        $response['Users'] = $this->User->find(
            'all',
            array(
                'order' => 'User.nom ASC'
            )
        );

        $response['Groups'] = $this->Group->find(
            'list',
            array(
                'conditions' => array(
                    'Group.id !=' => 1
                ),
                'fields' => array(
                    'Group.id',
                    'Group.name'
                )
            )
        );

        echo json_encode($response);

        exit();
    }

    /**
     * Envoi mail d'inscription
     *
     * @param $id
     */
    public function admin_envoyer_email($id)
    {
        $response['ok'] = false;

        if ($this->User->envoyer_compte_perdu($id)) {

            $response['ok'] = true;
        }

        echo json_encode($response);

        exit();
    }

    /**
     * Edition d'un profil
     *
     * @param null $id
     */
    public function admin_edit($id = null)
    {
        $this->loadModel('Group');
        $this->loadModel('UserGroup');

        //Enregistrement des données :
        if (!empty($this->data)) {

            $data = $this->data;

            $data['User']['verified'] = true;

            $response['ok'] = false;

            $this->User->set($data);

            $validate = array('nom', 'prenom', 'email');

            $new_password = false;

            if (!empty($data['User']['motdepasse'])) {

                $validate[] = 'motdepasse';
                $validate[] = 'confirm_password';

                $new_password = true;

            }

            if (!$new_password) unset($data['User']['motdepasse']);

            if ($this->User->validates(array('fieldList' => $validate))) {

                $this->User->save($data, array('validate' => false));

                $id = $this->User->id;

                $this->UserGroup->deleteAll(array('UserGroup.user_id' => $id));

                $admin = false;

                if (!empty($data['UserGroup'])) {

                    foreach ($data['UserGroup'] as $v) {

                        if ($v == 1) $admin = true;

                        $this->UserGroup->save(
                            array(
                                'UserGroup' => array(
                                    'id'        => null,
                                    'user_id'   => $id,
                                    'group_id'  => $v
                                )
                            )
                        );
                    }

                    if ($admin) {
                        $this->User->saveField('group_id', 1);
                    } else {
                        $this->User->saveField('group_id', 0);
                    }
                }

                $response['ok'] = true;
                $response['id'] = $id;

            } else {

                $response['ok'] = false;
                $response['errors']['User'] = $this->User->invalidFields();

            }

            echo json_encode($response);
            exit();

        } elseif ($id) {

            $response['Data'] = $this->User->findById($id);
            $response['Data']['UserGroup'] = array_values($this->UserGroup->find('list', array('fields' => array('UserGroup.group_id'), 'conditions' => array('UserGroup.user_id' => $id))));

            unset($response['Data']['User']['password']);
        }

        $response['Groups'] = $this->User->Group->find('list', array('fields' => array('Group.id', 'Group.name')));

        echo json_encode($response);
        exit();
    }

    /**
     * Supprimer un user
     */
    public function admin_delete($id)
    {
        $this->loadModel('UserGroup');

        $this->User->delete($id);
        $this->User->deleteAll(array('UserGroup.user_id' => $id));

        $this->Session->setFlash('Utilisateur supprimé', 'success');
        $this->redirect('/admin/users');
    }

    /**
     * Identification administrateur
     */
    public function admin_login()
    {


        if (!empty($this->request->data)) {

            $this->Auth->logout();


            if ($this->Auth->login()) {


                $this->redirect('/admin');

                exit(json_encode(AuthComponent::User()));

            } else {

                echo 'Non reconnu';
                exit();

            }

            exit();
        }



        if ($this->request->is('ajax')) {

            header('HTTP/1.0 401 Unauthorized');
            exit();
        }

        $this->layout = 'admin_login';
    }

    /**
     * Admin logout
     *
     * @return void
     */
    public function admin_logout()
    {
        $this->redirect($this->Auth->logout());
    }
}