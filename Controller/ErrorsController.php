<?php

/**
 * Class ErrorsController
 */
class ErrorsController extends AppController
{
    /**
     * The controller name
     *
     * @var string $name
     */
    public $name = 'Errors';

    /**
     * Before filter
     *
     * @return void
     */
    public function beforeFilter()
    {
        parent::beforeFilter();

        $this->Auth->allow('error404');
    }

    /**
     * Error 404
     *
     * @return void
     */
    public function error404()
    {
        $this->layout = 'default';
    }

    /**
     * Error 400
     *
     * @return void
     */
    public function error400()
    {
        $this->layout = 'default';
    }
}