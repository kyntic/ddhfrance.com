App.controller('PagesIndexCtrl', ['$scope', '$log', '$rootScope', '$http', '$state', 'Page','Typepage', 'toaster', function ($scope, $log, $rootScope, $http, $state, Page,Typepage, toaster) {

    'use strict';

    /**
     * Liste des fichiers
     * @type {Array}
     */
    $scope.Pages = [];

    $scope.loadPages = function (parent_id) {

        $rootScope.loading = true;

        $http.get(WH_ROOT + '/admin/pages/index/' + parent_id)
            .success(function (data) {

                $scope.Page         = data;
                $rootScope.loading  = false;

            }).error(function () {

                $rootScope.loading = false;
            });
    };

    $scope.loadPages($state.params.page_id);

    /**
     * Ordre
     */
    $scope.sortableOptions = {
        stop: function (e, ui) {

            var nouvelOrdre = $scope.Page.Childrens.map(function (i) {
                return i.Page.id;
            }).join(',');

            Page.ordre(nouvelOrdre).then(
                function (data) {

                    toaster.pop('success', 'L’ordre des pages a été modifié');

                },
                function (data) {

                    $scope.log = data;

                    toaster.pop('danger', 'Une erreur est survenue');
                }
            );
        }
    };

    /**
     * Delete
     */
    $scope.delete = function (index) {

        if (confirm('Etes vous sûre de vouloir supprimer cette page ? ')) {

            $rootScope.loading = true;

            Page.delete($scope.Page.Childrens[index].Page.id).then(function (reponse) {

                $rootScope.loading = false;

                toaster.pop('success', 'Page supprimée', '');

                $scope.Page.Childrens.splice(index, 1);

            }, function (erreur) {

                toaster.pop('danger', 'Erreur', erreur);

                $rootScope.loading = false;
            });
        }
    };

    /**
     * TREE
     */
    $scope.visible = function (item) {

        if ($scope.query && $scope.query.length > 0
            && item.Page.name.indexOf($scope.query) == -1) {

            return false;
        }

        return true;
    };

    $scope.findNodes = function () {

    };

    $http.get(WH_ROOT + '/admin/pages/tree')
        .success(function (data) {

            $scope.data = data;
        })
        .error(function () {


        });
}]);