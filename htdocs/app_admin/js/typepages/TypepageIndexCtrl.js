App.controller('TypepagesIndexCtrl', ['$scope' , '$log', '$rootScope', '$http', '$modal', 'toaster','Typepage', '$state', function ($scope, $log, $rootScope, $http, $modal, toaster, Typepage, $state) {

    'use strict';
console.log('ici');
    $scope.log = '';

    $scope.typepages = [];

    $scope.init = function () {

        $scope.load();

    }


    $scope.load = function () {

        $rootScope.loading = true;

        Typepage.findAll().then(function (response) {

            if (response.statut == 1) {

                console.log(response);
                $scope.typepages = response.data;

            } else {

                console.log(response);
                $scope.log = response;

            }

            $rootScope.loading = false;

        }, function (error) {

            $scope.log = error;

            $rootScope.loading = false;

        });

    }

    /**
     * Delete
     */
    $scope.delete = function (index) {

        if (confirm('Etes vous sûre de supprimer ?')) {

            $http
                .post(WH_ROOT + '/admin/typepages/delete/' + $scope.typepages[index].Typepage.id)

                .then(function (response) {

                    $scope.typepages.splice(index, 1);
                    toaster.pop('success', 'Opération réussie', 'Type de page supprimée');

                }

            );

        }
        ;

    }


    /**
     * Edition d'un group
     */
    $scope.edit = function (index) {

        var modalInstance = $modal.open({
            templateUrl: '/typepage-edit.html',
            controller: ModalInstanceCtrl,
            resolve: {
                data: function () {

                    if (!isNaN(index)) {
                        return $scope.typepages[index];
                    } else {
                        return {};
                    }

                }
            }
        }).result.then(function () {

                $scope.load();

            });
    };

    // Please note that $modalInstance represents a modal window (instance) dependency.
    // It is not the same as the $modal service used above.

    var ModalInstanceCtrl = function ($scope, $modalInstance, data) {

        $scope.data = data;


        $scope.ok = function () {

            $http
                .post(WH_ROOT + '/admin/typepages/edit', $scope.data)

                .then(function (response) {

                    $modalInstance.close();

                }, function (x) {

                    $scope.log = x;

                }

            );


        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    };

    $scope.init();

}]);
