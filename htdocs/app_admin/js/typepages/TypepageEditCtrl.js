App.controller('TypepagesEditCtrl',
    ['$scope' , '$log', '$rootScope', '$http', '$state', 'toaster', 'Product', 'Page', 'Attribute', 'AttributeFamily',
        function($scope, $log, $rootScope, $http, $state, toaster, Product, Page, Attribute, AttributeFamily) {

    'use strict';

    $scope.log = '';

    $scope.data = {
        galerie :   {files:[]}
    };
    $scope.rubriquesOptions = [];
    $scope.attributeFamilies = [];

    $scope.init = function () {

        $scope.loadProduct();
        $scope.loadRubriquesOptions();
        $scope.loadAttributeFamilies();
    };

    /**
     * Charge le produit
     */
    $scope.loadProduct = function () {

        $rootScope.loading = true;

        Product.getById($state.params.product_id).then(function (response) {

            if (response.statut == 1) {

                if(!angular.isObject(response.data.Product.galerie)) {

                    response.data.Product.galerie = {files:[]};

                }

                $scope.data = response.data;

            } else if (response.statut == 0) {

                $scope.data = {};

            } else {

                $scope.log = response;

            }

            $rootScope.loading = false;

        }, function (error) {

            $scope.log = error;

            $rootScope.loading = false;

        });

    };

    /**
     * Charge la liste des options du select des familles d'attribut
     */
    $scope.loadRubriquesOptions = function () {

        $rootScope.loading = true;

        Page.getRubriquesOptions().then(function (response) {

            if (response.statut == 1) {

                $scope.rubriquesOptions = response.data;

            } else {

                $scope.log = response;

            }

            $rootScope.loading = false;

        }, function (error) {

            $scope.log = error;

            $rootScope.loading = false;

        });

    };

    /**
     * Deletes an element in the gallery
     * @param fileIndex
     */
    $scope.galDelete = function(fileIndex) {

        $scope.data.Product.galerie.files.splice(fileIndex, 1);
    };

    /**
     * Charge la liste des familles d'attribut
     */
    $scope.loadAttributeFamilies = function () {

        $rootScope.loading = true;

        AttributeFamily.getAssociables().then(function (response) {

            if (response.statut == 1) {

                $scope.attributeFamilies = response.data;

                angular.forEach($scope.attributeFamilies, function(value, key) {

                    value.attributeOptions = [];

                    Attribute.getOptionsMultiple(value.AttributeFamily.id).then(function (response) {

                        if (response.statut == 1) {

                            value.attributeOptions = response.data;

                        } else {

                            $scope.log = response;

                        }

                        $rootScope.loading = false;

                    }, function (error) {

                        $scope.log = error;

                        $rootScope.loading = false;

                    });

                });

            } else {

                $scope.log = response;

            }

            $rootScope.loading = false;

        }, function (error) {

            $scope.log = error;

            $rootScope.loading = false;

        });

    };

    /**
     * Charge la liste des options du select des familles d'attribut
     */
    $scope.loadAttributesOptions = function () {

        $rootScope.loading = true;

        Attribute.getOptionsMultiple().then(function (response) {

            if (response.statut == 1) {

                $scope.attributesOptions = response.data;

            } else {

                $scope.log = response;

            }

            $rootScope.loading = false;

        }, function (error) {

            $scope.log = error;

            $rootScope.loading = false;

        });

    };

    /**
     * Enregistrement
     */
    $scope.submit = function() {

        $scope.log          = '';
        $rootScope.loading  = true;

        Product.save($scope.data).then(function(response) {

            if (response.statut == 1) {

                $scope.data.Product.id = response.data.Product.id;

                toaster.pop('success', 'Enregistrement effectué');

            } else {

                $scope.log = response;

            }

            $rootScope.loading = false;

        }, function (error) {

            $scope.log = error;

            $rootScope.loading = false;

        });

    };

    /**
     * Lancement de la fonction init
     */
    $scope.init();

}]);
