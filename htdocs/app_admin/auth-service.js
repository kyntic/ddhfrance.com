/**
 * AUTH SERCICE PERMET DE SECURISER L'INTERFACE
 * A CHAQUE REQUETTE $HTTP, si il retourne une erreur 401, il redirige vers login
 */

angular.module("AuthServices", [])



    .constant('AUTH_EVENTS', {
        loginSuccess    : 'auth-login-success',
        loginFailed     : 'auth-login-failed',
        logoutSuccess   : 'auth-logout-success',
        sessionTimeout  : 'auth-session-timeout',
        notAuthenticated: 'auth-not-authenticated',
        notAuthorized   : 'auth-not-authorized'
    })

    .config(function ($httpProvider) {
        $httpProvider.interceptors.push([
            '$injector',
            function ($injector) {

                return $injector.get('AuthInterceptor');
            }
        ]);
    })

    .factory('AuthInterceptor', function ($rootScope, $q, $location, AUTH_EVENTS, $window) {

        return {
            responseError: function (response) {

                if(response.status == 401) $window.location.href = WH_ROOT + '/admin/users/login';

                $rootScope.$broadcast({
                    302: AUTH_EVENTS.sessionTimeout,
                    401: AUTH_EVENTS.notAuthenticated,
                    403: AUTH_EVENTS.notAuthorized,
                    419: AUTH_EVENTS.sessionTimeout,
                    440: AUTH_EVENTS.sessionTimeout
                }[response.status], response);
                return $q.reject(response);
            }
        };
    })




    .service("SessionService", function() {

        this.setValue = function(key, value) {
            localStorage.setItem(key, value);
        };
        this.getValue = function(key) {
            return localStorage.getItem(key);
        };
        this.destroyItem = function(key) {
            localStorage.removeItem(key);
        };
    })
    .service("UserService", function($http, $location, SessionService, httpBufferService, $window) {



        this.currentUser = {
            name        : SessionService.getValue("User.name") || "",
            group_id    : SessionService.getValue("User.group_id") || false,
            isLoggedIn  : (SessionService.getValue("User.email") ? true : false)
        };

        this.login = function(data) {

            var _this = this;

            return $http.post(WH_ROOT + '/admin/users/login', data)

                .then(function(response) {


                    console.log(response);

                    if ( !response.data.id ) {

                        return false;

                    }else{

                        _this.currentUser.name = response.data.nom_connexion;
                        _this.currentUser.isLoggedIn = true;
                        SessionService.setValue("User.name", response.data.nom_connexion);
                        SessionService.setValue("User.email", response.data.email);
                        SessionService.setValue("User.group_id", response.data.group_id);
                        SessionService.setValue("User.id", response.data.id);

                        return true;

                    }


                }, function (x) {

                    return x.data;

                });
        };

        this.connected = function()
        {

            var _this = this;

            return $http.post(WH_ROOT + '/users/isauthenticatedbyfederation')

                .then(function(response) {

                    if ( !response.data.id ) {

                        return false;

                    } else {

                        _this.currentUser.name = response.data.nom_connexion;
                        _this.currentUser.isLoggedIn = true;
                        SessionService.setValue("User.name", response.data.nom_connexion);
                        SessionService.setValue("User.email", response.data.email);
                        SessionService.setValue("User.group_id", response.data.group_id);
                        SessionService.setValue("User.id", response.data.id);

                        return true;
                    }
                }, function (x) {

                    return x.data;

                });
        };

        this.logout = function() {
            var _this = this;


            return $http.post(WH_ROOT + "/users/logout").success(function() {

                _this.currentUser.isLoggedIn = false;

                SessionService.destroyItem("User.name");
                SessionService.destroyItem("User.group_id");
                SessionService.destroyItem("User.email");
                SessionService.destroyItem("User.id");

                $window.location.href = WH_ROOT + '/admin';

            });
        };


        this.loginShowing = false;

        this.setLoginState = function(state) {
            this.loginShowing = state;
        };
    })
    .factory("httpBufferService", function($injector) {

        var $http;
        var buffer = {};


        return {
            storeRequest: function(request) {
                buffer = request;
            },
            retryLastRequest: function() {

                function successCallback(response) {
                    buffer.deferred.resolve(response);
                }

                function errorCallback(response) {
                    buffer.deferred.reject(response);
                }
                $http = $http || $injector.get("$http");
                $http(buffer.config).then(successCallback, errorCallback);
            }
        };
    });