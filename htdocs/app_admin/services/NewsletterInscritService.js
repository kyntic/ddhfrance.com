App.service('NewsletterInscrit', function ($http, $q, $log, Module) {

    this.parametres = {

        urls: {
            index           :   '/admin/newsletter_manager/newsletter_inscrits/index/',
            exporter        :   '/admin/newsletter_manager/newsletter_inscrits/exporter/',
        }

    };

    /**
     * Récupérer tous les inscrits
     */
    this.findAll = function (id) {

        var _this = this, deffered = $q.defer();

        $http.get(this.parametres.urls.index)
            .success(function (response) {

                deffered.resolve(response);
            })
            .error(function (x) {

                deffered.reject(x);
            });

        return deffered.promise;

    };


});