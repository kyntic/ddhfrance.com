/*!
 * 
 * Angle - Bootstrap Admin App + AngularJS
 * 
 * Author: @themicon_co
 * Website: http://themicon.co
 * License: http://support.wrapbootstrap.com/knowledge_base/topics/usage-licenses
 * 
 */



if (typeof $ === 'undefined') { throw new Error('This application\'s JavaScript requires jQuery'); }

// APP START
// ----------------------------------- 
var App = angular.module('myAppAdmin', ['ngRoute', 'ngAnimate', 'ngStorage', 'ngCookies', 'ngSanitize', 'pascalprecht.translate', 'ui.bootstrap', 'ui.router', 'oc.lazyLoad', 'cfp.loadingBar', 'AuthServices'])
    .run(["$rootScope", "$state", "$stateParams",  '$window', '$templateCache', '$log', '$location', 'UserService', 'AUTH_EVENTS', 'SessionService', function ($rootScope, $state, $stateParams, $window, $templateCache, $log, $location, UserService, AUTH_EVENTS, SessionService) {
        // Set reference to access them from any scope
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
        $rootScope.$storage = $window.localStorage;
        $rootScope.WH_ROOT = WH_ROOT;

        // Uncomment this to disables template cache
        $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
         if (typeof(toState) !== 'undefined'){
         $templateCache.remove(toState.templateUrl);
         }
         });


        // Scope Globals
        // -----------------------------------
        $rootScope.app = {
            name        : 'Kobaltis',
            description : 'Administration de votre site web',
            year        : ((new Date()).getFullYear()),
            layout: {
                isFixed: true,
                isCollapsed: false,
                isBoxed: false,
                isRTL: false
            },
            viewAnimation: 'ng-fadeInUp'
        };

        $log.log('Début de programme');
    }
    ]);




/**=========================================================
 * Module: config.js
 * App routes and resources configuration
 =========================================================*/

App.config(['$stateProvider','$urlRouterProvider', '$controllerProvider', '$compileProvider', '$filterProvider', '$provide', '$ocLazyLoadProvider', 'APP_REQUIRES',
        function ($stateProvider, $urlRouterProvider, $controllerProvider, $compileProvider, $filterProvider, $provide, $ocLazyLoadProvider, appRequires) {
            'use strict';

            App.controller = $controllerProvider.register;
            App.directive  = $compileProvider.directive;
            App.filter     = $filterProvider.register;
            App.factory    = $provide.factory;
            App.service    = $provide.service;
            App.constant   = $provide.constant;
            App.value      = $provide.value;

            // LAZY MODULES
            // -----------------------------------

            $ocLazyLoadProvider.config({
                debug: false,
                events: true,
                modules: appRequires.modules
            });


            // defaults to dashboard
            $urlRouterProvider.otherwise('/public/login');


            var racine = '/app_admin/';

            //
            // Application Routes
            // -----------------------------------

            $stateProvider
                //
                // Page publics
                // -----------------------------------
                .state('public', {
                    url: '/public',
                    templateUrl: basepath('public/page.html'),
                    title: 'Espace public',
                    resolve: resolveFor('modernizr', 'icons', 'parsley', 'csspiner')
                })
                .state('public.login', {
                    url: '/login',
                    title: "Login",
                    templateUrl: basepath('public/login.html')
                })
                .state('public.register', {
                    url: '/register',
                    title: "Register",
                    templateUrl: basepath('public/register.html')
                })
                .state('public.recover', {
                    url: '/recover',
                    title: "Recover",
                    templateUrl: basepath('public/recover.html')
                })
                .state('public.lock', {
                    url: '/lock',
                    title: "Lock",
                    templateUrl: basepath('public/lock.html')
                })


            ;

            // Set here the base of the relative path
            // for all app views
            function basepath(uri) {
                return WH_ROOT + '/app_admin/views/' + uri;
            }



            // Generates a resolve object by passing script names
            // previously configured in constant.APP_REQUIRES
            function resolveFor() {
                var _args = arguments;
                return {
                    deps: ['$ocLazyLoad','$q', function ($ocLL, $q) {
                        // Creates a promise chain for each argument
                        var promise = $q.when(1); // empty promise
                        for(var i=0, len=_args.length; i < len; i ++){
                            promise = andThen(_args[i]);
                        }
                        return promise;

                        // creates promise to chain dynamically
                        function andThen(_arg) {
                            // also support a function that returns a promise
                            if(typeof _arg == 'function')
                                return promise.then(_arg);
                            else
                                return promise.then(function() {
                                    // if is a module, pass the name. If not, pass the array
                                    var whatToLoad = getRequired(_arg);
                                    // simple error check
                                    if(!whatToLoad) return $.error('Route resolve: Bad resource name [' + _arg + ']');
                                    // finally, return a promise
                                    return $ocLL.load( whatToLoad );
                                });
                        }
                        // check and returns required data
                        // analyze module items with the form [name: '', files: []]
                        // and also simple array of script files (for not angular js)
                        function getRequired(name) {
                            if (appRequires.modules)
                                for(var m in appRequires.modules)
                                    if(appRequires.modules[m].name && appRequires.modules[m].name === name)
                                        return appRequires.modules[m];
                            return appRequires.scripts && appRequires.scripts[name];
                        }

                    }]};
            }

        }]).config(['$translateProvider', function ($translateProvider) {

        $translateProvider.useStaticFilesLoader({
            prefix : WH_ROOT+'/app_admin/assets/i18n/',
            suffix : '.json'
        });
        $translateProvider.preferredLanguage('fr');
        $translateProvider.useLocalStorage();

    }]).config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
        cfpLoadingBarProvider.includeBar = true;
        cfpLoadingBarProvider.includeSpinner = false;
        cfpLoadingBarProvider.latencyThreshold = 500;
        cfpLoadingBarProvider.parentSelector = '.wrapper > section';
    }])
    .controller('NullController', function() {});

/**=========================================================
 * Module: constants.js
 * Define constants to inject across the application
 =========================================================*/
App
    .constant('APP_COLORS', {
        'primary':                '#5d9cec',
        'success':                '#27c24c',
        'info':                   '#23b7e5',
        'warning':                '#ff902b',
        'danger':                 '#f05050',
        'inverse':                '#131e26',
        'green':                  '#37bc9b',
        'pink':                   '#f532e5',
        'purple':                 '#7266ba',
        'dark':                   '#3a3f51',
        'yellow':                 '#fad732',
        'gray-darker':            '#232735',
        'gray-dark':              '#3a3f51',
        'gray':                   '#dde6e9',
        'gray-light':             '#e4eaec',
        'gray-lighter':           '#edf1f2'
    })
    .constant('APP_MEDIAQUERY', {
        'desktopLG':             1200,
        'desktop':                992,
        'tablet':                 768,
        'mobile':                 480
    })
    .constant('redactorOptions', {

    })
    .constant('APP_REQUIRES', {
        scripts: {
            'jquery':             [WH_ROOT+'/vendor/jquery/jquery.min.js'],
            'icons':              [WH_ROOT+'/vendor/skycons/skycons.js', WH_ROOT+'/vendor/fontawesome/css/font-awesome.min.css',WH_ROOT+'/vendor/simplelineicons/simple-line-icons.css', WH_ROOT+'/vendor/weathericons/css/weather-icons.min.css'],
            'modernizr':          [WH_ROOT+'/vendor/modernizr/modernizr.js'],
            'fastclick':          [WH_ROOT+'/vendor/fastclick/fastclick.js'],
            'filestyle':          [WH_ROOT+'/vendor/filestyle/bootstrap-filestyle.min.js'],
            'csspiner':           [WH_ROOT+'/vendor/csspinner/csspinner.min.css'],
            'animo':              [WH_ROOT+'/vendor/animo/animo.min.js'],
            'sparklines':         [WH_ROOT+'/vendor/sparklines/jquery.sparkline.min.js'],
            'slimscroll':         [WH_ROOT+'/vendor/slimscroll/jquery.slimscroll.min.js'],
            'screenfull':         [WH_ROOT+'/vendor/screenfull/screenfull.min.js'],
            'classyloader':       [WH_ROOT+'/vendor/classyloader/js/jquery.classyloader.min.js'],
            'vector-map':         [WH_ROOT+'/vendor/jvectormap/jquery-jvectormap-1.2.2.min.js', WH_ROOT+'/vendor/jvectormap/maps/jquery-jvectormap-world-mill-en.js', WH_ROOT+'/vendor/jvectormap/jquery-jvectormap-1.2.2.css'],
            'loadGoogleMapsJS':   [WH_ROOT+'/vendor/gmap/load-google-maps.js'],
            'google-map':         [WH_ROOT+'/vendor/gmap/jquery.gmap.min.js'],
            'flot-chart':         [WH_ROOT+'/vendor/flot/jquery.flot.min.js'],
            'flot-chart-plugins': [WH_ROOT+'/vendor/flot/jquery.flot.tooltip.min.js',WH_ROOT+'/vendor/flot/jquery.flot.resize.min.js','/vendor/flot/jquery.flot.pie.min.js',WH_ROOT+'/vendor/flot/jquery.flot.time.min.js',WH_ROOT+'/vendor/flot/jquery.flot.categories.min.js','/vendor/flot/jquery.flot.spline.min.js'],
            'jquery-ui':          [WH_ROOT+'/vendor/jqueryui/jquery-ui.min.js', WH_ROOT+'/vendor/touch-punch/jquery.ui.touch-punch.min.js'],
            'chosen':             [WH_ROOT+'/vendor/chosen/chosen.jquery.min.js', WH_ROOT+'/vendor/chosen/chosen.min.css'],
            'slider':             [WH_ROOT+'/vendor/slider/js/bootstrap-slider.js', WH_ROOT+'/vendor/slider/css/slider.css'],
            'moment' :            [WH_ROOT+'/vendor/moment/min/moment-with-locales.min.js'],
            'fullcalendar':       [WH_ROOT+'/vendor/fullcalendar/dist/fullcalendar.min.js', WH_ROOT+'/vendor/fullcalendar/dist/fullcalendar.css'],
            'codemirror':         [WH_ROOT+'/vendor/codemirror/lib/codemirror.js', WH_ROOT+'/vendor/codemirror/lib/codemirror.css'],
            'codemirror-plugins': [WH_ROOT+'/vendor/codemirror/addon/mode/overlay.js',WH_ROOT+'/vendor/codemirror/mode/markdown/markdown.js','/vendor/codemirror/mode/xml/xml.js','/vendor/codemirror/mode/gfm/gfm.js','/vendor/marked/marked.js'],
            'taginput' :          [WH_ROOT+'/vendor/tagsinput/bootstrap-tagsinput.min.js', '/vendor/tagsinput/bootstrap-tagsinput.css'],
            'inputmask':          [WH_ROOT+'/vendor/inputmask/jquery.inputmask.bundle.min.js'],
            'bwizard':            [WH_ROOT+'/vendor/wizard/js/bwizard.min.js'],
            'parsley':            [WH_ROOT+'/vendor/parsley/parsley.min.js'],
            'datatables':         [WH_ROOT+'/vendor/datatable/media/js/jquery.dataTables.min.js', WH_ROOT+'/vendor/datatable/extensions/datatable-bootstrap/css/dataTables.bootstrap.css'],
            'datatables-pugins':  [WH_ROOT+'/vendor/datatable/extensions/datatable-bootstrap/js/dataTables.bootstrap.js',WH_ROOT+'/vendor/datatable/extensions/datatable-bootstrap/js/dataTables.bootstrapPagination.js',WH_ROOT+'/vendor/datatable/extensions/ColVis/js/dataTables.colVis.min.js', WH_ROOT+'/vendor/datatable/extensions/ColVis/css/dataTables.colVis.css'],
            'flatdoc':            [WH_ROOT+'/vendor/flatdoc/flatdoc.js'],
            'redactor':           [WH_ROOT+'/vendor/redactor/redactor/redactor.min.js', WH_ROOT+'/vendor/redactor/redactor/redactor.css']
        },
        modules: [
            { name: 'toaster',         files: [WH_ROOT+'/vendor/toaster/toaster.js', WH_ROOT+'/vendor/toaster/toaster.css'] },
            { name: 'ngWig',          files: [WH_ROOT+'/vendor/ngwig/ng-wig.min.js'] }
        ]
    })
;
/**=========================================================
 * Module: access-login.js
 * Demo for login api
 =========================================================*/

App.controller('LoginFormController', ['$scope', '$http', '$state', 'UserService', '$location', function($scope, $http, $state, UserService, $location) {

    // bind here all data from the form
    $scope.data = {};
    // place the message if something goes wrong
    $scope.authMsg = '';

    $scope.login = function() {

        $scope.loading = true;

        $scope.authMsg = '';

        UserService.login($scope.data).then(function(res) {

            $scope.loading = false;

            if(res) {

                window.location = WH_ROOT + '/admin';

            }else{

                $scope.authMsg = 'Email ou mot de passe inconnu.';

            }


        }, function() {

            $scope.authMsg = 'Email ou mot de passe inconnu.';

        });


    };

}]);

/**=========================================================
 * Module: access-login.js
 * Demo for login api
 =========================================================*/

App.controller('RegisterFormController', ['$scope', '$http', '$state', 'UserService', '$location', function($scope, $http, $state, $location) {

    $scope.editForm = {};
    $scope.data = {};
    $scope.errors = {};

    console.log($scope.errors);


    $scope.inscription = function () {

        $scope.errors = {};
        $scope.loading = true;

        $http
            .post('/users/inscription', $scope.data)

            .then(function (response) {

                $scope.loading = false;

                if(response.data.ok) {

                    $state.go('public.login');

                }else{

                    $scope.errors = response.data.errors;

                }


            }, function (x) {

                $scope.log = x;

            }

        );



    };


}]);


/**=========================================================
 * Module: utils.js
 * jQuery Utility functions library
 * adapted from the core of UIKit
 =========================================================*/

(function($, window, doc){
    'use strict';

    var $html = $("html"), $win = $(window);

    $.support.transition = (function() {

        var transitionEnd = (function() {

            var element = doc.body || doc.documentElement,
                transEndEventNames = {
                    WebkitTransition: 'webkitTransitionEnd',
                    MozTransition: 'transitionend',
                    OTransition: 'oTransitionEnd otransitionend',
                    transition: 'transitionend'
                }, name;

            for (name in transEndEventNames) {
                if (element.style[name] !== undefined) return transEndEventNames[name];
            }
        }());

        return transitionEnd && { end: transitionEnd };
    })();

    $.support.animation = (function() {

        var animationEnd = (function() {

            var element = doc.body || doc.documentElement,
                animEndEventNames = {
                    WebkitAnimation: 'webkitAnimationEnd',
                    MozAnimation: 'animationend',
                    OAnimation: 'oAnimationEnd oanimationend',
                    animation: 'animationend'
                }, name;

            for (name in animEndEventNames) {
                if (element.style[name] !== undefined) return animEndEventNames[name];
            }
        }());

        return animationEnd && { end: animationEnd };
    })();

    $.support.requestAnimationFrame = window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.msRequestAnimationFrame || window.oRequestAnimationFrame || function(callback){ window.setTimeout(callback, 1000/60); };
    $.support.touch                 = (
        ('ontouchstart' in window && navigator.userAgent.toLowerCase().match(/mobile|tablet/)) ||
            (window.DocumentTouch && document instanceof window.DocumentTouch)  ||
            (window.navigator['msPointerEnabled'] && window.navigator['msMaxTouchPoints'] > 0) || //IE 10
            (window.navigator['pointerEnabled'] && window.navigator['maxTouchPoints'] > 0) || //IE >=11
            false
        );
    $.support.mutationobserver      = (window.MutationObserver || window.WebKitMutationObserver || window.MozMutationObserver || null);

    $.Utils = {};

    $.Utils.debounce = function(func, wait, immediate) {
        var timeout;
        return function() {
            var context = this, args = arguments;
            var later = function() {
                timeout = null;
                if (!immediate) func.apply(context, args);
            };
            var callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow) func.apply(context, args);
        };
    };

    $.Utils.removeCssRules = function(selectorRegEx) {
        var idx, idxs, stylesheet, _i, _j, _k, _len, _len1, _len2, _ref;

        if(!selectorRegEx) return;

        setTimeout(function(){
            try {
                _ref = document.styleSheets;
                for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                    stylesheet = _ref[_i];
                    idxs = [];
                    stylesheet.cssRules = stylesheet.cssRules;
                    for (idx = _j = 0, _len1 = stylesheet.cssRules.length; _j < _len1; idx = ++_j) {
                        if (stylesheet.cssRules[idx].type === CSSRule.STYLE_RULE && selectorRegEx.test(stylesheet.cssRules[idx].selectorText)) {
                            idxs.unshift(idx);
                        }
                    }
                    for (_k = 0, _len2 = idxs.length; _k < _len2; _k++) {
                        stylesheet.deleteRule(idxs[_k]);
                    }
                }
            } catch (_error) {}
        }, 0);
    };

    $.Utils.isInView = function(element, options) {

        var $element = $(element);

        if (!$element.is(':visible')) {
            return false;
        }

        var window_left = $win.scrollLeft(),
            window_top  = $win.scrollTop(),
            offset      = $element.offset(),
            left        = offset.left,
            top         = offset.top;

        options = $.extend({topoffset:0, leftoffset:0}, options);

        if (top + $element.height() >= window_top && top - options.topoffset <= window_top + $win.height() &&
            left + $element.width() >= window_left && left - options.leftoffset <= window_left + $win.width()) {
            return true;
        } else {
            return false;
        }
    };

    $.Utils.options = function(string) {

        if ($.isPlainObject(string)) return string;

        var start = (string ? string.indexOf("{") : -1), options = {};

        if (start != -1) {
            try {
                options = (new Function("", "var json = " + string.substr(start) + "; return JSON.parse(JSON.stringify(json));"))();
            } catch (e) {}
        }

        return options;
    };

    $.Utils.events       = {};
    $.Utils.events.click = $.support.touch ? 'tap' : 'click';

    $.langdirection = $html.attr("dir") == "rtl" ? "right" : "left";

    $(function(){

        // Check for dom modifications
        if(!$.support.mutationobserver) return;

        // Install an observer for custom needs of dom changes
        var observer = new $.support.mutationobserver($.Utils.debounce(function(mutations) {
            $(doc).trigger("domready");
        }, 300));

        // pass in the target node, as well as the observer options
        observer.observe(document.body, { childList: true, subtree: true });

    });

    // add touch identifier class
    $html.addClass($.support.touch ? "touch" : "no-touch");

}(jQuery, window, document));


/**=========================================================
 * Module: anchor.js
 * Disables null anchor behavior
 =========================================================*/

App.directive('href', function() {

    return {
        restrict: 'A',
        compile: function(element, attr) {
            return function(scope, element) {
                if(attr.ngClick || attr.href === '' || attr.href === '#'){
                    if( !element.hasClass('dropdown-toggle') )
                        element.on('click', function(e){
                            e.preventDefault();
                            e.stopPropagation();
                        });
                }
            };
        }
    };
});
/**=========================================================
 * Module: animate-enabled.js
 * Enable or disables ngAnimate for element with directive
 =========================================================*/

App.directive("animateEnabled", ["$animate", function ($animate) {
    return {
        link: function (scope, element, attrs) {
            scope.$watch(function () {
                return scope.$eval(attrs.animateEnabled, scope);
            }, function (newValue) {
                $animate.enabled(!!newValue, element);
            });
        }
    };
}]);


/**=========================================================
 * Module: chosen-select.js
 * Initializes the chose select plugin
 =========================================================*/

App.directive('chosen', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attr) {

            // update the select when data is loaded
            scope.$watch(attr.chosen, function(oldVal, newVal) {
                element.trigger('chosen:updated');
            });

            // update the select when the model changes
            scope.$watch(attr.ngModel, function() {
                element.trigger('chosen:updated');
            });

            if($.fn.chosen)
                element.chosen();
        }
    };
});
/**=========================================================
 * Module: classy-loader.js
 * Enable use of classyloader directly from data attributes
 =========================================================*/

App.directive('classyloader', function($timeout) {
    'use strict';

    var $scroller       = $(window),
        inViewFlagClass = 'js-is-in-view'; // a classname to detect when a chart has been triggered after scroll

    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            // run after interpolation
            $timeout(function(){

                var $element = $(element),
                    options  = $element.data();

                // At lease we need a data-percentage attribute
                if(options) {
                    if( options.triggerInView ) {

                        $scroller.scroll(function() {
                            checkLoaderInVIew($element, options);
                        });
                        // if the element starts already in view
                        checkLoaderInVIew($element, options);
                    }
                    else
                        startLoader($element, options);
                }

            }, 0);

            function checkLoaderInVIew(element, options) {
                var offset = -20;
                if( ! element.hasClass(inViewFlagClass) &&
                    $.Utils.isInView(element, {topoffset: offset}) ) {
                    startLoader(element, options);
                }
            }
            function startLoader(element, options) {
                element.ClassyLoader(options).addClass(inViewFlagClass);
            }
        }
    };
});

/**=========================================================
 * Module: clear-storage.js
 * Removes a key from the browser storage via element click
 =========================================================*/

App.directive('resetKey',  ['$state','$rootScope', function($state, $rootScope) {
    'use strict';

    return {
        restrict: 'A',
        scope: {
            resetKey: '='
        },
        link: function(scope, element, attrs) {

            scope.resetKey = attrs.resetKey;

        },
        controller: function($scope, $element) {

            $element.on('click', function (e) {
                e.preventDefault();

                if($scope.resetKey) {
                    delete $rootScope.$storage[$scope.resetKey];
                    $state.go($state.current, {}, {reload: true});
                }
                else {
                    $.error('No storage key specified for reset.');
                }
            });

        }

    };

}]);
/**=========================================================
 * Module: filestyle.js
 * Initializes the fielstyle plugin
 =========================================================*/

App.directive('filestyle', function() {
    return {
        restrict: 'A',
        controller: function($scope, $element) {
            var $elem = $($element);
            $elem.filestyle({
                classInput: $elem.data('classinput')
            });
        }
    };
});

/**=========================================================
 * Module: flatdoc.js
 * Creates the flatdoc markup and initializes the plugin
 =========================================================*/

App.directive('flatdoc', ['$location', function($location) {
    return {
        restrict: "EA",
        template: "<div role='flatdoc'><div role='flatdoc-menu'></div><div role='flatdoc-content'></div></div>",
        link: function(scope, element, attrs) {

            Flatdoc.run({
                fetcher: Flatdoc.file(attrs.src)
            });

            var $root = $('html, body');
            $(document).on('flatdoc:ready', function() {
                var docMenu = $('[role="flatdoc-menu"]');
                docMenu.find('a').on('click', function(e) {
                    e.preventDefault(); e.stopPropagation();

                    var $this = $(this);

                    docMenu.find('a.active').removeClass('active');
                    $this.addClass('active');

                    $root.animate({
                        scrollTop: $(this.getAttribute('href')).offset().top - ($('.topnavbar').height() + 10)
                    }, 800);
                });

            });
        }
    };

}]);

/**=========================================================
 * Module: fullscreen.js
 * Toggle the fullscreen mode on/off
 =========================================================*/

App.directive('toggleFullscreen', function() {
    'use strict';

    return {
        restrict: 'A',
        link: function(scope, element, attrs) {

            element.on('click', function (e) {
                e.preventDefault();

                if (screenfull.enabled) {

                    screenfull.toggle();

                    // Switch icon indicator
                    if(screenfull.isFullscreen)
                        $(this).children('em').removeClass('fa-expand').addClass('fa-compress');
                    else
                        $(this).children('em').removeClass('fa-compress').addClass('fa-expand');

                } else {
                    $.error('Fullscreen not enabled');
                }

            });
        }
    };

});


/**=========================================================
 * Module: load-css.js
 * Request and load into the current page a css file
 =========================================================*/

App.directive('loadCss', function() {
    'use strict';

    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            element.on('click', function (e) {
                if(element.is('a')) e.preventDefault();
                var uri = attrs.loadCss,
                    link;

                if(uri) {
                    link = createLink(uri);
                    if ( !link ) {
                        $.error('Error creating stylesheet link element.');
                    }
                }
                else {
                    $.error('No stylesheet location defined.');
                }

            });

        }
    };

    function createLink(uri) {
        var linkId = 'autoloaded-stylesheet',
            oldLink = $('#'+linkId).attr('id', linkId + '-old');

        $('head').append($('<link/>').attr({
            'id':   linkId,
            'rel':  'stylesheet',
            'href': uri
        }));

        if( oldLink.length ) {
            oldLink.remove();
        }

        return $('#'+linkId);
    }


});


/**=========================================================
 * Module: masked,js
 * Initializes the masked inputs
 =========================================================*/

App.directive('masked', function() {
    return {
        restrict: 'A',
        controller: function($scope, $element) {
            var $elem = $($element);
            if($.fn.inputmask)
                $elem.inputmask();
        }
    };
});

/**=========================================================
 * Module: navbar-search.js
 * Navbar search toggler * Auto dismiss on ESC key
 =========================================================*/

App.directive('searchOpen', ['navSearch', function(navSearch) {
        'use strict';

        return {
            restrict: 'A',
            controller: function($scope, $element) {
                $element
                    .on('click', function (e) { e.stopPropagation(); })
                    .on('click', navSearch.toggle);
            }
        };

    }]).directive('searchDismiss', ['navSearch', function(navSearch) {
        'use strict';

        var inputSelector = '.navbar-form input[type="text"]';

        return {
            restrict: 'A',
            controller: function($scope, $element) {

                $(inputSelector)
                    .on('click', function (e) { e.stopPropagation(); })
                    .on('keyup', function(e) {
                        if (e.keyCode == 27) // ESC
                            navSearch.dismiss();
                    });

                // click anywhere closes the search
                $(document).on('click', navSearch.dismiss);
                // dismissable options
                $element
                    .on('click', function (e) { e.stopPropagation(); })
                    .on('click', navSearch.dismiss);
            }
        };

    }]);


/**=========================================================
 * Module: notify.js
 * Create a notifications that fade out automatically.
 * Based on Notify addon from UIKit (http://getuikit.com/docs/addons_notify.html)
 =========================================================*/

App.directive('notify', function($window){

    return {
        restrict: 'A',
        controller: function ($scope, $element) {

            $element.on('click', function (e) {
                e.preventDefault();
                notifyNow($element);
            });

        }
    };

    function notifyNow(elem) {
        var $element = $(elem),
            message = $element.data('message'),
            options = $element.data('options');

        if(!message)
            $.error('Notify: No message specified');

        $.notify(message, options || {});
    }


});


/**
 * Notify Addon definition as jQuery plugin
 * Adapted version to work with Bootstrap classes
 * More information http://getuikit.com/docs/addons_notify.html
 */

(function($, window, document){

    var containers = {},
        messages   = {},

        notify     =  function(options){

            if ($.type(options) == 'string') {
                options = { message: options };
            }

            if (arguments[1]) {
                options = $.extend(options, $.type(arguments[1]) == 'string' ? {status:arguments[1]} : arguments[1]);
            }

            return (new Message(options)).show();
        },
        closeAll  = function(group, instantly){
            if(group) {
                for(var id in messages) { if(group===messages[id].group) messages[id].close(instantly); }
            } else {
                for(var id in messages) { messages[id].close(instantly); }
            }
        };

    var Message = function(options){

        var $this = this;

        this.options = $.extend({}, Message.defaults, options);

        this.uuid    = "ID"+(new Date().getTime())+"RAND"+(Math.ceil(Math.random() * 100000));
        this.element = $([
            // @geedmo: alert-dismissable enables bs close icon
            '<div class="uk-notify-message alert-dismissable">',
            '<a class="close">&times;</a>',
            '<div>'+this.options.message+'</div>',
            '</div>'

        ].join('')).data("notifyMessage", this);

        // status
        if (this.options.status) {
            this.element.addClass('alert alert-'+this.options.status);
            this.currentstatus = this.options.status;
        }

        this.group = this.options.group;

        messages[this.uuid] = this;

        if(!containers[this.options.pos]) {
            containers[this.options.pos] = $('<div class="uk-notify uk-notify-'+this.options.pos+'"></div>').appendTo('body').on("click", ".uk-notify-message", function(){
                $(this).data("notifyMessage").close();
            });
        }
    };


    $.extend(Message.prototype, {

        uuid: false,
        element: false,
        timout: false,
        currentstatus: "",
        group: false,

        show: function() {

            if (this.element.is(":visible")) return;

            var $this = this;

            containers[this.options.pos].show().prepend(this.element);

            var marginbottom = parseInt(this.element.css("margin-bottom"), 10);

            this.element.css({"opacity":0, "margin-top": -1*this.element.outerHeight(), "margin-bottom":0}).animate({"opacity":1, "margin-top": 0, "margin-bottom":marginbottom}, function(){

                if ($this.options.timeout) {

                    var closefn = function(){ $this.close(); };

                    $this.timeout = setTimeout(closefn, $this.options.timeout);

                    $this.element.hover(
                        function() { clearTimeout($this.timeout); },
                        function() { $this.timeout = setTimeout(closefn, $this.options.timeout);  }
                    );
                }

            });

            return this;
        },

        close: function(instantly) {

            var $this    = this,
                finalize = function(){
                    $this.element.remove();

                    if(!containers[$this.options.pos].children().length) {
                        containers[$this.options.pos].hide();
                    }

                    delete messages[$this.uuid];
                };

            if(this.timeout) clearTimeout(this.timeout);

            if(instantly) {
                finalize();
            } else {
                this.element.animate({"opacity":0, "margin-top": -1* this.element.outerHeight(), "margin-bottom":0}, function(){
                    finalize();
                });
            }
        },

        content: function(html){

            var container = this.element.find(">div");

            if(!html) {
                return container.html();
            }

            container.html(html);

            return this;
        },

        status: function(status) {

            if(!status) {
                return this.currentstatus;
            }

            this.element.removeClass('alert alert-'+this.currentstatus).addClass('alert alert-'+status);

            this.currentstatus = status;

            return this;
        }
    });

    Message.defaults = {
        message: "",
        status: "normal",
        timeout: 5000,
        group: null,
        pos: 'top-center'
    };


    $["notify"]          = notify;
    $["notify"].message  = Message;
    $["notify"].closeAll = closeAll;

    return notify;

}(jQuery, window, document));

/**=========================================================
 * Module: now.js
 * Provides a simple way to display the current time formatted
 =========================================================*/

App.directive("now", ['dateFilter', '$interval', function(dateFilter, $interval){
    return {
        restrict: 'E',
        link: function(scope, element, attrs){

            var format = attrs.format;

            function updateTime() {
                var dt = dateFilter(new Date(), format);
                element.text(dt);
            }

            updateTime();
            $interval(updateTime, 1000);
        }
    };
}]);
/**=========================================================
 * Module panel-tools.js
 * Directive tools to control panels.
 * Allows collapse, refresh and dismiss (remove)
 * Saves panel state in browser storage
 =========================================================*/

App.directive('paneltool', function(){
    var templates = {
        /* jshint multistr: true */
        collapse:"<a href='#' panel-collapse='' data-toggle='tooltip' title='Collapse Panel' ng-click='{{panelId}} = !{{panelId}}' ng-init='{{panelId}}=false'> \
                <em ng-show='{{panelId}}' class='fa fa-plus'></em> \
                <em ng-show='!{{panelId}}' class='fa fa-minus'></em> \
              </a>",
        dismiss: "<a href='#' panel-dismiss='' data-toggle='tooltip' title='Close Panel'>\
               <em class='fa fa-times'></em>\
             </a>",
        refresh: "<a href='#' panel-refresh='' data-toggle='tooltip' data-spinner='{{spinner}}' title='Refresh Panel'>\
               <em class='fa fa-refresh'></em>\
             </a>"
    };

    return {
        restrict: 'E',
        template: function( elem, attrs ){
            var temp = '';
            if(attrs.toolCollapse)
                temp += templates.collapse.replace(/{{panelId}}/g, (elem.parent().parent().attr('id')) );
            if(attrs.toolDismiss)
                temp += templates.dismiss;
            if(attrs.toolRefresh)
                temp += templates.refresh.replace(/{{spinner}}/g, attrs.toolRefresh);
            return temp;
        },
        // scope: true,
        // transclude: true,
        link: function (scope, element, attrs) {
            element.addClass('pull-right');
        }
    };
})
/**=========================================================
 * Dismiss panels * [panel-dismiss]
 =========================================================*/
    .directive('panelDismiss', function(){
        'use strict';
        return {
            restrict: 'A',
            controller: function ($scope, $element) {
                var removeEvent   = 'panel-remove',
                    removedEvent  = 'panel-removed';

                $element.on('click', function () {

                    // find the first parent panel
                    var parent = $(this).closest('.panel');

                    if($.support.animation) {
                        parent.animo({animation: 'bounceOut'}, removeElement);
                    }
                    else removeElement();

                    function removeElement() {
                        // Trigger the event and finally remove the element
                        $.when(parent.trigger(removeEvent, [parent]))
                            .done(destroyPanel);
                    }

                    function destroyPanel() {
                        var col = parent.parent();
                        parent.remove();
                        // remove the parent if it is a row and is empty and not a sortable (portlet)
                        col
                            .trigger(removedEvent) // An event to catch when the panel has been removed from DOM
                            .filter(function() {
                                var el = $(this);
                                return (el.is('[class*="col-"]:not(.sortable)') && el.children('*').length === 0);
                            }).remove();

                    }
                });
            }
        };
    })
/**=========================================================
 * Collapse panels * [panel-collapse]
 =========================================================*/
    .directive('panelCollapse', ['$timeout', function($timeout){
        'use strict';

        var storageKeyName = 'panelState',
            storage;

        return {
            restrict: 'A',
            // transclude: true,
            controller: function ($scope, $element) {

                // Prepare the panel to be collapsible
                var $elem   = $($element),
                    parent  = $elem.closest('.panel'), // find the first parent panel
                    panelId = parent.attr('id');

                storage = $scope.$storage;

                // Load the saved state if exists
                var currentState = loadPanelState( panelId );
                if ( typeof currentState !== undefined) {
                    $timeout(function(){
                            $scope[panelId] = currentState; },
                        10);
                }

                // bind events to switch icons
                $element.bind('click', function() {

                    savePanelState( panelId, !$scope[panelId] );

                });
            }
        };

        function savePanelState(id, state) {
            if(!id) return false;
            var data = angular.fromJson(storage[storageKeyName]);
            if(!data) { data = {}; }
            data[id] = state;
            storage[storageKeyName] = angular.toJson(data);
        }

        function loadPanelState(id) {
            if(!id) return false;
            var data = angular.fromJson(storage[storageKeyName]);
            if(data) {
                return data[id];
            }
        }

    }])
/**=========================================================
 * Refresh panels
 * [panel-refresh] * [data-spinner="standard"]
 =========================================================*/
    .directive('panelRefresh', function(){
        'use strict';

        return {
            restrict: 'A',
            controller: function ($scope, $element) {

                var refreshEvent   = 'panel-refresh',
                    csspinnerClass = 'csspinner',
                    defaultSpinner = 'standard';

                // method to clear the spinner when done
                function removeSpinner() {
                    this.removeClass(csspinnerClass);
                }

                // catch clicks to toggle panel refresh
                $element.on('click', function () {
                    var $this   = $(this),
                        panel   = $this.parents('.panel').eq(0),
                        spinner = $this.data('spinner') || defaultSpinner
                        ;

                    // start showing the spinner
                    panel.addClass(csspinnerClass + ' ' + spinner);

                    // attach as public method
                    panel.removeSpinner = removeSpinner;

                    // Trigger the event and send the panel object
                    $this.trigger(refreshEvent, [panel]);

                });

            }
        };
    });



/**=========================================================
 * Module: play-animation.js
 * Provides a simple way to run animation with a trigger
 * Requires animo.js
 =========================================================*/

App.directive('animate', function($window){

    'use strict';

    var $scroller = $(window).add('body, .wrapper');

    return {
        restrict: 'A',
        link: function (scope, elem, attrs) {

            // Parse animations params and attach trigger to scroll
            var $elem     = $(elem),
                offset    = $elem.data('offset'),
                delay     = $elem.data('delay')     || 100, // milliseconds
                animation = $elem.data('play')      || 'bounce';

            if(typeof offset !== 'undefined') {

                // test if the element starts visible
                testAnimation($elem);
                // test on scroll
                $scroller.scroll(function(){
                    testAnimation($elem);
                });

            }

            // Test an element visibilty and trigger the given animation
            function testAnimation(element) {
                if ( !element.hasClass('anim-running') &&
                    $.Utils.isInView(element, {topoffset: offset})) {
                    element
                        .addClass('anim-running');

                    setTimeout(function() {
                        element
                            .addClass('anim-done')
                            .animo( { animation: animation, duration: 0.7} );
                    }, delay);

                }
            }

            // Run click triggered animations
            $elem.on('click', function() {

                var $elem     = $(this),
                    targetSel = $elem.data('target'),
                    animation = $elem.data('play') || 'bounce',
                    target    = $(targetSel);

                if(target && target) {
                    target.animo( { animation: animation } );
                }

            });
        }
    };

});

/**=========================================================
 * Module: scroll.js
 * Make a content box scrollable
 =========================================================*/

App.directive('scrollable', function(){
    return {
        restrict: 'EA',
        link: function(scope, elem, attrs) {
            var defaultHeight = 250;
            elem.slimScroll({
                height: (attrs.height || defaultHeight)
            });
        }
    };
});
/**=========================================================
 * Module: sidebar.js
 * Wraps the sidebar and handles collapsed state
 =========================================================*/

App.directive('sidebar', ['$window', 'APP_MEDIAQUERY', function($window, mq) {

    var $win  = $($window);
    var $html = $('html');
    var $body = $('body');
    var $scope;
    var $sidebar;

    return {
        restrict: 'EA',
        template: '<nav class="sidebar" ng-transclude></nav>',
        transclude: true,
        replace: true,
        link: function(scope, element, attrs) {

            $scope   = scope;
            $sidebar = element;

            var eventName = isTouch() ? 'click' : 'mouseenter' ;
            $sidebar.on( eventName, '.nav > li', function() {
                if( isSidebarCollapsed() && !isMobile() )
                    toggleMenuItem( $(this) );
            });

            scope.$on('closeSidebarMenu', function() {
                removeFloatingNav();
                $('.sidebar li.open').removeClass('open');
            });
        }
    };


    // Open the collapse sidebar submenu items when on touch devices
    // - desktop only opens on hover
    function toggleTouchItem($element){
        $element
            .siblings('li')
            .removeClass('open')
            .end()
            .toggleClass('open');
    }

    // Handles hover to open items under collapsed menu
    // -----------------------------------
    function toggleMenuItem($listItem) {

        removeFloatingNav();

        var ul = $listItem.children('ul');

        if( !ul.length ) return;
        if( $listItem.hasClass('open') ) {
            toggleTouchItem($listItem);
            return;
        }

        var $aside = $('.aside');
        var mar =  $scope.app.layout.isFixed ?  parseInt( $aside.css('margin-top'), 0) : 0;

        var subNav = ul.clone().appendTo( $aside );

        toggleTouchItem($listItem);

        var itemTop = ($listItem.position().top + mar) - $sidebar.scrollTop();
        var vwHeight = $win.height();

        subNav
            .addClass('nav-floating')
            .css({
                position: $scope.app.layout.isFixed ? 'fixed' : 'absolute',
                top:      itemTop,
                bottom:   (subNav.outerHeight(true) + itemTop > vwHeight) ? 0 : 'auto'
            });

        subNav.on('mouseleave', function() {
            toggleTouchItem($listItem);
            subNav.remove();
        });

    }

    function removeFloatingNav() {
        $('.sidebar-subnav.nav-floating').remove();
    }

    function isTouch() {
        return $html.hasClass('touch');
    }
    function isSidebarCollapsed() {
        return $body.hasClass('aside-collapsed');
    }
    function isSidebarToggled() {
        return $body.hasClass('aside-toggled');
    }
    function isMobile() {
        return $win.width() < mq.tablet;
    }

}]);


/**=========================================================
 * Module: table-checkall.js
 * Tables check all checkbox
 =========================================================*/

App.directive('checkAll', function() {
    'use strict';

    return {
        restrict: 'A',
        controller: function($scope, $element){

            $element.on('change', function() {
                var $this = $(this),
                    index= $this.index() + 1,
                    checkbox = $this.find('input[type="checkbox"]'),
                    table = $this.parents('table');
                // Make sure to affect only the correct checkbox column
                table.find('tbody > tr > td:nth-child('+index+') input[type="checkbox"]')
                    .prop('checked', checkbox[0].checked);

            });
        }
    };

});
/**=========================================================
 * Module: tags-input.js
 * Initializes the tag inputs plugin
 =========================================================*/

App.directive('tagsinput', function() {
    return {
        restrict: 'A',
        controller: function($scope, $element) {
            var $elem = $($element);
            if($.fn.tagsinput)
                $elem.tagsinput();
        }
    };
});

/**=========================================================
 * Module: toggle-state.js
 * Toggle a classname from the BODY Useful to change a state that
 * affects globally the entire layout or more than one item
 * Targeted elements must have [toggle-state="CLASS-NAME-TO-TOGGLE"]
 * User no-persist to avoid saving the sate in browser storage
 =========================================================*/

App.directive('toggleState', ['toggleStateService', function(toggle) {
    'use strict';

    return {
        restrict: 'A',
        link: function(scope, element, attrs) {

            var $body = $('body');

            $(element)
                .on('click', function (e) {
                    e.preventDefault();
                    var classname = attrs.toggleState;

                    if(classname) {
                        if( $body.hasClass(classname) ) {
                            $body.removeClass(classname);
                            if( ! attrs.noPersist)
                                toggle.removeState(classname);
                        }
                        else {
                            $body.addClass(classname);
                            if( ! attrs.noPersist)
                                toggle.addState(classname);
                        }

                    }

                });
        }
    };

}]);

/**=========================================================
 * Module: masked,js
 * Initializes the jQuery UI slider controls
 =========================================================*/

App.directive('uiSlider', function() {
    return {
        restrict: 'A',
        controller: function($scope, $element) {
            var $elem = $($element);
            if($.fn.slider)
                $elem.slider();
        }
    };
});

/**=========================================================
 * Module: validate-form.js
 * Initializes the validation plugin Parsley
 =========================================================*/

App.directive('validateForm', function() {
    return {
        restrict: 'A',
        controller: function($scope, $element) {
            var $elem = $($element);
            if($.fn.parsley)
                $elem.parsley();
        }
    };
});


App.service('browser', function(){
    "use strict";

    var matched, browser;

    var uaMatch = function( ua ) {
        ua = ua.toLowerCase();

        var match = /(opr)[\/]([\w.]+)/.exec( ua ) ||
            /(chrome)[ \/]([\w.]+)/.exec( ua ) ||
            /(version)[ \/]([\w.]+).*(safari)[ \/]([\w.]+)/.exec( ua ) ||
            /(webkit)[ \/]([\w.]+)/.exec( ua ) ||
            /(opera)(?:.*version|)[ \/]([\w.]+)/.exec( ua ) ||
            /(msie) ([\w.]+)/.exec( ua ) ||
            ua.indexOf("trident") >= 0 && /(rv)(?::| )([\w.]+)/.exec( ua ) ||
            ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec( ua ) ||
            [];

        var platform_match = /(ipad)/.exec( ua ) ||
            /(iphone)/.exec( ua ) ||
            /(android)/.exec( ua ) ||
            /(windows phone)/.exec( ua ) ||
            /(win)/.exec( ua ) ||
            /(mac)/.exec( ua ) ||
            /(linux)/.exec( ua ) ||
            /(cros)/i.exec( ua ) ||
            [];

        return {
            browser: match[ 3 ] || match[ 1 ] || "",
            version: match[ 2 ] || "0",
            platform: platform_match[ 0 ] || ""
        };
    };

    matched = uaMatch( window.navigator.userAgent );
    browser = {};

    if ( matched.browser ) {
        browser[ matched.browser ] = true;
        browser.version = matched.version;
        browser.versionNumber = parseInt(matched.version);
    }

    if ( matched.platform ) {
        browser[ matched.platform ] = true;
    }

    // These are all considered mobile platforms, meaning they run a mobile browser
    if ( browser.android || browser.ipad || browser.iphone || browser[ "windows phone" ] ) {
        browser.mobile = true;
    }

    // These are all considered desktop platforms, meaning they run a desktop browser
    if ( browser.cros || browser.mac || browser.linux || browser.win ) {
        browser.desktop = true;
    }

    // Chrome, Opera 15+ and Safari are webkit based browsers
    if ( browser.chrome || browser.opr || browser.safari ) {
        browser.webkit = true;
    }

    // IE11 has a new token so we will assign it msie to avoid breaking changes
    if ( browser.rv )
    {
        var ie = "msie";

        matched.browser = ie;
        browser[ie] = true;
    }

    // Opera 15+ are identified as opr
    if ( browser.opr )
    {
        var opera = "opera";

        matched.browser = opera;
        browser[opera] = true;
    }

    // Stock Android browsers are marked as Safari on Android.
    if ( browser.safari && browser.android )
    {
        var android = "android";

        matched.browser = android;
        browser[android] = true;
    }

    // Assign the name and platform variable
    browser.name = matched.browser;
    browser.platform = matched.platform;


    return browser;

});
/**=========================================================
 * Module: colors.js
 * Services to retrieve global colors
 =========================================================*/

App.factory('colors', ['APP_COLORS', function(colors) {

    return {
        byName: function(name) {
            return (colors[name] || '#fff');
        }
    };

}]);


/**=========================================================
 * Module: nav-search.js
 * Services to share navbar search functions
 =========================================================*/

App.service('navSearch', function() {
    var navbarFormSelector = 'form.navbar-form';
    return {
        toggle: function() {

            var navbarForm = $(navbarFormSelector);

            navbarForm.toggleClass('open');

            var isOpen = navbarForm.hasClass('open');

            navbarForm.find('input')[isOpen ? 'focus' : 'blur']();

        },

        dismiss: function() {
            $(navbarFormSelector)
                .removeClass('open')      // Close control
                .find('input[type="text"]').blur() // remove focus
                .val('')                    // Empty input
            ;
        }
    };

});
/**=========================================================
 * Module: toggle-state.js
 * Services to share toggle state functionality
 =========================================================*/

App.service('toggleStateService', ['$rootScope', function($rootScope) {

    var storageKeyName  = 'toggleState';

    // Helper object to check for words in a phrase //
    var WordChecker = {
        hasWord: function (phrase, word) {
            return new RegExp('(^|\\s)' + word + '(\\s|$)').test(phrase);
        },
        addWord: function (phrase, word) {
            if (!this.hasWord(phrase, word)) {
                return (phrase + (phrase ? ' ' : '') + word);
            }
        },
        removeWord: function (phrase, word) {
            if (this.hasWord(phrase, word)) {
                return phrase.replace(new RegExp('(^|\\s)*' + word + '(\\s|$)*', 'g'), '');
            }
        }
    };

    // Return service public methods
    return {
        // Add a state to the browser storage to be restored later
        addState: function(classname){
            var data = angular.fromJson($rootScope.$storage[storageKeyName]);

            if(!data)  {
                data = classname;
            }
            else {
                data = WordChecker.addWord(data, classname);
            }

            $rootScope.$storage[storageKeyName] = angular.toJson(data);
        },

        // Remove a state from the browser storage
        removeState: function(classname){
            var data = $rootScope.$storage[storageKeyName];
            // nothing to remove
            if(!data) return;

            data = WordChecker.removeWord(data, classname);

            $rootScope.$storage[storageKeyName] = angular.toJson(data);
        },

        // Load the state string and restore the classlist
        restoreState: function($elem) {
            var data = angular.fromJson($rootScope.$storage[storageKeyName]);

            // nothing to restore
            if(!data) return;
            $elem.addClass(data);
        }

    };

}]);


/**
 * Form Helper
 * <whform type="text" label="Rubrique : " placeholder="Votre nom de rubrique" ng-model="data.Rub.name" error="error.name"></whform>
 */
App.directive("whform", [function() {

    return {
        restrict: 'EA',
        transclude: true,
        require: 'ngModel',
        scope : {
            ngModel     : '=',
            label       : '@',
            placeholder : '@',
            error       : '=',
            options     : '=',
            tagClass    : '@',
            legende     : '@',
            mask        : '@',
            disabled    : '@',
            type        : '@',
            inputStyle  : '@',
            append      : '@',
            required    : '@',
            inputmask   : '@'
        },
        templateUrl: function(elem, attr){

            var tag = (attr.tag) ? attr.tag : 'input';

            if(attr.options) tag = 'select';

            return '/app_admin/partials/helper-form/bootstrap/' + tag + '.html';

        },
        link: function(scope, element, attrs, ngModel) {

            if(attrs.inputmask) {

                var el = $('input', element);
                el.inputmask(attrs.inputmask);

            }


        }
    };


}]);
