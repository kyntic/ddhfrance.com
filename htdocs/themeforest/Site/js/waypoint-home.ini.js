(function($){
	$(document).ready(function(){
		var $blockHeight = $('.background-image-15').height();
		$('.tablet-image').css({top : $blockHeight});
		$('.background-image-15').waypoint(function(){
			$(this).find('.postslider').stop(true).delay(800).animate({opacity : 1},300,function(){
				$(this).closest('.row').find('.col-md-4').each(function(index){
					if($(this).index() == 3){
						$(this).find('.tablet-image').stop(true).animate({top : 0, opacity : 1}, 300, function(){
							$(this).parent().parent().find('.right-align div:first').stop(true).animate({opacity : 1}, 300, function(){
								$(this).parent().parent().find('.left-align div:first').stop(true).animate({opacity : 1}, 300, function(){
									$(this).parent().parent().find('.right-align div:last').stop(true).animate({opacity : 1}, 300, function(){
										$(this).parent().parent().find('.left-align div:last').stop(true).animate({opacity : 1}, 300);
									});
								});
							});
						});
					}
				});
			});
		}, {offset: '100%'});
	});
})(jQuery);
