(function($){
 $(window).load(function() {
	var QSandAnimationStart = true;
	// get the action filter option item on page load
	var $filterType = $('#qsource li.active a, #qsource-1 li.active a, #qsource-2 li.active a').attr('data-value');
	
	// get and assign the ourHolder element to the
	// $holder varible for use later
	var $holder = $('#qcontainer ul.q-sand-blog-target, #qcontainer ul.q-sand-target, .home-qsand-target, .home-blog-qsand-target');
  
	// clone all items within the pre-assigned $holder element
	var $data = $holder.clone();
	
	// init services (set at first)
	servicesInit(true, $holder, $data, $filterType);


	// page resize response
	$(window).resize(function() {
    resizeDelay(function(){
      $('#qsource li.active a, #qsource-1 li.active a, #qsource-2 li.active a').trigger('click');
    }, 100);
	});

	// attempt to call Quicksand when a filter option
	// item is clicked
	$('#qsource li a, #qsource-1 li a, #qsource-2 li a').click(function(e) {
	
		e.preventDefault();
		
		// reset the active class on all the buttons
		$('#qsource li, #qsource-1 li, #qsource-2 li').removeClass('active');
		
		// assign the class of the clicked filter option
		// element to our $filterType variable
		var $filterType = $(this).attr('data-value');
		$(this).parent().addClass('active');
		
		servicesInit(false, $holder, $data, $filterType);
	});
});

var resizeDelay = (function(){
  var timer = 0;
  return function(callback, ms){
    clearTimeout (timer);
    timer = setTimeout(callback, ms);
  };
})();

function  servicesInit(QSandInit, $holder, $data, $filterType) {
	
	if ($filterType == 'all') {
		// assign all li items to the $filteredData var when
		// the 'All' filter option is clicked
		var $filteredData = $data.find('li');
	} 
	else {
		// find all li elements that have our required $filterType
		// values for the data-type element
		var $filteredData = $data.find('li[data-type~="' + $filterType + '"]');
		
	}

	if(!QSandInit){
		// call quicksand and assign transition parameters
		$holder.quicksand($filteredData, {
			adjustHeight: 'dynamic',
			duration: 800,
			easing: 'easeInOutQuad'
		}, function(){
			ExpandCollapse();
		});
	}
	else {
		// call quicksand and assign transition parameters
		$holder.quicksand($filteredData, {
			duration: 1
		}, function(){
			ExpandCollapse();
		});
	}
	
}

function ExpandCollapse(){
	var $ImageHeight = $('.blog-post a').height();
	var $constantElement = $('.blog-post .always-visible-element').height();
	var $staticHeight = $ImageHeight + $constantElement + 92;
	var $dynamicElement = $('.border-box.padding-left48').height();
	var $dynamicHeight = $dynamicElement + $ImageHeight;
	var $statSocBar = $('.static-social-bar').height();
	$('.blog-post').css({"height" : $staticHeight});
	$(document).on('click', '.always-visible-element h4 a', function(){
		$(this).parent().parent().parent().parent().find('a.read-more-link').trigger('click');
	});
	$(document).on('click', 'a.write-img, .reply-blog-post', function(){
		$(this).parent().parent().parent().find('a.read-more-link').trigger('click');
		var $elementFromTop = $(this).parent().parent().parent().parent().parent().parent().find('#blogpost-form').offset().top;
		$('html, body').delay(200).animate({scrollTop : $elementFromTop - 48},800);
	});
	$(document).on('click', 'a.read-more-link', function(){
			$('body').find('.opened-element').find('a.read-less-link').trigger('click');
			$(this).parent().parent().parent().parent().parent().stop(true).animate({height : $dynamicHeight},1200).addClass('opened-element');
			$(this).parent().parent().parent().parent().stop(true).animate({height : 0},300);	
	});
	$(document).on('click', 'a.read-less-link', function(){
		$(this).parent().parent().parent().parent().parent().parent().find('.blog-post').stop(true).animate({height : $staticHeight},800).removeClass('opened-element');
		$('.static-social-bar').stop(true).animate({height : '68px'},300);
	});	
}

})(jQuery);