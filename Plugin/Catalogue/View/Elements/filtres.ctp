<?= $this->Form->create('Filtres', array('id' => 'FiltresForm')); ?>
    <?= $this->Filtres->generateFiltresSelectionnes($filtres['attributeFamilies'], $filtres['attributes']); ?>
    <?php $this->Filtres->html = ''; ?>
    <?= $this->Filtres->generateFiltres($filtres['attributeFamilies'], $filtres['attributes']); ?>
    <?= $this->WhForm->submit('Lancer la recherche'); ?>
<?= $this->Form->end(); ?>

<?php
	$prixMini = 0;
	if (!empty($this->data['Filtres']['prix_mini'])) {
		$prixMini = $this->data['Filtres']['prix_mini'];
	}
	$prixMaxi = 100;
	if (!empty($this->data['Filtres']['prix_maxi'])) {
		$prixMaxi = $this->data['Filtres']['prix_maxi'];
	}
?>

<?php $this->Html->scriptStart($options = array('block' => 'scriptBottom')); ?>

    $('#FiltresForm ul.caracteristiqueFamilles li a').click(function(){

        $(this).parent('li').toggleClass('open');
        return false;

    });

	$('#FiltresForm .filtresSelectionnes .filtre a').click(function(){

		$('#FiltresForm ' + $(this).attr('href')).attr('checked', false);
		$(this).parent('.filtre').remove();
		return false;

	});

	$('#sliderPrix').slider({
		range: true,
		min: 0,
		max: 100,
		values: [<?= $prixMini; ?>, <?= $prixMaxi; ?>],
		slide: function(event, ui) {
			$('#FiltresForm > ul.caracteristiqueFamilles > li.prix .labels span.min').html(ui.values[0] + '€');
			$('#FiltresForm > ul.caracteristiqueFamilles > li.prix .labels span.max').html(ui.values[1] + '€');
			$('#FiltresPrixMini').val(ui.values[0]);
			$('#FiltresPrixMaxi').val(ui.values[1]);
		}
	});

	$('#FiltresForm > ul.caracteristiqueFamilles > li.prix .labels span.min').html($('#sliderPrix').slider("values", 0) + '€');
	$('#FiltresForm > ul.caracteristiqueFamilles > li.prix .labels span.max').html($('#sliderPrix').slider("values", 1) + '€');
	$('#FiltresPrixMini').val($('#sliderPrix').slider("values", 0));
	$('#FiltresPrixMaxi').val($('#sliderPrix').slider("values", 1));

	$('#FiltresForm input[type="checkbox"]').change(function(){
		$('#FiltresForm').submit();
	});

<?php $this->Html->scriptEnd(); ?>