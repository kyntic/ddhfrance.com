<?php

class ProductAttribute extends CatalogueAppModel {

	public function beforeSave($options = array())
	{

		if (!empty($this->data['ProductAttribute']['attribute_id'])) {

			$Attribute = ClassRegistry::init('Catalogue.Attribute');
			$attribute = $Attribute->findById($this->data['ProductAttribute']['attribute_id']);

			if (!empty($attribute)) {

				$this->data['ProductAttribute']['attribute_family_id'] = $attribute['Attribute']['attribute_family_id'];

			}

		}

	}

    public function getAttributes($productId) {

        $attributeIds = $this->find(
            'list',
            array(
                'fields' => array(
                    'ProductAttribute.attribute_id',
                    'ProductAttribute.attribute_id'
                ),
                'conditions' => array(
                    'ProductAttribute.product_id' => $productId
                )
            )
        );

        if (!$attributeIds) {
            $attributeIds = array();
        }

        return $attributeIds;

    }

    public function getAttributesByFamily($productId) {

        $attributes = $this->find(
            'all',
            array(
                'conditions' => array(
                    'ProductAttribute.product_id' => $productId
                )
            )
        );

        $attributeIds = array();

        foreach ($attributes as $attribute) {
            $attributeIds[$attribute['ProductAttribute']['attribute_family_id']][] = $attribute['ProductAttribute']['attribute_id'];
        }

        return $attributeIds;

    }

    public function saveAttributes($productId, $attributeIds)
    {

        if (!$this->deleteAttributes($productId)) {

            return false;
        }

        foreach ($attributeIds as $key => $attributeId) {

            if (!$this->save(
                array(
                    'id'            =>  null,
                    'product_id'	=>	$productId,
                    'attribute_id'	=>  $attributeId
                )
            )) {

                return false;
            }

        }

        return true;

    }

    public function deleteAttributes($productId)
    {

        if($this->deleteAll(
            array(
                'ProductAttribute.product_id'	=>	$productId
            )
        )) {

            return true;
        }

        return false;

    }

    /**
     * Retourne les filtres
     */
    public function getFiltres($utiliseProductIds = false, $productIds = array()) {

        $AttributeFamily = ClassRegistry::init('Catalogue.AttributeFamily');
        $Attribute = ClassRegistry::init('Catalogue.Attribute');

        if ($utiliseProductIds) {

            $productAttributes = $this->find(
                'all',
                array(
                    'fields' => array(
                        'ProductAttribute.attribute_id',
                        'ProductAttribute.attribute_family_id'
                    ),
                    'conditions' => array(
                        'ProductAttribute.product_id' => $productIds
                    )
                )
            );

            $attributeFamiliesIds = array();
            $attributesIds = array();

            foreach ($productAttributes as $productAttribute) {
                $attributeFamiliesIds[$productAttribute['ProductAttribute']['attribute_family_id']] = $productAttribute['ProductAttribute']['attribute_family_id'];
                $attributesIds[$productAttribute['ProductAttribute']['attribute_id']] = $productAttribute['ProductAttribute']['attribute_id'];
            }

        }

        $conditions = array();
        if ($utiliseProductIds) {

            foreach ($attributeFamiliesIds as $attributeFamilyId) {

                $parents = $AttributeFamily->getPath($attributeFamilyId);
                foreach ($parents as $parent) {
                    $attributeFamiliesIds[$parent['AttributeFamily']['id']] = $parent['AttributeFamily']['id'];
                }

            }

            $conditions['AttributeFamily.id'] = $attributeFamiliesIds;
        }

        $attributeFamilies = $AttributeFamily->find(
            'threaded',
            array(
                'conditions' => $conditions,
                'order' => array(
                    'AttributeFamily.position' => 'ASC'
                )
            )
        );

        foreach ($attributeFamilies as &$attributeFamily) {

            $attributeFamily['AttributeFamily']['childrenIds'][] = $attributeFamily['AttributeFamily']['id'];
            $children = $AttributeFamily->children($attributeFamily['AttributeFamily']['id']);
            foreach ($children as $child) {
                $attributeFamily['AttributeFamily']['childrenIds'][] = $child['AttributeFamily']['id'];
            }

        }

        $conditions = array();
        if ($utiliseProductIds) {
            $conditions['Attribute.id'] = $attributesIds;
        }

        $attributes = array();
        $tmpAttributes = $Attribute->find(
            'threaded',
            array(
                'conditions' => $conditions,
                'order' => array(
                    'Attribute.position' => 'ASC'
                )
            )
        );

        foreach ($tmpAttributes as $tmpAttribute) {
            $attributes[$tmpAttribute['Attribute']['attribute_family_id']][] = $tmpAttribute;
        }

        return array(
            'attributeFamilies' =>  $attributeFamilies,
            'attributes'        =>  $attributes
        );

    }

    public function cleanData($data)
    {

        foreach ($data as $attributeFamilyId => $attributes) {

            foreach ($attributes as $attributeId => $value) {

                if ($value != 1) {

                    unset($data[$attributeFamilyId][$attributeId]);

                }

            }

        }

        return $data;

    }

    /**
     * Filtre la liste des productIds envoyés à partir des datas
     */
    public function appliqueFiltres($data, $productIds) {

        $attributeFamilies = array();

        if (empty($data)) {
            return $productIds;
        }

        foreach ($data as $attributeFamilyId => $attributes) {

            foreach ($attributes as $attributeId => $value) {

                if ($value == 1) {

                    $attributeFamilies[$attributeFamilyId][] = $attributeId;

                }

            }

        }

        foreach ($attributeFamilies as $attributeFamilyId => $attributeIds) {

            $attributeFamilyProductIds = $this->find(
                'list',
                array(
                    'fields' => array(
                        'ProductAttribute.product_id',
                        'ProductAttribute.product_id'
                    ),
                    'conditions' => array(
                        'ProductAttribute.attribute_family_id'  =>  $attributeFamilyId,
                        'ProductAttribute.attribute_id'         =>  $attributeIds
                    )
                )
            );

            if (!$attributeFamilyProductIds) {
                $attributeFamilyProductIds = array();
            }

            $productIds = array_intersect($productIds, $attributeFamilyProductIds);

        }

        return $productIds;

    }

}