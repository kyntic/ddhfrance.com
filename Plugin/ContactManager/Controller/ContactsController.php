<?php
class ContactsController extends ContactManagerAppController {
    
    public $uses = array('ContactManager.Contact');
    public $components = array('RequestHandler');
   
/**
 * Action index affiche la page contact et envoie d'un message
 *
 * @id
 */
    public function index ($id = null)
    {

        //$this->Cont = $this->Contact->findById($id);

        //if(!$id) $this->Cont = $this->Contact->find('first', array('conditions' => array('Contact.type' => 'default')));


        //Si on recoit des données : 
        if(!empty($this->data)) {


            if(!$this->request->is('ajax')) { // Si c'est un envoie Ajax

                $this->disableCache();
                $this->layout = 'ajax';
                $statut = array();

            }

            $data = $this->request->data;


            $data['Contact']['id'] = $this->Cont['Contact']['id'];


            $this->Contact->set($data);

            if($this->Contact->validates($data)) {

                if($this->Contact->send($data)) {

                    $statut['statut'] = 1;
                    $statut['message']['titre']     = 'Votre message a été envoyé !';
                    $statut['message']['texte']     = 'Nous reprendrons contact avec vous dans les plus brefs délais. ';

                }else{

                    $statut['statut'] = 0;
                    $statut['message']['titre']     = 'Erreur lors de l’envoi du message !';
                    $statut['message']['texte']     = 'Merci d’essayer à nouveau. ';                       
                }

            }else{

                $statut['statut']               = 0;
                $statut['erreurs']              = $this->Contact->validationErrors;
                $statut['message']['titre']     = 'Le formulaire contient des erreurs !';
                $statut['message']['texte']     = 'Veuillez contrôler les champs marqués de rouge. ';                    

            }

                  
            exit(json_encode($statut));

        }

        //Breadcrumb 
        //$this->set('_breadcrumb', array('Model' => 'MenuItem', 'Chemin' => $this->Contact->MenuItem->getPath($this->Cont['MenuItem']['id'])));



    }

    /**
     * Admin new function
     *
     * @param null $id
     * @return void
     */
    public function admin_new()
    {


        if(!empty($this->data)) {

            $data = $this->data;

            $data['Contact']['content_type_id'] = 'contact';
            $data['Contact']['h1'] = $data['Contact']['name'];

            if($this->Contact->saveAll($data, array('validate' => false))) {

                $this->Session->setFlash('Contact enregistré', 'success');

                if(!empty($this->data['Contact']['edit'])) {

                    $this->redirect('/admin/contact_manager/contacts/edit/'.$this->Contact->getLastInsertID());

                }else{

                    $this->redirect('/admin/menus/');

                }

            }

        }

        $this->set('MenuItems', $this->Contact->MenuItem->generateTreeList(null, '{n}.MenuItem.id', '{n}.MenuItem.name', ' - '));

        $this->layout = 'admin_popup';

    }




/**
 * Action edit : Modifie la page d'accueil
 *
 * 
 */

    public function admin_edit ($id = null) {

        $ContentType = $this->Contact->ContentType->findById('contact');

        if(!empty($this->data)) {

            $data = $this->data;

            $data['Contact']['content_type_id']     = $ContentType['ContentType']['id'];
            $data['MenuItem']['content_type_id']    = $ContentType['ContentType']['id'];

            
            if($this->Contact->saveAll($data, array('validate' => false))) {

                $this->Session->setFlash('Contact enregistrée', 'success');

                if(!empty($this->data['Contact']['nouveau'])) {

                    $this->redirect('/admin/contact_manager/contacts/edit/edit');

                }else{

                    $this->redirect('/admin/menus');

                }

            }else{

                $this->Session->setFlash('Une erreur est survenue', 'error');

            }


        }elseif($id) {

            $this->data = $this->Contact->findById($id);

        }


        $this->loadModel('MenuItem');
        $this->set('MenuItems', $this->MenuItem->generateTreeList(null, '{n}.MenuItem.id', '{n}.MenuItem.name', ' - '));
        $this->set('MenuAdminActives', array(85));



    }


/**
 * Action delete 
 *
 * 
 */
    public function admin_delete ($id) {

        $this->Contact->delete($id);
        $this->Session->setFlash(__('Page supprimé'), 'success');
        $this->redirect(Controller::referer());

    }



}
?>