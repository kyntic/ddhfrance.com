<?php
$this->extend('/Templates/template_default');

$this->element('balises_metas', array('metas' => $Cont['Page']));

foreach($_breadcrumb['Chemin'] as $v) $this->Html->addCrumb($v[$_breadcrumb['Model']]['name'], $v[$_breadcrumb['Model']]['url']);

$this->assign('h1', $Cont['Page']['h1']);
$this->assign('body', $Cont['Page']['body']);

if(!empty($_menuPages)) $this->assign('menu_col', $this->element('menu_col', array('menus' => $_menuPages)));
if(!empty($evenements)) $this->assign('event_col', $this->element('Blog.liste_evenement_col', array('evenements' => $evenements,'menuItemId' => $Cont['Page']['menu_item_id'])));




//Petit resume sur la colone de droite
if(!empty($Cont['Page']['resume']))
{
    $this->start('resume_col');
    ?>
    <section class="widget has-divider">
        <h3 class="title"><?php echo $this->fetch('h1');?></h3>
        <p><?php echo $Cont['Page']['resume'];?></p>
    </section>
    <?php
    $this->end();
}


//Afficahge de la vignette
$this->start('vignette');
if(!empty($Cont['Vignette']['id'])) ?> <p class="featured-image"><?php echo $this->WhFile->show($Cont['Vignette'],array() ,array('class' => 'img-responsive'));?></p>
<?php
$this->end();



//Affichage petite infos date auteur
$this->start('date_maj');
?>
    <p class="meta text-muted">
        <?php if(!empty($Cont['Page']['auteur'])) ?>Par: <a href="#"><?php echo $Cont['User']['nom'].' '.$Cont['User']['prenom'];?></a>
        | Posté le: <?php echo $Cont['Page']['created'];?>
    </p>
<?php
$this->end();

//affichage des actualites
$this->start('actualites');
?>
    <div id="actualites" class="contenuPage col-md-8 col-sm-7">
        <h2 class="title">Retrouvez ici l'ensemble de nos articles</h2>

        <?php
        foreach($actualites as $k => $actualite)
        {
            ?>
            <article class="news-item page-row has-divider clearfix row">
                <?php if(!empty($actualite['Vignette']['id'])){ ?>
                    <figure class="thumb col-md-2 col-sm-3 col-xs-4">
                        <?php echo $this->WhFile->show($actualite['Vignette'], array(), array('class' => 'img-responsive'));?>
                    </figure>
                <?php }?>
                <div class="details col-md-10 col-sm-9 col-xs-8">
                    <h3 class="title"><?php echo $this->Html->link($actualite['Actualite']['name'],$actualite['Actualite']['url'].'/page:'.$Cont['Page']['menu_item_id']);?></h3>
                    <p><?php echo $actualite['Actualite']['resume'];?></p>
                    <?php echo $this->Html->link('En savoir +<i class="fa fa-chevron-right"></i>', $actualite['Actualite']['url'].'/page:'.$Cont['Page']['menu_item_id'], array('class' => 'btn btn-theme read-more','escape' => false));?>
                </div>
            </article>
        <?php
        }

        ?>

        <?php $pagination = $this->Paginator->params();?>
        <ul class="pagination">
            <?php
            $prevClass = ($pagination['prevPage'])? '' : 'disabled';
            $nextClass = ($pagination['nextPage'])? '' : 'disabled';

            if(!empty($actualites))
            {
                if($pagination['pageCount'] > 1 )echo '<li class="'.$prevClass.'">'.$this->Paginator->prev('«', array('tag' => 'li'), null, array('class' => '')).'</li>';
                echo $this->Paginator->numbers(array('separator' => '', 'class' => 'pagination pagination-sm', 'currentClass' => 'active', 'first' => 1, 'last' => 1, 'tag' => 'li','lien_on_current' =>true));
                if($pagination['pageCount'] > 1 )echo '<li class="'.$nextClass.'">'.$this->Paginator->next('»', array('tag' => 'li'), null, array('class' => '')).'</li>';
            }
            ?>

        </ul>

    </div>
<?php
$this->end();