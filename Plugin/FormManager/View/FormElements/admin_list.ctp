<table class="table table-normal table-striped">
    <thead>
    <tr>
        <th class="col-md-1">Admin</th>
        <th>Nom de l'élément</th>
    </tr>
    </thead>

    <tbody class="sortable">
    <?php foreach($elements as $element) : ;?>
        <tr id="page_<?php echo $element['FormElement']['id'];?>">
            <td>
                <?php echo $this->Html->link(
                    '<i class="fa fa-edit"></i>',
                    '/admin/form_manager/form_elements/edit/' . $element['FormElement']['id'],
                    array(
                        'escape'            => false,
                        'class'             => 'btn btn-primary btn-xs',
                        'data-toggle'       => 'modal',
                        'data-target'       => '#myModal',
                    )
                );?>
                <?php echo $this->Html->link(
                    '<i class="fa fa-tag"></i>',
                    '/admin/form_manager/form_elements/configure/' . $element['FormElement']['id'],
                    array(
                        'escape'        => false,
                        'class'         => 'btn btn-success btn-xs',
                        'data-toggle'   => 'modal',
                        'data-target'   => '#myModal',
                    )
                );?>
                <?php echo $this->Html->link(
                    '<i class="fa fa-trash-o"></i>',
                    '/admin/form_manager/form_elements/delete/' . $element['FormElement']['id'],
                    array(
                        'escape'    => false,
                        'class'     => 'btn btn-danger btn-xs'
                    )
                );?>
            </td>
            <td><?php echo $element['FormElement']['name'];?></td>
        </tr>
    <?php endforeach;?>
    </tbody>
</table>

<script type="text/javascript">

    $(document).ready( function(){

        $('.sortable').each(function() {

            var el = $(this);

            el.sortable({
                placeholder : 'highlight',
                axis        : 'y',
                stop        : function(event, ui) {

                    el.showLoading();

                    $.ajax({
                        type      : 'POST',
                        url       : '/admin/form_manager/form_elements/ordre',
                        data      : el.sortable('serialize'),
                        cache     : false,
                        complete  : function () {el.hideLoading()},
                        success   : function () {

                            el.hideLoading();

                        }
                    });
                }
            });

            $(this).disableSelection(); // on désactive la possibilité au navigateur de faire des sélections
        });
    });
</script>