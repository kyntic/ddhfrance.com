<?php
$this->extend('/Templates/template_default');
$this->element('balises_metas', array('metas' => $Cont['Actualite']));

$this->assign('h1', $Cont['Actualite']['h1']);

foreach($_breadcrumb['Chemin'] as $v) $this->Html->addCrumb($v[$_breadcrumb['Model']]['name'], $v[$_breadcrumb['Model']]['url']);

$this->start('body');
?>
    /Plugin/View/Actualites/realisation.ctp
    <div class="full-width-container white-bg">
        <div class="container padding-bottom24">
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-8 margin-top24">
                    <?php


                    if(!empty($contentModulesIframe)) {
                        foreach ($contentModulesIframe as $k => $v) {
                            echo $v['ContentModule']['content']->txt;
                        }
                    }
                    if(!empty($Cont['Actualite']['vignette'])){
                        echo $this->Html->image($Cont['Actualite']['vignette']->File->url, array('style' => 'width:100%;'));
                    }

                    ?>
                </div>
                <div class="col-md-4 margin-top24">
                    <h3><?=$Cont['Actualite']['name'];?></h3>

                    <div class="margin-bottom24 bodyMetier" style="display:inline-block;"><?=$Cont['Actualite']['body'];?></div>
                    <div class="margin-top12">
                        <ul class="margin0 no-style blogpost-socials padding0">


                            <ul class="margin0 no-style blogpost-socials padding0">
                                <li><a data-color="#3b5998" class="relative" href="https://www.facebook.com/sharer.php?u=<?=FULL_BASE_URL.$this->here.'&t='.$Cont['Actualite']['h1'];?>"><img class="static" src="/themeforest/Site/images/port-item/1.png" alt=""><img class="dynamic" src="/themeforest/Site/images/port-item/1-1.png" alt=""><span class="port-item-soch"></span></a></li>
                                <li><a data-color="#007bb6" class="relative" href="#"><img class="static" src="/themeforest/Site/images/port-item/2.png" alt=""><img class="dynamic" src="/themeforest/Site/images/port-item/2-2.png" alt=""><span class="port-item-soch"></span></a></li>
                                <li><a data-color="#cb2027" class="relative" href="#"><img class="static" src="/themeforest/Site/images/port-item/3.png" alt=""><img class="dynamic" src="/themeforest/Site/images/port-item/3-3.png" alt=""><span class="port-item-soch"></span></a></li>
                                <li><a data-color="#00aced" class="relative" href="https://twitter.com/share?url=<?=FULL_BASE_URL.$this->here.'&text='.$Cont['Actualite']['h1'].'&via=akatim';?>"><img class="static" src="/themeforest/Site/images/port-item/4.png" alt=""><img class="dynamic" src="/themeforest/Site/images/port-item/4-4.png" alt=""><span class="port-item-soch"></span></a></li>
                            </ul>
                        </ul>
                    </div>
                </div>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>

    <!-- Autre Block -->
<?php

if (!empty($ContentModules)) {

    foreach ($ContentModules as $k => $module) {

        switch ($module['ContentModule']['module']) {

            default :

	            $element = 'ModuleManager.' . $module['ContentModule']['module'];
	            if (!empty($module['ContentModule']['template'])) {
		            $element .= $module['ContentModule']['template'];
	            }

                echo $this->element(
                    $element,
                    array(
                        'content' => $module['ContentModule'],
                        'classBlock' => ($k%2) ? 'pale-gray-bg' : 'white-bg'
                    )
                );

                break;
        }
    }
}

?>


    <!-- LISTE DES REFERENCES -->
<?php if(!empty($Refs)) : ;?>
    <div class="full-width-container pale-gray-bg gray-border-top">
        <div class="container padding-top96 padding-bottom96">
            <div class="margin-bottom-2 margin-top12">
                <h3 class="padding-bottom12">VOIR AUSSI</h3>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <div id="myCarousel2" class="carousel slide carousel-3col margin0">
                        <div class="carousel-nav">
                            <a class="carousel-nav-3col font-dosis black-letters pull-left font600" href="#myCarousel2" data-slide="prev">&lt;&nbsp;PRECEDENT</a>
                            <a class="carousel-nav-3col font-dosis black-letters pull-left font600" href="#myCarousel2" data-slide="next">SUIVANT&nbsp;&gt;</a>
                            <div class="clearfix"></div>
                        </div>
                        <div class="carousel-inner">
                            <?php
                            foreach($Refs as $k => $v) : ;
                                $e = $k%3;

                                if($e == 0) {

                                    $active = (!$k) ? 'active' : '';
                                    echo '<div class="item '.$active.'">';
                                    echo '<div class="row">';
                                    $open = 1;

                                }
                                ?>

                                <div class="col-md-4 border-box relative overflow-hidden item element design isotope-item">
                                    <?=$this->element('bloc_real', array('v' => $v));?>
                                </div>

                                <?php
                                $this->Html->scriptStart(array('block' => 'scriptSuperBottom', 'inline' => false));
                                ?>
                                $('.carousel-image').carousel({
                                interval: 5000,
                                pause: null
                                });
                                <?php
                                $this->Html->scriptEnd();
                                ?>

                                <?php
                                if($e == 2) {

                                    echo '</div>';
                                    echo '</div>';

                                    $open = 0;

                                }
                            endforeach;

                            if($open) echo '</div></div>';
                            ?>

                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>

<?php endif;?>



<?php
$this->end();
?>