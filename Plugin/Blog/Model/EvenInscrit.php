<?php

/**
 * Class EvenInscrit
 */
class EvenInscrit extends AppModel
{
    /**
     * The validate
     *
     * @var array $validate
     */
    public $validate = array(
        'first_name' => array(
            'required'  => true,
            'rule'      => 'alphaNumeric',
            'message'   => 'Lettres seulement | Ne peut pas être vide'
        ),
        'name' => array(
            'required'  => true,
            'rule'      => 'alphaNumeric',
            'message'   => 'Lettres seulement | Ne peut pas être vide'
        ),
        'email' => array(
            'emailValidation' => array(
                'required'  => true,
                'rule'      => 'email',
                'message'   => 'Format d\'email invalide'
            )
        ),
        'even_id' => array(
            'required'  => true,
            'rule'      => 'notEmpty'
        )
    );
}