<?php

/**
 * Class Agenda
 */
class Agenda extends AppModel
{

    public $belongsTo = array(
        'LastEven' => array(
            'className'     => 'Blog.Actualite',
            'foreignKey'    => 'last_even_id'
        )
    );


    public function afterFind ($results, $primary = false) {

        foreach($results as &$v) {

            if(!empty($v[$this->alias]['name']) && !empty($v[$this->alias]['id'])) {

                $v[$this->alias]['url_ics'] = FULL_BASE_URL.'/agenda/'.strtolower(Inflector::slug($v[$this->alias]['name'])).'-'.$v[$this->alias]['id'].'.ics';

            }

        }

        return $results;

    }

}
