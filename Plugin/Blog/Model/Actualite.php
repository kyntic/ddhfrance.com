<?php

/**
 * Class Actualite
 */
class Actualite extends AppModel
{
    public $belongsTo = array(
        'ContentType',
        'Etat',
        'Page',
        'Auteur' => array(
            'className'     => 'User',
            'foreignKey'    => 'auteur_id'
        )

    );



    /**
     * Before Save
     * Modificaiton du format de la date
     * @return bool
     */
    public function beforeSave($options = Array())
    {

        if (isset($this->data[$this->alias]['publication_date_fr']) && !empty($this->data[$this->alias]['publication_date_fr']) && !empty($this->data[$this->alias]['etat_id']) && $this->data[$this->alias]['etat_id'] == 'publish_date') {

            $this->data[$this->alias]['publication_date'] = preg_replace('#([0-9]{2})/([0-9]{2})/([0-9]{4})#', '$1-$2-$3', $this->data[$this->alias]['publication_date_fr']);
            $this->data[$this->alias]['publication_date'] = strtotime($this->data[$this->alias]['publication_date']);


        }

        if (isset($this->data[$this->alias]['creation_fr']) && !empty($this->data[$this->alias]['creation_fr'])) {

            $this->data[$this->alias]['created'] = preg_replace('#([0-9]{2})/([0-9]{2})/([0-9]{4})#', '$3-$2-$1 00:00:00', $this->data[$this->alias]['creation_fr']);
        }

        $this->data[$this->alias]['from'] = Router::url( $this->here, true );

        if (isset($this->data[$this->alias]['publication_date_fin_fr']) && !empty($this->data[$this->alias]['publication_date_fin_fr'])) {

            $this->data[$this->alias]['publication_date_fin'] = preg_replace('#([0-9]{2})/([0-9]{2})/([0-9]{4})#', '$3-$2-$1', $this->data[$this->alias]['publication_date_fin_fr']);
            $this->data[$this->alias]['publication_date_fin'] = strtotime($this->data[$this->alias]['publication_date_fin']);


        }

        if (empty($this->data[$this->alias]['name']) && !empty($this->data[$this->alias]['h1'])) {

            $this->data[$this->alias]['name'] = $this->data[$this->alias]['h1'];

        }

        if(empty($this->data[$this->alias]['id']) && empty($this->data[$this->alias]['type'])) $this->data[$this->alias]['type'] = 'actu';

        if(!empty($this->data[$this->alias]['vignette']) && is_array($this->data[$this->alias]['vignette'])) $this->data[$this->alias]['vignette'] = json_encode($this->data[$this->alias]['vignette']);

        if(!empty($this->data[$this->alias]['id'])) {

            $this->data[$this->alias]['auteur_id'] = AuthComponent::user('id');

        }

        return true;

    }

    /**
     * After Find :
     * Création d'une url si inexistance
     * @param mixed $results
     * @return mixed
     */
    public function afterFind($results, $primary = false)
    {

        foreach ($results as &$v) {

            if (!empty($v[$this->alias])) {

                if (isset($v[$this->alias]['url']) && !empty($v[$this->alias]['id'])) $v[$this->alias]['url'] = $v['Page']['url']. DS . strtolower(Inflector::slug($v[$this->alias]['name'])) . DS . 'actu-' . $v[$this->alias]['id'].'.html';

                if(!empty($v[$this->alias]['url_r'])) $v[$this->alias]['url'] = $v[$this->alias]['url_r'];

                if (!empty($v[$this->alias]['vignette'])) {

                    $v[$this->alias]['vignette'] = json_decode($v[$this->alias]['vignette']);

                }

            }

            if(isset($v[$this->alias]['publication_date']) && !empty($v[$this->alias]['publication_date'])) {

                $v[$this->alias]['publication_date_fr'] = date('d/m/Y', $v[$this->alias]['publication_date']);

            }

            if(isset($v[$this->alias]['publication_date_fin']) && !empty($v[$this->alias]['publication_date_fin'])) {

                $v[$this->alias]['publication_date_fin_fr'] = date('d/m/Y', $v[$this->alias]['publication_date_fin']);

            }

            if (isset($v[$this->alias]['created']) && !empty($v[$this->alias]['created'])) {

                $v[$this->alias]['creation_fr'] = preg_replace('#([0-9]{4})-([0-9]{2})-([0-9]{2})#', '$3/$2/$1', $v[$this->alias]['created']);
            }


        }

        return $results;


    }



}
