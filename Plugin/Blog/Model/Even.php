<?php

/**
 * Class Actualite
 */
class Even extends AppModel
{
    /**
     * The use table
     *
     * @var string $useTable
     */
    public $useTable = 'actualites';

    /**
     * The belongs to
     *
     * @var array $belongsTo
     */
    public $belongsTo = array(
        'ContentType',
        'Etat',
        'Blog.Agenda',
        'Page'
    );

    /**
     * The virtual fields
     *
     * @var array $virtualFields
     */
    public $virtualFields = array(
        'date_even_fin_int' => 'UNIX_TIMESTAMP(Even.even_date_fin)',
        'date_even_deb_int' => 'UNIX_TIMESTAMP(Even.even_date_deb)'
    );

    /**
     * The before save
     *
     * @param   array   $options
     * @return  bool
     */
    public function beforeSave($options = array())
    {
        if (isset($this->data[$this->alias]['publication_date_js'])) {

            if (!empty($this->data[$this->alias]['publication_date_js'])) {

                $date = new DateTime($this->data[$this->alias]['publication_date_js']);
                $this->data[$this->alias]['publication_date'] = $date->format('Y-m-d');

            }

        }

        if (isset($this->data[$this->alias]['all_day']) && $this->data[$this->alias]['all_day']) {

            $this->data[$this->alias]['heure_even_deb_fr'] = '00:00';
            $this->data[$this->alias]['heure_even_fin_fr'] = '00:00';

        }

        if (!empty($this->data[$this->alias]['publication_date_fr'])) {

            $this->data[$this->alias]['publication_date'] = preg_replace('#([0-9]{2})/([0-9]{2})/([0-9]{4})#', '$3-$2-$1' , $this->data[$this->alias]['publication_date_fr']);

        }

        if (!empty($this->data[$this->alias]['date_even_deb_fr'])) {

            $this->data[$this->alias]['even_date_deb'] = preg_replace('#([0-9]{2})/([0-9]{2})/([0-9]{4})#', '$3-$2-$1' , $this->data[$this->alias]['date_even_deb_fr']);


            if (!empty($this->data[$this->alias]['heure_even_deb_fr'])) $this->data[$this->alias]['even_date_deb'] .= ' '.$this->data[$this->alias]['heure_even_deb_fr'];

        }

        if (!empty($this->data[$this->alias]['date_even_fin_fr'])) {

            $this->data[$this->alias]['even_date_fin'] = preg_replace('#([0-9]{2})/([0-9]{2})/([0-9]{4})#', '$3-$2-$1' , $this->data[$this->alias]['date_even_fin_fr']);

            if (!empty($this->data[$this->alias]['heure_even_fin_fr'])) $this->data[$this->alias]['even_date_fin'] .= ' '.$this->data[$this->alias]['heure_even_fin_fr'];

        }

        if (empty($this->data[$this->alias]['name']) && !empty($this->data[$this->alias]['h1'])) {

            $this->data[$this->alias]['name'] = $this->data[$this->alias]['h1'];

        }

        if(!empty($this->data[$this->alias]['vignette']) && is_array($this->data[$this->alias]['vignette'])) $this->data[$this->alias]['vignette'] = json_encode($this->data[$this->alias]['vignette']);


        return true;

    }

    /**
     * After Find :
     * Création d'une url si inexistance
     * @param mixed $results
     * @return mixed
     */
    public function afterFind($results, $primary = false)
    {

        foreach ($results as &$v) {


            if (!empty($v[$this->alias])) {

                if(!empty($v[$this->alias]['publication_date'])) $v[$this->alias]['publication_date_fr'] = preg_replace('#([0-9]{4})-([0-9]{2})-([0-9]{2})#', '$3/$2/$1' ,$v[$this->alias]['publication_date']);

                if(!empty($v[$this->alias]['even_date_fin'])) {

                    $v[$this->alias]['date_even_fin_fr'] = preg_replace('#([0-9]{4})-([0-9]{2})-([0-9]{2}) ([0-9]{2}):([0-9]{2}):([0-9]{2})#', '$3/$2/$1' ,$v[$this->alias]['even_date_fin']);
                    $v[$this->alias]['heure_even_fin_fr'] = preg_replace('#([0-9]{4})-([0-9]{2})-([0-9]{2}) ([0-9]{2}):([0-9]{2}):([0-9]{2})#', '$4:$5' ,$v[$this->alias]['even_date_fin']);
                }

                if(!empty($v[$this->alias]['even_date_deb'])) {

                    $v[$this->alias]['date_even_deb_fr'] = preg_replace('#([0-9]{4})-([0-9]{2})-([0-9]{2}) ([0-9]{2}):([0-9]{2}):([0-9]{2})#', '$3/$2/$1' ,$v[$this->alias]['even_date_deb']);
                    $v[$this->alias]['heure_even_deb_fr'] = preg_replace('#([0-9]{4})-([0-9]{2})-([0-9]{2}) ([0-9]{2}):([0-9]{2}):([0-9]{2})#', '$4:$5' ,$v[$this->alias]['even_date_deb']);
                }

                if (isset($v[$this->alias]['url']) && !empty($v[$this->alias]['id'])) $v[$this->alias]['url'] = $v['Page']['url']. DS . strtolower(Inflector::slug($v[$this->alias]['name'])) . DS . 'even-' . $v[$this->alias]['id'].'.html';

                if (!empty($v[$this->alias]['vignette'])) {

                    $v[$this->alias]['vignette'] = json_decode($v[$this->alias]['vignette']);
                }
            }
        }

        return $results;
    }
}