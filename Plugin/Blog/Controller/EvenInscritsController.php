<?php

/**
 * Class EvensController
 */
class EvenInscritsController extends BlogAppController
{
    /**
     * Admin index
     *
     * @return void
     */
    public function admin_index($id)
    {

        echo json_encode($this->EvenInscrit->find('all', array('order' => 'EvenInscrit.name ASC', 'conditions' => array('EvenInscrit.even_id' => $id))));

        exit();

    }

    /**
     * Admin edit : Enregistrer
     *
     * @param null $id
     */
    public function admin_edit($id = null)
    {

        if (!empty($this->data)) {

            $data = $this->data;

            if ($this->EvenInscrit->save($data)) {

                $res['ok'] = true;

            } else {

                $res['ok'] = false;
                $res['error'] = $this->EvenInscrit->invalidFields();
            }

            exit(json_encode($res));
        }

        exit();

    }


    /**
     * Admin delete
     *
     * @param $id
     */
    public function admin_delete($id)
    {

        $res['ok'] = false;

        $this->EvenInscrit->delete($id);

        $res['ok'] = true;

        echo json_encode($res);

        exit();


    }


}