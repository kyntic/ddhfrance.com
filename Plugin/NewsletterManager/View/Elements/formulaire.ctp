<h3>S'inscrire a la newletter</h3>
<p>Inscrivez vous pour recevoir nos mails hebdomadaires</p>

<?php
echo $this->Form->create('NewsletterInscrit', array('id' => 'NewsletterInscritInscriptionForm', 'url' => '/newsletter_manager/newsletter_inscrits/inscription' , 'class' => 'subscribe-form'));
?>
<div class="form-group">
    <?php echo $this->WhForm->input('email', '', array( 'class' => 'form-control' ,'label' => false, 'placeholder' => 'Email', 'div' =>false)); ?>
</div>
<input class="btn btn-theme btn-subscribe" type="submit" value="S'inscrire">
<?php echo $this->Form->end(); ?>


<?php $this->Html->scriptStart(array('inline' => false, 'block' => 'scriptBottom')); ?>
$('#NewsletterInscritInscriptionForm').on('submit', function (e) {

	var form = $(this);

	$(form).showLoading();
	$('.alert', this).remove();
	$('.error_text', this).remove();

	$.ajax({
	  type 		: 'POST',
	  url 		: $(this).attr('action'),
	  data 		: $(this).serialize(),
	  cache 	: false,
	  complete	: function () {$(form).hideLoading()},
	  success 	: function (reponse) {

	  	var reponse = jQuery.parseJSON(reponse);

	  	if(reponse.statut == 0) { //Erreur

	  		$('input', form).each(function (index) {

				var elt 	= $(this);
				var champ 	= elt.data('name');

				if(reponse['erreurs'][champ]) {

					var text_erreur = '<span class="error_text">' + reponse['erreurs'][champ][0] + '</p></span>';
 					elt.after(text_erreur);
					elt.css('background', '#f2dede');

	 			}

	  		});

	 		var alert = '<div class="alert alert-error"><h3 class="alert-heading">'+reponse.message.titre+'</h3><p>'+reponse.message.texte+'</p></div>';
 			form.prepend(alert);


	  	}

		if(reponse.statut == 1) { //ok

			form.html('<div class="alert alert-success"><h3 class="alert-heading">'+reponse.message.titre+'</h3><p>'+reponse.message.texte+'</p></div>');

		}

	  }

	});

	return false;

});

<?php $this->Html->scriptEnd();?>