<?php
$this->assign('titre', 'Ajout d’email manuel');
echo $this->Smart->create('NewsletterEmail', array('url' => '/admin/newsletter_manager/newsletter_emails/import_from_unique/' . $listId, 'class' => 'smart-form', 'id' => 'NewsletterEmailAdminImportFromUniqueForm'));
?>

<div class="modal-body">
    <fieldset>

        <?=$this->Smart->input('NewsletterEmail.email', 'Email');?>
        <?=$this->Smart->input('NewsletterEmail.first_name', 'Prénom');?>
        <?=$this->Smart->input('NewsletterEmail.last_name', 'Nom');?>

    </fieldset>
</div>
<div class="modal-footer">
    <footer>

        <?=$this->Form->button(__('Enregistrer'), array('type' => 'submit', 'class' => 'btn btn-primary btn-xs'));?>

    </footer>
</div>
<?=$this->Smart->end();?>

<script type="text/javascript" >
    $('#NewsletterEmailAdminImportFromUniqueForm').on('submit', function (e) {

        var form = $(this);

        $(form).showLoading();
        $('.alert', this).remove();
        $('.error_text', this).remove();

        $.ajax({
            type 		: 'POST',
            url 		: $(this).attr('action'),
            data 		: $(this).serialize(),
            cache 	: false,
            complete	: function () {$(form).hideLoading()},
            success 	: function (reponse) {

                var reponse = jQuery.parseJSON(reponse);

                if(reponse.status == 0) { //Erreur

                    var alert = '<div class="padded"><div class="alert alert-error" style="margin:0;" >Email non ajouté !</div></div>';
                    $(form).hideLoading();
                    form.prepend(alert);
                }

                if(reponse.status == 1) { //ok
                    chargeListeEmail();
                    $('#popup_ajx').modal('hide');
                }
            }
        });

        return false;
    });
</script>