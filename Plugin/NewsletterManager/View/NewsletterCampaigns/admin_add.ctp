<?php $this->assign('titre', 'Création de campagne'); ?>
<?=$this->WhForm->create('NewsletterCampaign', array('url' => '/admin/newsletter_manager/newsletter_campaigns/add', 'class' => 'form-horizontal fill-up'));?>
    <div class="padded">
        <?=$this->WhForm->input('NewsletterCampaign.name', 'Nom de la campagne');?>
    </div>
    <div class="modal-footer">
        <button class="btn btn-blue" type="submit">Enregistrer</button>
    </div>
<?=$this->WhForm->end();?>