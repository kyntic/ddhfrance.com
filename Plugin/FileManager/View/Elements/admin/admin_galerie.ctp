<?php 
$unique = uniqid();
$param_url = array(); 
foreach($params as $k => $v) $param_url[] = $k.':'.$v; 
$param_url = implode('/', $param_url); //Traformation du tableau en chaine de paramètres pour les urls 
?>
<div id="galerie_<?php echo $unique;?>">
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Aperçu</th>
				<th>Type</th>
				<th>Nom</th>
				<th>Description</th>
				<th>Lien</th>
				<th><?php echo $this->Html->link('<i class="fa fa-plus-square"></i>', '#', array('class' => 'btn btn-success btn-xs btn-upload', 'escape' => false));?></th>
			</tr>
		</thead>
		<tbody class="sortable" ></tbody>
	</table>
</div>
<?php 
$this->Html->script('/file_manager/js/admin.WhUpload.js', array('block' => 'scriptBottom'));
$this->Html->scriptStart(array('inline' => false));
?>

$('#galerie_<?php echo $unique;?>').WhUpload({
	btn_upload 		: $('#galerie_<?php echo $unique;?> .btn-upload'), 
	params_url		: '<?php echo $param_url;?>', 
	container		: $('#galerie_<?php echo $unique;?> tbody'), 
	container_url 	: '/admin/file_manager/files/list/<?php echo $param_url;?>', 
	type			: 'multiple', //unique / multiple 
	finish			: function () {
	    $('#galerie_<?php echo $unique;?> .sortable').each(function(index) {

	        var el = $(this);

	        el.sortable({ 
	            placeholder : 'highlight',
	            axis        : 'y', 
	            stop        : function(event, ui) {

	                el.showLoading();

	                $.ajax({
	                    type      : 'POST',
	                    url       : '/admin/file_manager/files/ordre',
	                    data      : el.sortable('serialize'),
	                    cache     : false, 
	                    complete  : function () {el.hideLoading()}, 
	                    success   : function (reponse) {

	                        el.hideLoading();

	                    }

	                });  

	            }     

	        });     

	        $(this).disableSelection(); // on désactive la possibilité au navigateur de faire des sélections

	    });
	}
});


<?php
$this->Html->scriptEnd();
?>