<?php 
$param_url = array(); foreach($this->request->params['named'] as $k => $v) $param_url[] = $k.':'.$v; $param_url = implode('/', $param_url); //Traformation du tableau en chaine de paramètres pour les urls 


?>

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>Admin</th>
			<th></th>
			<th>Nom</th>
			<th>Type</th>
			<th>Dossier</th>

		</tr>
	</thead>
	<tbody>
		<?php foreach($Files as $v) : ;?>
		<tr>
			<td style="width:150px;">
			<?php 
			echo $this->Html->link('<i class="fa fa-download"></i>', 'javascript:void(0);', array('escape' => false, 'data-id' => $v['File']['id'], 'class' => 'btn btn-success btn-xs add')).' ';
			echo $this->Html->link('<i class="fa fa-trash-o"></i>', '/admin/file_manager/files/delete/'.$v['File']['id'], array('escape' => false, 'class' => 'btn btn-danger btn-xs'), 'Etes vous sûre de vouloir supprimer ce fichier ? ').' ';
			?>
			</td>
			<td><?php echo $this->WhFile->show($v['File'], array('x' => 80, 'y' => 80));?></td>
			<td><?php echo $v['File']['name'];?></td>
			<td><?php echo $v['Extention']['type'];?></td>
			<td>Dossier</td>
		</tr>
		<?php endforeach;?>
	</tbody>
</table>
<div class="table-footer">
    <div  class="dataTables_info"><?php echo $this->Paginator->counter('Page <strong>{:page}</strong> sur <strong>{:pages}</strong>. Résultat <strong>{:start}</strong> sur <strong>{:end}</strong> (total <strong>{:count}</strong>)'); ;?></div>
    <div class="dataTables_paginate paging_full_numbers"><?php echo $this->Paginator->numbers(array('separator' => '', 'class' => 'paginate_button', 'currentClass' => 'paginate_active', 'first' => 1, 'last' => 1 )); ?></div>   
</div>
<?php 
$this->Html->scriptStart(array('inline' => false));
?> 
var el = false;

<?php if(!empty($this->request->params['named']['el'])) echo 'el = "#'.$this->request->params['named']['el'].'"'; ?>

$('.add').click(function() {
	
	var btn = $(this);	

	//window.location = '/admin/file_manager/files/associe/' + $(btn).data('id') + '/<?php echo $param_url;?>';

	$.ajax({
		'url' 		: '/admin/file_manager/files/associe/' + $(btn).data('id') + '/<?php echo $param_url;?>', 
	}).done(function( msg ) {
    	
    	if(el) {

    		window.parent.$(el).WhUploadFinished();

    	}

  	});

});

<?php
$this->Html->scriptEnd();
?>


