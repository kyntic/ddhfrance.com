<?php 
$param_url = array(); foreach($this->request->params['named'] as $k => $v) $param_url[] = $k.':'.$v; $param_url = implode('/', $param_url); //Traformation du tableau en chaine de paramètres pour les urls 
echo $this->Form->create('File', array('url' => '/admin/file_manager/files/upload/'.$param_url, 'type' => 'file', 'id' => 'fileupload', 'class' => 'form-horizontal'));
 ?>
<!-- The fileinput-button span is used to style the file input field as button -->
<span class="btn btn-success fileinput-button">
    <i class="fa fa-square-plus"></i>
    <span>Séléctionnez vos fichiers</span>
    <!-- The file input field used as target for the file upload widget -->
    <input id="fileupload" type="file" name="files[]" multiple>
</span>
<br>
<br>
<!-- The global progress bar -->
<div id="progress" class="progress">
    <div class="progress-bar progress-bar-success"></div>
</div>
<!-- The container for the uploaded files -->
<div id="files" class="files"></div>
<?php echo $this->Form->end(); ?>

<?php
$this->Html->css('/file_manager/css/jquery.fileupload.css', null, array('inline' => false));
$this->Html->script('/file_manager/js/vendor/jquery.ui.widget.js', array('block' => 'scriptBottom'));
$this->Html->script('/file_manager/js/jquery.fileupload.js', array('block' => 'scriptBottom'));
$this->Html->script('/file_manager/js/jquery.iframe-transport.js', array('block' => 'scriptBottom'));
//<!--[if gte IE 8]><script src="js/cors/jquery.xdr-transport.js"></script><![endif]-->
$this->Html->scriptStart(array('inline' => false));
?> 
var el = false;

<?php if(!empty($this->request->params['named']['el'])) echo 'el = "#'.$this->request->params['named']['el'].'"'; ?>

$(function () {
    'use strict';
    $('#fileupload').fileupload({
        url: $('#fileupload').attr('action'),
        dataType: 'json',
        <?php if(!empty($this->request->params['named']['nbr_max'])) echo 'singleFileUploads : '.$this->request->params['named']['nbr_max'].',';?> 
        done: function (e, data) {
            
            $.each(data.result.files, function (index, file) {

                $('<p/>').text(file.name).appendTo('#files');
            });
        },
        progressall: function (e, data) {

            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .progress-bar').css(
                'width',
                progress + '%'
            );
        }, 
        stop : function () {

            if(el) {

                window.parent.$(el).WhUploadFinished();

            }else{

                window.parent.location.reload();

            }

        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});

<?php
$this->Html->scriptEnd();
?>


