<?php if(!empty($videos)) :?>

<section class="video">
    <h1 class="section-heading text-highlight"><span class="line">Video Tour</span></h1>
    <div class="carousel-controls">
        <a class="prev" href="#videos-carousel" data-slide="prev"><i class="fa fa-caret-left"></i></a>
        <a class="next" href="#videos-carousel" data-slide="next"><i class="fa fa-caret-right"></i></a>
    </div>
    <div class="section-content">

        <div id="videos-carousel" class="videos-carousel carousel slide">
            <div class="carousel-inner">
                <?php foreach($videos as $k => $v) :?>

                        <div class="item <?php if($k == 0) echo 'active';?>">

                            <?=$this->Html->image($v['Actualite']['vignette']->File->url, array('url' => $v['Actualite']['url']));?>

                            <p class="description"><?=$v['Actualite']['resume'];?></p>
                        </div>
                <?php endforeach;?>
            </div>
        </div>
    </div>
</section>
<?php endif;?>
