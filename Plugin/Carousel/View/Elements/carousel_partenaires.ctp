<?php if(!empty($partenaires)) : ?>
    <section id="partenaires" class="awards">
        <div id="partenaires-carousel" class="awards-carousel carousel slide">
            <div class="carousel-inner">
                <?php foreach($partenaires as $k => $v):
                    $active = '';
                    if($k == 0) $active = ' active';
                    if($k % 6 == 0) echo '<div class="item '.$active.'"><ul class="logos">';?>
                    <li class="col-md-2 col-sm-2 col-xs-4">
                        <?php echo $this->Html->link($this->Html->image($v['Partenaire']['vignette']->File->url, array('class' => 'img-responsive')), $v['Partenaire']['url_r'], array('escape' => false,'target' => '_blank')); ?>
                    </li>
                    <?php if($k %6 == 5) echo '</ul></div>'; ?>
                <?php endforeach; ?>
                <?php if($k %6 != 5) echo '</ul></div>';?>

            </div><!--//carousel-inner-->
            <a class="left carousel-control" href="#partenaires-carousel" data-slide="prev">
                <i class="fa fa-angle-left"></i>
            </a>
            <a class="right carousel-control" href="#partenaires-carousel" data-slide="next">
                <i class="fa fa-angle-right"></i>
            </a><!--
    --></div>
    </section>

<?php endif;?>
