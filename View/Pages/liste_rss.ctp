    <?php
    $this->extend('/Templates/template_default');

    $this->element('balises_metas', array('metas' => $Cont['Page']));

    foreach($_breadcrumb['Chemin'] as $v) $this->Html->addCrumb($v[$_breadcrumb['Model']]['name'], $v[$_breadcrumb['Model']]['url']);

    $this->assign('h1', $Cont['Page']['h1']);

    /**
     * Start the body content with modules
     */
    $this->start('body');

    if(!empty($ContentModules)) {

        foreach($ContentModules as $module) {

            switch($module['ContentModule']['module']) {

                default :

                    echo $this->element('ModuleManager.'.$module['ContentModule']['module'], array('content' => $module['ContentModule']));

                    break;
            }


        }
    } else {

        echo $Cont['Page']['body'];
    }


    /**
     * Liste des abonnements
     */
    foreach($rsss as $v) : ;
    ?>

        <div class="actualite">
            <div>
                <a href="/pages/rss/<?=strtolower(Inflector::slug($v['Page']['rss_name'])).'/'.$v['Page']['id'];?>.rss">
                    <h3><i class="fa fa-rss"></i> <?=$v['Page']['rss_name']; ?></h3>
                    <p class="resume"><?php echo $v['Page']['rss_description']; ?></p>
                    <div class="plus">S'abonner +</div>
                </a>
            </div>
        </div>
        <hr/>

    <?php
    endforeach;
    $this->end();

    if(!empty($actualites)) $this->assign('actu_col', $this->element('Blog.liste_actualite_col', array('actualites' => $actualites)));

    if(!empty($evenements)) $this->assign('event_col', $this->element('Blog.liste_evenement_col', array('evenements' => $evenements)));

    if(!empty($Cont['Page']['rss_name'])) {
        $this->start('rss_col');
    ?>
        <section class="widget has-divider">
            <h3 class="title"><i class="fa fa-rss"></i> <?=$Cont['Page']['rss_name'];?></h3>
            <p><?php echo $Cont['Page']['rss_description'];?></p>
            <a href="/pages/rss/<?=strtolower(Inflector::slug($Cont['Page']['rss_name'])).'/'.$Cont['Page']['id'].'.rss';?>" target="_blank">Cliquez ici</a>
        </section>

    <?php
        $this->end();
    }


    //Petit resume sur la colone de droite
    if(!empty($Cont['Page']['resume']))
    {
        $this->start('resume_col');
        ?>
        <section class="widget has-divider">
            <h3 class="title"><?php echo $Cont['Page']['name'];?></h3>
            <p><?php echo $Cont['Page']['resume'];?></p>

            <?php
            //Si des elements du portfolio existe , on les rend telechargeable
            if (!empty($portFolio)) {
                foreach($portFolio as $v):
                    if($v['File']['type'] == 'file'){
                        echo $this->Html->link(
                            '<i class="fa fa-download"> Télécharger la brochure</i>',
                            '/files/' . $v['File']['url'],
                            array('class' => 'btn btn-theme', 'escape' => false, 'target' => '_blank')
                        );

                    }
                endforeach;
            }
            ?>
        </section>
        <?php
        $this->end();
    }

    //Affichage petite infos date auteur
    $this->start('date_maj');
    ?>
    <p class="meta text-muted">
        <?php if(!empty($Cont['Page']['auteur'])) ?>Par: <a href="#"><?php echo $Cont['User']['nom'].' '.$Cont['User']['prenom'];?></a>
        | Posté le: <?php echo $Cont['Page']['created'];?>
    </p>
    <?php
    $this->end();

