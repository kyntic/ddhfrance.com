<?php
$this->extend('/Templates/template_default');

$this->element('balises_metas', array('metas' => $Cont['Page']));

foreach($_breadcrumb['Chemin'] as $v) $this->Html->addCrumb($v[$_breadcrumb['Model']]['name'], $v[$_breadcrumb['Model']]['url']);

$this->assign('h1', $Cont['Page']['h1']);

/**
 * Start the body content with modules
 */
$this->start('body');


foreach($ContentModules as $module) {

    switch($module['ContentModule']['module']) {

        default :

	        $element = 'ModuleManager.' . $module['ContentModule']['module'];
	        if (!empty($module['ContentModule']['template'])) {
		        $element .= $module['ContentModule']['template'];
	        }
            echo $this->element($element, array('content' => $module['ContentModule']));

            break;
    }


}
?>
<section class="events-wrapper">
    <?php foreach($Evens as $v) : ;?>
        <article class="events-item page-row has-divider clearfix">
            <div class="date-label-wrapper col-md-1 col-sm-2">
                <p class="date-label">
                    <span class="month"><?=$this->WhDate->show($v['Even']['even_date_deb'],'mois_court');?></span>
                    <span class="date-number"><?=$this->WhDate->show($v['Even']['even_date_deb'],'jour');?></span>
                </p>
            </div><!--//date-label-wrapper-->
            <div class="details col-md-11 col-sm-10">
                <h3 class="title"><a href="<?=$v['Even']['url'];?>"><?=$v['Even']['name'];?></a></h3>
                <p class="meta">
                    <span class="time"><i class="fa fa-clock-o"></i>
                        <?=$this->WhDate->show($v['Even']['even_date_deb'],'heure');?> -
                        <?=$this->WhDate->show($v['Even']['even_date_fin'],'heure');?>
                    </span>
                    <span class="location"><i class="fa fa-map-marker"></i><a href="<?=$v['Even']['url'];?>"><?=$v['Even']['adresse'];?></a></span>
                </p>
                <p class="desc"><?=$this->Text->truncate($v['Even']['resume'], 120, array('ellipsis' => '...'))?></p>
            </div><!--//details-->
        </article>
    <?php endforeach;?>
</section>
<?php
$this->end();

if(!empty($actualites)) $this->assign('actu_col', $this->element('Blog.liste_actualite_col', array('actualites' => $actualites)));


//Petit resume sur la colone de droite
if(!empty($Cont['Page']['resume']))
{
    $this->start('resume_col');
    ?>
    <section class="widget has-divider">
        <h3 class="title"><?php echo $Cont['Page']['name'];?></h3>
        <p><?php echo $Cont['Page']['resume'];?></p>

        <?php
        //Si des elements du portfolio existe , on les rend telechargeable
        if (!empty($portFolio)) {
            foreach($portFolio as $v):
                if($v['File']['type'] == 'file'){
                    echo $this->Html->link(
                        '<i class="fa fa-download"> Télécharger la brochure</i>',
                        '/files/' . $v['File']['url'],
                        array('class' => 'btn btn-theme', 'escape' => false, 'target' => '_blank')
                    );

                }
            endforeach;
        }
        ?>
    </section>
    <?php
    $this->end();
}
