        <!-- Counter Section -->
        <section id="counter" class="overlay-dark80 light-color ptb-80" style="background-image: url('/mazel/img/full/23.jpg');" data-stellar-background-ratio="0.5">
            <div class="container text-center">
                <div class="row">
                    <div class="col-md-3 mb-sm-30">
                        <h1 class="counter" data-count="59">0</h1>
                        <h6>awards</h6>
                    </div>
                    <div class="col-md-3 mb-sm-30">
                        <h1 class="counter" data-count="1054">0</h1>
                        <h6>Clients</h6>
                    </div>
                    <div class="col-md-3 mb-sm-30">
                        <h1 class="counter" data-count="34">0</h1>
                        <h6>Team</h6>
                    </div>
                    <div class="col-md-3 mb-sm-30">
                        <h1 class="counter" data-count="154">0</h1>
                        <h6>Project</h6>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Counter Section -->