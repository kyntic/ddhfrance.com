


        <!-- Work Detail Section -->
        <section class="ptb ptb-sm-80">
            <div class="container ">
                <div class="masonry nf-col-3">

                    <div class="nf-item">
                        <div class="item-box">
                                <img alt="1" src="/mazel/img/portfolio/1.jpg" class="item-container">
                                <div class="item-mask">
                                    <div class="item-caption">
                                        <h5 class="white">Consequat massa quis</h5>
                                        <p class="white">Branding, Design</p>
                                    </div>
                                </div>
                        </div>
                    </div>

                    <div class="nf-item">
                        <div class="item-box">
                                <img alt="1" src="/mazel/img/portfolio/2.jpg" class="item-container">
                                <div class="item-mask">
                                    <div class="item-caption">
                                        <h5 class="white">Bookmarksgrove right at</h5>
                                        <p class="white">Branding, Design, Coffee</p>
                                    </div>
                                </div>
                        </div>
                    </div>

                    <div class="nf-item">
                        <div class="item-box">
                                <img alt="1" src="/mazel/img/portfolio/3.jpg" class="item-container">
                                <div class="item-mask">
                                    <div class="item-caption">
                                        <h5 class="white">A small river named</h5>
                                        <p class="white">Branding, Design</p>
                                    </div>
                                </div>
                        </div>
                    </div>



                    <div class="nf-item">
                        <div class="item-box">
                                <img alt="1" src="/mazel/img/portfolio/5.jpg" class="item-container">
                                <div class="item-mask">
                                    <div class="item-caption">
                                        <h5 class="white">Rethoric question ran over</h5>
                                        <p class="white">Branding, Design, Coffee</p>
                                    </div>
                                </div>
                        </div>
                    </div>

                    <div class="nf-item w3-1">
                        <div class="item-box">
                                <img alt="1" src="/mazel/img/portfolio/4.jpg" class="item-container">
                                <div class="item-mask">
                                    <div class="item-caption">
                                        <h5 class="white">Because there were thousands</h5>
                                        <p class="white">Branding, Design, Coffee</p>
                                    </div>
                                </div>
                        </div>
                    </div>

                    <div class="nf-item">
                        <div class="item-box">
                                <img alt="1" src="/mazel/img/portfolio/28.jpg" class="item-container">
                                <div class="item-mask">
                                    <div class="item-caption">
                                        <h5 class="white">Ran over her cheek</h5>
                                        <p class="white">Branding, Slider</p>
                                    </div>
                                </div>
                        </div>
                    </div>

                    <div class="nf-item">
                        <div class="item-box">
                                <img alt="1" src="/mazel/img/portfolio/6.jpg" class="item-container">
                                <div class="item-mask">
                                    <div class="item-caption">
                                        <h5 class="white">Oxmox advised her</h5>
                                        <p class="white">Branding, Video</p>
                                    </div>
                                </div>
                        </div>
                    </div>

                    <div class="nf-item">
                        <div class="item-box">
                                <img alt="1" src="/mazel/img/portfolio/7.jpg" class="item-container">
                                <div class="item-mask">
                                    <div class="item-caption">
                                        <h5 class="white">Subline of her own road</h5>
                                        <p class="white">Branding, Youtube</p>
                                    </div>
                                </div>
                        </div>
                    </div>

                    <div class="nf-item w2">
                        <div class="item-box">
                                <img alt="1" src="/mazel/img/portfolio/7.jpg" class="item-container">
                                <div class="item-mask">
                                    <div class="item-caption">
                                        <h5 class="white">But the Little Blind</h5>
                                        <p class="white">Branding, Vimeo</p>
                                    </div>
                                </div>
                        </div>
                    </div>

                    <div class="nf-item">
                        <div class="item-box">
                                <img alt="1" src="/mazel/img/portfolio/27.jpg" class="item-container">
                                <div class="item-mask">
                                    <div class="item-caption">
                                        <h5 class="white">Even the all-powerful</h5>
                                        <p class="white">Branding, Design</p>
                                    </div>
                                </div>
                        </div>
                    </div>

                    <div class="nf-item w1">
                        <div class="item-box">
                                <img alt="1" src="/mazel/img/portfolio/23.jpg" class="item-container">

                                <div class="item-mask">
                                    <div class="item-caption">
                                        <h5 class="white">Italic Mountains</h5>
                                        <p class="white">Branding, Coffee</p>
                                    </div>
                                </div>
                        </div>
                    </div>

                    <div class="nf-item">
                        <div class="item-box">
                                <img alt="1" src="/mazel/img/portfolio/8.jpg" class="item-container">
                                <div class="item-mask">
                                    <div class="item-caption">
                                        <h5 class="white">control about the blind</h5>
                                        <p class="white">Branding, Design</p>
                                    </div>
                                </div>
                        </div>
                    </div>

                    <div class="nf-item">
                        <div class="item-box">
                                <img alt="1" src="/mazel/img/portfolio/9.jpg" class="item-container">
                                <div class="item-mask">
                                    <div class="item-caption">
                                        <h5 class="white">The Big Oxmox advised</h5>
                                        <p class="white">Branding, Slider</p>
                                    </div>
                                </div>
                        </div>
                    </div>

                    <div class="nf-item">
                        <div class="item-box">
                                <img alt="1" src="/mazel/img/portfolio/10.jpg" class="item-container">
                                <div class="item-mask">
                                    <div class="item-caption">
                                        <h5 class="white">When she reached</h5>
                                        <p class="white">Branding, Video</p>
                                    </div>
                                </div>
                        </div>
                    </div>

                </div>


            </div>

        </section>
        <!-- End Work Detail Section -->


        <!-- End CONTENT ------------------------------------------------------------------------------>
