

    <!-- Hero Slider Section -->
    <div class="flexslider fullscreen-carousel hero-slider-1 parallax parallax-section1">
        <ul class="slides">
            <?php
            foreach($portfolioItems as $v) : ; 
            ?>
            <li>
                <img src="<?=$v['File']['url'];?>" alt="portfolio" />
                <div class="overlay-hero">
                    <div class="container caption-hero dark-color">
                        <div class="inner-caption">

                            <p class="legende"><?=$v['File']['name'];?></p>
                            <p class="lead"><?=$v['File']['description'];?></p>

                        </div>
                    </div>
                </div>
            </li>
            <?php
            endforeach;
            ?>
        </ul>
    </div>
    <!-- End Hero Slider Section -->
</section>
<div class="clearfix"></div>
