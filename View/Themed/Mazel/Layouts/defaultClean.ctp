<!DOCTYPE html>

<head>

    <?php
        echo $this->Html->charset();
        echo '<title>' . $this->fetch('meta_title') . '</title>';
        echo $this->Html->meta('description', $this->fetch('meta_description'));
        echo $this->Html->meta('keywords', $this->fetch('meta_keywords'));
        echo $this->Html->meta(array('name' => 'viewport', 'content' => 'width=device-width, initial-scale=1'));
        echo $this->Html->meta(array('name' => 'author', 'content' => 'Whatson Web'));
        echo $this->Html->meta(array('name' => 'robots', 'content' => $this->fetch('meta_robots')));
    ?>

</head>

<body>

    <?php
        //Google tag manager
        $ga = Configure::read('Params.google_tag_manager');
        echo $ga;
    ?>

    <?= $this->fetch('content'); ?>

</body>

</html>