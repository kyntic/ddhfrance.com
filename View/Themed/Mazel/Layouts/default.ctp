<!DOCTYPE html>
<html>
<head>

    <?php
        echo $this->Html->charset();
        echo '<title>' . $this->fetch('meta_title') . '</title>';
        echo $this->Html->meta('description', $this->fetch('meta_description'));
        echo $this->Html->meta('keywords', $this->fetch('meta_keywords'));
        echo $this->Html->meta(array('name' => 'viewport', 'content' => 'width=device-width, initial-scale=1'));
        echo $this->Html->meta(array('name' => 'author', 'content' => 'Whatson Web'));
        echo $this->Html->meta(array('name' => 'robots', 'content' => $this->fetch('meta_robots')));
    ?>


    <!-- Favicone Icon -->

    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
    <link rel="icon" type="image/png" href="img/favicon.png">
    <link href='https://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
    <link rel="apple-touch-icon" href="img/favicon.png">
    <link href='https://fonts.googleapis.com/css?family=Damion' rel='stylesheet' type='text/css'>

    <!-- CSS TEMPLATE -->
    <?= $this->Html->css('/mazel/css/style.css'); ?>
    <?= $this->Html->css('/mazel/css/bootstrap.css'); ?>
    <?= $this->Html->css('/mazel/css/font-awesome.css'); ?>
    <?= $this->Html->css('/mazel/css/ionicons.css'); ?>
    <?= $this->Html->css('/mazel/css/plugin/jPushMenu.css'); ?>
    <?= $this->Html->css('/mazel/css/plugin/animate.css'); ?>
    <?= $this->Html->css('/mazel/css/jquery-ui.css'); ?>
    <?= $this->Html->css('/mazel/css/prettyPhoto.css'); ?>


    <!-- CSS PERSO -->


</head>

<body class="full-intro">
<?php
        //Google tag manager
        $ga = Configure::read('Params.google_tag_manager');
        echo $ga;
    ?>

    <!-- Preloader -->
    <section id="preloader">
        <div class="loader" id="loader">
            <div class="loader-img"></div>
        </div>
    </section>
    <!-- End Preloader -->

    <!-- Sidemenu -->
    <section class="side-menu cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right">
        <a class="menu-close toggle-menu menu-right push-body"><i class="ion ion-android-close"></i></a>
       
        <ul>
             <?php
                                            if(!empty($_menuPages)) {
                                                $this->WhTree->html = '';
                                                $this->WhTree->Model = 'Page';
                                                $this->WhTree->niveau = 0;
                                                echo $this->WhTree->generate_nav_simple($_menuPages);
                                            }
                                            ?>
        </ul>
    </section>
    <!--End Sidemenu -->

    <!-- Search menu Top -->
    <section class=" top-search-bar cbp-spmenu cbp-spmenu-horizontal cbp-spmenu-top">
        <div class="container">
            <div class="search-wraper">
                <input type="text" class="input-sm form-full" placeholder="Search..." name="search" />
                <a class="search-bar-icon"><i class="fa fa-search"></i></a>
                <a class="bar-close toggle-menu menu-top push-body"><i class="ion ion-android-close"></i></a>
            </div>
        </div>
    </section>
    <!--End Search menu Top -->

    <!-- Site Wraper -->
    <div class="wrapper">

        <!-- HEADER -->
        <header class="header">
            <div class="container">

                <!-- logo -->
                <div class=" logo">
                    <?=$this->Html->link('<img src="'.WH_ROOT.'/img/logo.png" alt="" />',
                                        FULL_BASE_URL,
                                        array('escape' => false, 'id' => 'logoHeader')
                                    );?>
                </div>
                <!--End logo-->

                <!-- Rightside Menu (Search, Cart, Bart icon) -->
                <div class="side-menu-btn">
                    <ul>
                       
                      

                        <!--Sidebar Menu Icon-->
                        <li class="">
                            <a class="right-icon bar-icon toggle-menu menu-right push-body"><i class="fa fa-bars"></i></a>
                        </li>
                        <!-- End Sidebar Menu Icon-->
                    </ul>
                </div>
                <!-- End Rightside Menu -->

                <!-- Navigation Menu -->
                <nav class='navigation'>

                                            <?php
                                            if(!empty($_menuPages)) {
                                                $this->WhTree->html = '';
                                                $this->WhTree->Model = 'Page';
                                                $this->WhTree->niveau = 0;
                                                echo $this->WhTree->generate_nav_simple($_menuPages);
                                            }
                                            ?>
                </nav>
                <!--End Navigation Menu -->

            </div>
        </header>
        <!-- END HEADER -->

  <?= $this->fetch('content'); ?>

        <!-- FOOTER -->
        <footer class="footer">
            <div class="container">
          
                <!--Footer Info -->
                <div class="row">
                   
                    <div class="col-md-4 col-sm-4 col-xs-12 mb-sm-30 mb-xs-0">
                          <ul class="margin0 no-style footer-nav padding0">
                            <?php
                            foreach($_menuPages as $v) : ;
                                echo '<li>'.$this->Html->link($v['Page']['name'], $v['Page']['url']).'</li>';
                            endforeach;
                            ?>
                        </ul>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12 mb-sm-30">
                         <!-- <div class="about-footer-txt padding-top12"><? /*=nl2br(Configure::read('Params.about'));*/ ?></div> -->
                        
                         <div><a href="/contact/p-3"><img src="/mazel/img/contact-bas.png"></a></div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <p class="adresse"><?=Configure::read('Params.adresse_1');?> <?=Configure::read('Params.cp');?>  <?=Configure::read('Params.ville');?></p>
                            <p class="adresse">Tel.: <?=Configure::read('Params.telephone');?></p>
                            <p class="lien"><?php echo $this->Html->link('Nous contacter', $_lienContact['Page']['url']);?></p>
                            <p class="lien"><?php echo $this->Html->link('Mentions légales', $_lienMentionsLegales['Page']['url'], array('class' => 'mention_legale')); ?></p>
                            <p class="lien">Développement : <?php echo $this->Html->link('Kobaltis','http://www.kobaltis.com', array('class' => 'credit', 'target' => '_blank', 'title' => 'Web agency', 'style' => 'display:inline-block;vertical-align:top;')); ?></p>
                    </div>
                    
                </div>
<div class="row">
                    <div class="col-xs-12">
<p class="phrase-footer">DDH France - Votre protection Environnementale dans le Nord - Nous vous proposons tous les services de dératisation, désinfection, désinsectisation. Nous assurons aussi les entretiens des terrasses, des vide-ordures, des VMC. Le traitement des bois et des champignons fait aussi partie de
 notre activité.</p>
					</div>


</div>
                <!-- End Footer Info -->
            </div>


         

        </footer>
        <!-- END FOOTER -->

        <!-- Scroll Top -->
        <a class="scroll-top">
            <i class="fa fa-angle-double-up"></i>
        </a>
        <!-- End Scroll Top -->

    </div>
    <!-- Site Wraper End -->



   <!-- JS -->
    <?= $this->Html->script('/mazel/js/jquery-1.11.2.min.js'); ?>
            <?= $this->Html->script('/mazel/js/plugin/imagesloaded.pkgd.min.js'); ?> 

    <?= $this->Html->script('/mazel/js/plugin/jquery.easing.js'); ?>
    <?= $this->Html->script('/mazel/js/jquery-ui.min.js'); ?>
    <?= $this->Html->script('/mazel/js/bootstrap.min.js'); ?>
    <?= $this->Html->script('/mazel/js/plugin/jquery.flexslider.js'); ?>
    <?= $this->Html->script('/mazel/js/plugin/background-check.min.js'); ?>
    <?= $this->Html->script('/mazel/js/plugin/jquery.fitvids.js'); ?>
    <?= $this->Html->script('/mazel/js/plugin/jquery.viewportchecker.js'); ?>
    <?= $this->Html->script('/mazel/js/plugin/jquery.stellar.min.js'); ?>
    <?= $this->Html->script('/mazel/js/plugin/wow.min.js'); ?>
    <?= $this->Html->script('/mazel/js/plugin/jquery.colorbox-min.js'); ?>
    <?= $this->Html->script('/mazel/js/plugin/owl.carousel.min.js'); ?>
    <?= $this->Html->script('/mazel/js/plugin/isotope.pkgd.min.js'); ?>
    <?= $this->Html->script('/mazel/js/plugin/masonry.pkgd.min.js'); ?>
    <?= $this->Html->script('/mazel/js/plugin/jPushMenu.js'); ?>
    <?= $this->Html->script('/mazel/js/plugin/jquery.fs.tipper.min.js'); ?>
    <?= $this->Html->script('/mazel/js/plugin/mediaelement-and-player.min.js'); ?>
    <?= $this->Html->script('/mazel/js/theme.js'); ?>
    <?= $this->Html->script('/mazel/js/navigation.js'); ?>
       <?= $this->Html->script('/mazel/js/prettyPhoto.js'); ?>
    <?= $this->Html->script('/mazel/js/prettyPhoto-ini.js'); ?> 

    
    



</body>
</html>
