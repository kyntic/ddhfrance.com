<?php if(!empty($_menuPages)) {?>
<section class="widget has-divider">
    <h3 class="title">Menu</h3>
    <nav id="menuRight">
    <?php

        $this->WhTree->html = '';
        $this->WhTree->Model = 'Page';
        $this->WhTree->niveau = 0;
        echo $this->WhTree->generate_nav_right($_menuPages);
    ?>
    </nav>
</section>
<?php  }?>
