<?php 
$meta_title = (!empty($metas['meta_title'])) ? $metas['meta_title'] : $metas['h1'].' - '.Configure::read('Projet.nom');
$this->assign('meta_title', $meta_title);

$meta_description = (!empty($metas['meta_description'])) ? $metas['meta_description'] : '';
$this->assign('meta_description', $meta_description);

$meta_keywords = (!empty($metas['meta_keywords'])) ? $metas['meta_keywords'] : '';
$this->assign('meta_keywords', $meta_keywords);

$meta_robots = (!empty($metas['meta_robots'])) ? $metas['meta_robots'] : 'INDEX,FOLLOW';
$this->assign('meta_robots', $meta_robots);
?> 