<!DOCTYPE html>

<head>

    <?php
    echo $this->Html->charset();
    echo '<title>' . $this->fetch('meta_title') . '</title>';
    echo $this->Html->meta('description', $this->fetch('meta_description'));
    echo $this->Html->meta('keywords', $this->fetch('meta_keywords'));
    echo $this->Html->meta(array('name' => 'viewport', 'content' => 'width=device-width, initial-scale=1'));
    echo $this->Html->meta(array('name' => 'author', 'content' => 'Whatson Web'));
    echo $this->Html->meta(array('name' => 'robots', 'content' => $this->fetch('meta_robots')));

    ?>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <link rel="shortcut icon" href="<?=WH_ROOT;?>/img/favicon.png">

    <!-- ///////////LE TEMPLATE///////////  /themeforest/Site/ -->
    <?= $this->Html->css(
        'http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,200,200italic,300,300italic,400italic,700'
    ); ?>
    <?= $this->Html->css(
        'http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic&subset=latin,cyrillic-ext,greek-ext,greek,vietnamese,latin-ext,cyrillic'
    ); ?>

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>

    <!-- Bootstrap CSS and JS Files -->
    <link rel="stylesheet" type="text/css" href="<?=WH_ROOT;?>/themeforest/Site/css/bootstrap.min.css"/>
    <script type="text/javascript" src="<?=WH_ROOT;?>/themeforest/Site/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

    <!-- Theme CSS and JS Files files -->

    <link rel="stylesheet" type="text/css" href="<?=WH_ROOT;?>/themeforest/Site/css/margins-paddings.css"/>
    <link rel="stylesheet" href="<?=WH_ROOT;?>/themeforest/Site/css/idangerous.swiper.css">
    <link rel="stylesheet" type="text/css" href="<?=WH_ROOT;?>/themeforest/Site/css/prettyPhoto.css"/>

    <?= $this->fetch('usquare'); ?>
    <?= $this->fetch('rerefence'); ?>

    <link rel="stylesheet" type="text/css" href="<?=WH_ROOT;?>/themeforest/Site/style.css"/>
    <link rel="stylesheet" type="text/css" href="<?=WH_ROOT;?>/themeforest/Site/css/responsive.css"/>

    <?= $this->Html->css('/css/layout.css'); ?>
    <?= $this->fetch('css'); ?>

    <script type="text/javascript" src="<?=WH_ROOT;?>/themeforest/Site/js/prettyPhoto.js"></script>
    <script type="text/javascript" src="<?=WH_ROOT;?>/themeforest/Site/js/prettyPhoto-ini.js"></script>
    <script type="text/javascript" src="<?=WH_ROOT;?>/themeforest/Site/js/ie.html5.js"></script>
    <script type="text/javascript" src="<?=WH_ROOT;?>/themeforest/Site/js/jquery-color.js"></script>
    <script type="text/javascript" src="<?=WH_ROOT;?>/themeforest/Site/js/header.js"></script>
    <script type="text/javascript" src="<?=WH_ROOT;?>/themeforest/Site/js/common.js"></script>
    <script type="text/javascript" src="<?=WH_ROOT;?>/themeforest/Site/js/jquery.mousewheel.js"></script>
    <script type="text/javascript" src="<?=WH_ROOT;?>/themeforest/Site/js/jquerry.easing.1.3.js"></script>
    <script type="text/javascript" src="<?=WH_ROOT;?>/themeforest/Site/js/swipe-screen.js"></script>
    <script src="<?=WH_ROOT;?>/themeforest/Site/js/idangerous.swiper-2.1.min.js"></script>
    <script src="<?=WH_ROOT;?>/themeforest/Site/js/idangerous.swiper.scrollbar-2.1.js"></script>

    <link rel="stylesheet" type="text/css" href="<?=WH_ROOT;?>/themeforest/Site/css/style.css"/>

</head>

<body class="relative">


<div class="peter-river">

    <?=$this->fetch('content');?>

    <div style="position:absolute;top:0px;left:0px;width:100%;">
        <header class="relative">
            <div class="header_nav no-border-bott relative">
                <!-- MENU -->
                <div class="white-bg header-box-shadow">
                    <div class="container header_height">
                        <div class="row text-center relative">
                            <div class="header-nav-wrap relative text-right">
                                <div class="zero-logo pull-left">
                                    <?=$this->Html->link('<img src="'.WH_ROOT.'/img/logo.png" alt="" />',
                                        WH_ROOT,
                                        array('escape' => false, 'id' => 'logoHeader')
                                    );?>
                                </div>
                                <nav class="inline-block text-right">
                                    <div class="navbar margin0 relative">
                                        <div class="navbar-inner">

                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </nav>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- header_nav -->
        </header>
    </div>

    <footer>
        <div class="footer-bg">
            <div class="container padding-bottom12">

                <div class="clearfix"></div>
            </div>
            <div class="container padding-bottom24">
                <!-- ABOUT -->
                <div class="clearfix"></div>
            </div>
        </div>
    </footer>


    <div class="modal hide fade" id="popup_ajx">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="popupLabelTitre"></h3>
        </div>
        <div class="modal-body"></div>
    </div>

</div>
<?php
$this->Html->scriptStart(array('inline' => false, 'block' => 'scriptBottom'));
?>

<?php
$this->Html->scriptEnd();
echo $this->Html->script('loading/jquery.showLoading.min.js');
echo $this->fetch('scriptBottom');
echo $this->Js->writeBuffer(); // Le code jascript en cache

echo $this->fetch('scriptSuperBottom');
?>
</body>

</html>