<!DOCTYPE html>
<html lang="en" data-ng-app="myAppAdmin">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="description" content="{{app.description}}">
        <meta name="keywords" content="app, responsive, angular, bootstrap, dashboard, admin">
        <title data-ng-bind="pageTitle()">Angle - Angular Bootstrap Admin Template</title>
        <link rel="stylesheet" href="<?=WH_ROOT;?>/app_admin/assets/css/app.css">
        <link rel="stylesheet" href="<?=WH_ROOT;?>/app_admin/assets/css/style.css">
        <link id="autoloaded-stylesheet" rel="stylesheet" href="<?=WH_ROOT;?>/app_admin/assets/css/theme-e.css">

        <link rel="stylesheet" href="<?=WH_ROOT;?>/vendor/ui-select-master/dist/select.min.css">
        <link rel="stylesheet" href="<?=WH_ROOT;?>/vendor/select2.css">


        <script>
            var WH_ROOT = '<?=WH_ROOT;?>';
        </script>

    </head>

    <body data-ng-class="{ 'layout-fixed' : app.layout.isFixed, 'aside-collapsed' : app.layout.isCollapsed, 'layout-boxed' : app.layout.isBoxed }">

        <div data-ui-view="" data-autoscroll="false" data-ng-class="mainViewAnimation" class="wrapper">

            chargement... Veuillez patienter...

        </div>
        <script>
            var params = {

                Url: {
                    menu: '<?= FULL_BASE_URL; ?>/admin/admin_menus/getMenu'
                },
                Multilingue : <?=json_encode(Configure::read('multilingue'));?>,
                Locales : '<?=json_encode(Configure::read('Config.languages'));?>'

            }
        </script>

        <script src="<?=WH_ROOT;?>/app_admin/base.js"></script>

        <script src="<?=WH_ROOT;?>/vendor/angular/angular-locale_fr-fr.js"></script>

        <script src="<?=WH_ROOT;?>/vendor/angular-checklist/angular-checklist.js"></script>
        <script src="<?=WH_ROOT;?>/app_admin/auth-service.js"></script>
        <script src="<?=WH_ROOT;?>/vendor/angular-sortable/sortable.js"></script>
        <script src="<?=WH_ROOT;?>/vendor/angular-ui-tree-master/dist/angular-ui-tree.min.js"></script>
        <script src="<?=WH_ROOT;?>/vendor/ui-select-master/dist/select.min.js"></script>

        <script src="<?=WH_ROOT;?>/app_admin/app.js"></script>

        <!-- LOADING THE SERVICES -->
        <script src="<?=WH_ROOT;?>/app_admin/services/rightService.js"></script>
        <script src="<?=WH_ROOT;?>/app_admin/services/pageService.js"></script>
        <script src="<?=WH_ROOT;?>/app_admin/services/ldapGroupService.js"></script>
        <script src="<?=WH_ROOT;?>/app_admin/services/AttributeService.js"></script>
        <script src="<?=WH_ROOT;?>/app_admin/services/AttributeFamilyService.js"></script>
        <script src="<?=WH_ROOT;?>/app_admin/services/ProductService.js"></script>
        <script src="<?=WH_ROOT;?>/app_admin/services/TypepageService.js"></script>
        <!-- END LOADING THE SERVICES -->

        <script src="<?=WH_ROOT;?>/app_admin/js/wh-file-manager.js"></script>
        <script src="<?=WH_ROOT;?>/app_admin/js/wh-modules.js"></script>
        <script src="<?=WH_ROOT;?>/app_admin/js/wh-dashboard.js"></script>
        <script src="<?=WH_ROOT;?>/app_admin/js/wh-actualites.js"></script>
        <script src="<?=WH_ROOT;?>/app_admin/js/wh-messagerie.js"></script>
        <script src="<?=WH_ROOT;?>/app_admin/js/wh-parametres.js"></script>
        <script src="<?=WH_ROOT;?>/app_admin/js/wh-users.js"></script>
        <script src="<?=WH_ROOT;?>/app_admin/js/wh-groups.js"></script>
        <script src="<?=WH_ROOT;?>/app_admin/js/wh-agenda.js"></script>
        <script src="<?=WH_ROOT;?>/app_admin/js/wh-partenaires.js"></script>
        <script src="<?=WH_ROOT;?>/app_admin/js/wh-formulaires.js"></script>
        <script src="<?=WH_ROOT;?>/app_admin/js/wh-cours.js"></script>
        <script src="<?=WH_ROOT;?>/app_admin/js/wh-rights.js"></script>
        <script src="<?=WH_ROOT;?>/app_admin/js/pages/pageEditionController.js"></script>
        <script src="<?=WH_ROOT;?>/app_admin/js/pages/pageIndexController.js"></script>
        <script src="<?=WH_ROOT;?>/app_admin/js/catalogue/attributes_families/AttributeFamiliesIndexCtrl.js"></script>
        <script src="<?=WH_ROOT;?>/app_admin/js/catalogue/attributes_families/AttributeFamiliesEditCtrl.js"></script>
        <script src="<?=WH_ROOT;?>/app_admin/js/catalogue/products/ProductsIndexCtrl.js"></script>
        <script src="<?=WH_ROOT;?>/app_admin/js/catalogue/products/ProductsEditCtrl.js"></script>
        <script src="<?=WH_ROOT;?>/app_admin/js/typepages/TypepageEditCtrl.js"></script>
        <script src="<?=WH_ROOT;?>/app_admin/js/typepages/TypepageIndexCtrl.js"></script>

    </body>
</html>
