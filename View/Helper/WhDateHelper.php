<?php
App::uses('AppHelper', 'View/Helper');

class WhDateHelper extends AppHelper {

	function show ($datetime, $format = 'lettre') {


        $timestamp = explode(' ', $datetime);

        if(count($timestamp) == 1) {

            $date = explode('-', $timestamp[0]);

            $timestamp = mktime(0, 0, 0, $date[2], $date[1], $date[0]);

        }

        if(count($timestamp) == 2) {

            $date = explode('-', $timestamp[0]);
            $heure = explode(':', $timestamp[1]);


            $timestamp = mktime($heure[0], $heure[1], $heure[2], $date[1], $date[2], $date[0]);

        }



		$jour = array(
			'Dimanche', 
			'Lundi', 
			'Mardi', 
			'Mercredi', 
			'Jeudi', 
			'Vendredi', 
			'Samedi', 
			'Dimanche'
		);

		$mois_court = array(
            'dec',
            'janv',
			'fév', 
			'mars', 
			'avr', 
			'mai', 
			'juin', 
			'juil', 
			'août', 
			'sept', 
			'oct', 
			'nov', 
			'déc'
		);

		$mois_long = array(
			'Janvier', 
			'Février', 
			'Mars', 
			'Avril', 
			'Mai', 
			'Juin', 
			'Juillet', 
			'Août', 
			'Septembre', 
			'Octobre', 
			'Novembre', 
			'Décembre'
		);

		switch($format) {

			case 'date_admin' : 
				$date = date('d', $timestamp).'/'.date('m', $timestamp).'/'.date('Y', $timestamp).' '.date('H', $timestamp).':'.date('i', $timestamp);
			break;

			case 'date' : 
				$date = date('d', $timestamp).'/'.date('m', $timestamp).'/'.date('Y', $timestamp);
			break;

            case 'mois_court':

                $date = $mois_court[(int)date('m', $timestamp)];
                break;

            case 'jour':
                $date = date('d', $timestamp);

                break;

            case 'heure':
                $date = date('H', $timestamp).':'.date('i', $timestamp);
                break;

			default : 
				$date = date('d', $timestamp).'/'.date('m', $timestamp).'/'.date('Y', $timestamp);
			break;

		}

		return $date;


	}




}
?>