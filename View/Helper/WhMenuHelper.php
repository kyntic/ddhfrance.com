 <?php
/*
 Format du $menu : 
array(
    'menu' => array(
        'class' => 'class du menu', 
        'id'    => 'id', 
        'items' => array(
            'id'    => 'id', 
            'class' => 'class', 
            'url'   => 'url', 
            'text'  => 'texte'
            'memu'  => array(
                'items' => 
                    'url'   => 'url', 
                    'text'  => 'texte'
            )
        );
    )

);
 */

App::uses('AppHelper', 'View/Helper');

class WhMenuHelper extends AppHelper {
    
    var $helpers    = array('Html');
    

    public function creer ($menu = array()) {

        
        $out = $this->boucle($menu);

        return $out;

    }


    private function boucle ($menu) {

        $out = '<ul '.$this->return_param($menu).'>';

        if(isset($menu['items']) && is_array($menu['items'])) {

            foreach ($menu['items'] as $k => $v) {
                
                $v['class'] = (empty($v['class'])) ? '' : $v['class'];
                
                if($this->here == $v['a']['href']) $v['class'] .= ' active';
                
                $out .= '<li '.$this->return_param($v).'>'; 

                $v['a']['escape'] = false;

                $out .= $this->Html->link($v['a']['html'], $v['a']['href'], $this->return_param($v['a'], 'link')); 

                if(isset($v['menu']) && is_array($v['menu'])) {

                    $out .= $this->boucle($v['menu']);

                }

                $out .= '</li>'; 
            }

            $out .= '</ul>';

        }

       return $out;
        
    }

    private function return_param ($array = array(), $type = false) {

        $param      = array();
        $param_key  = array();

        foreach($array as $k => $v) {

            if(is_array($v)) continue;
            if($k == 'html') continue;

            if($type) {

                switch ($type) {

                    case 'link' : 
                        if($k == 'href') continue;
                    break;

                }

            }

            $param[] = $k.'="'.$v.'"';
            $param_key[$k] = $v;

        }

        return ($type) ? $param_key : implode(' ', $param);


    }


}
?> 