<?php
App::uses('HtmlHelper', 'View/Helper');

class MyHtmlHelper extends AppHelper {

	public $helpers = array('Html');

    function url($url = null, $full = false) {

    if(Configure::read('multilingue')) {

        // Analyse de l'URL
        if(!empty($url))
        {

            if(!is_array($url)) {

                $langFromUrl = preg_replace('#^/?([a-z]{2,3})/?(.*)$#', '$1', $url);

                $languages = Configure::read('Config.languages');

                //Si il n'y pas de langue d'appelée
                if($langFromUrl && !isset($languages[$langFromUrl]))
                {

                    $lang = Configure::read('Config.langCode');

                    $url = (preg_match('#^/#', $url)) ? '/'.$lang.$url : $url;

                }

            }else{


            }


        }

    }

    return parent::url($url, $full);
}


    public function link($title, $url = null, $options = array(), $confirmMessage = false) {
        $url = strtolower($url);
    	return parent::link($title, $url, $options, $confirmMessage);
    }

    public function charset($charset = null) {
    	return parent::charset($charset);
    }
}
?>
