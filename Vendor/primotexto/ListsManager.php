<?php
    require_once 'BaseManager.php';
    class ListsManager extends BaseManager {

        public static function getListsCount () {
            CredentialsHelper::ensureLogin();
            $curl = parent::getGetCurl(BaseManager::$baseURL."/lists/count");
            $result = curl_exec($curl);
            echo $result;
            curl_close($curl);
        }
        
        public static function getList ($listId) {
            CredentialsHelper::ensureLogin();
            $curl = parent::getGetCurl(BaseManager::$baseURL."/lists/$listId");
            $result = curl_exec($curl);
            echo $result;
            curl_close($curl);
        }
        
        public static function getLists () {
            CredentialsHelper::ensureLogin();
            $curl = parent::getGetCurl(BaseManager::$baseURL."/lists");
            $result = curl_exec($curl);
            foreach (json_decode($result) as $l) {
                $lists = json_encode(array ('id' => "$l->id", 'name' => "$l->name"));
                return $lists;
            }
            curl_close($curl);
        }
        
        public static function addList ($name) {
            CredentialsHelper::ensureLogin();
            $data = array ('name' => "$name");
            $curl = parent::getPostCurl(BaseManager::$baseURL.'/lists', json_encode($data));
            $result = curl_exec($curl);
            echo $result;
            curl_close($curl);
        }
        
        public static function delList ($id) {
            CredentialsHelper::ensureLogin();
            $curl = parent::getDeleteCurl(BaseManager::$baseURL."/lists/$id");
            $result = curl_exec($curl);
            return $result;
            curl_close($curl);
        }
        
        public static function getBlacklists () {
            CredentialsHelper::ensureLogin();
            $curl = parent::getGetCurl(BaseManager::$baseURL."/accounts/current");
            $result = curl_exec($curl);
            $result = json_decode($result);
            $bouncesId = $result->bounces->id;
            $unsubscribersId = $result->unsubscribers->id;
            $result = json_encode(array ('bouncesId' => $bouncesId, 'unsubscribersId' => $unsubscribersId));
            echo $result;
            curl_close($curl);
        }
        
        public static function getBlacklist ($id) {
            CredentialsHelper::ensureLogin();
            $curl = parent::getGetCurl(BaseManager::$baseURL."/blacklists/defaults/$id/contacts");
            $result = curl_exec($curl);
            echo $result;
            curl_close($curl);
        }
        
        public static function addBounce ($identifier) {
            CredentialsHelper::ensureLogin();
            $curl = parent::getGetCurl(BaseManager::$baseURL."/accounts/current");
            $result = curl_exec($curl);
            $result = json_decode($result);
            $bouncesId = $result->bounces->id;
            $bounce = array ('blacklist' => array ('id' => "$bouncesId"), 'identifier' => "$identifier");
            $curl = parent::getPostCurl(BaseManager::$baseURL.'/blacklists/default/$bouncesId/contacts', json_encode($bounce, JSON_FORCE_OBJECT));
            $result = curl_exec($curl);
            echo $result;
            curl_close($curl);
        }
        
        public static function addUnsubscriber ($identifier) {
            CredentialsHelper::ensureLogin();
            $curl = parent::getGetCurl(BaseManager::$baseURL."/accounts/current");
            $result = curl_exec($curl);
            $result = json_decode($result);
            $unsubscribersId = $result->unsubscribers->id;
            $bounce = array ('blacklist' => array ('id' => "$unsubscribersId"), 'identifier' => "$identifier");
            $curl = parent::getPostCurl(BaseManager::$baseURL.'/blacklists/default/$unsubscribersId/contacts', json_encode($bounce, JSON_FORCE_OBJECT));
            $result = curl_exec($curl);
            echo $result;
            curl_close($curl);
        }
        
        public static function delBounce ($id) {
            CredentialsHelper::ensureLogin();
            $curl = parent::getGetCurl(BaseManager::$baseURL."/accounts/current");
            $result = curl_exec($curl);
            $result = json_decode($result);
            $bouncesId = $result->bounces->id;
            $curl = parent::getDeleteCurl(BaseManager::$baseURL."/blacklists/default/$bouncesId/contacts/$id");
            $result = curl_exec($curl);
            return $result;
            curl_close($curl);
        }
        
        public static function delUnsubscriber ($id) {
            CredentialsHelper::ensureLogin();
            $curl = parent::getGetCurl(BaseManager::$baseURL."/accounts/current");
            $result = curl_exec($curl);
            $result = json_decode($result);
            $unsubscribersId = $result->unsubscribers->id;
            $curl = parent::getDeleteCurl(BaseManager::$baseURL."/blacklists/default/$unsubscribersId/contacts/$id");
            $result = curl_exec($curl);
            return $result;
            curl_close($curl);
        }
        
        private static function GetContactByNumber($contacts, $number) {
            foreach ($contacts as $c) {
                if ($c->identifier==$number)
                    return $c;
            }
            return NULL;
        }
        
        private static function GetFieldIdByList ($list, $field) {
            foreach ($list as $l) {
                if ($l->name==$field)
                    return $l;
            }
            return NULL;
        }
        
        private static function GetListIdByName ($list, $name) {
            foreach ($list as $l) {
                if ($l->name=="$name")
                   return $l;
            }
            return NULL;
        }
        
        public static function SearchIdByNumber($list, $number) {
            CredentialsHelper::ensureLogin();
            $curl = parent::getGetCurl(BaseManager::$baseURL."/lists/$list/contacts");
            $result = curl_exec($curl);
            $result = json_decode($result);
            $contact = self::GetContactByNumber($result, $number);
            if ($contact != NULL) {
                return $contact->id;
            }
            curl_close($curl);
        }
        
        public static function SearchFieldIdByFields ($list, $field) {
            CredentialsHelper::ensureLogin();
            $curl = parent::getGetCurl(BaseManager::$baseURL."/lists/$list/fields");
            $result = curl_exec($curl);
            $result = json_decode($result);
            $field = self::GetFieldIdByList($result, $field);
            if ($field != NULL) {
                return $field->id;
            }
            curl_close($curl);
        }
        
        public static function ChangeFieldByIdentifierOnListId ($list, $identifier, $field, $value) {
            CredentialsHelper::ensureLogin();
            $id = self::SearchIdByNumber($list, $identifier);
            $fid = self::SearchFieldIdByFields($list, $field);
            $curl = parent::getGetCurl(Basemanager::$baseURL."/lists/$list/contacts/$id");
            $result = curl_exec($curl);
            $result = json_decode($result);
            $result->attributes->$fid = $value;
            $curl = parent::getPutCurl(Basemanager::$baseURL."/lists/$list/contacts/$id", json_encode($result));
            $result = curl_exec($curl);
            curl_close($curl);
        }
        
        public static function ChangeFieldByIdentifierOnListName ($list, $identifier, $field, $value) {
            CredentialsHelper::ensureLogin();
            $curl = parent::getGetCurl(Basemanager::$baseURL."/lists");
            $result = curl_exec($curl);
            $result = json_decode($result);
            $list = self::GetListIdByName($result, $list);
            if ($list != NULL) {
                $list = $list->id;
                $id = self::SearchIdByNumber($list, $identifier);
                $fid = self::SearchFieldIdByFields($list, $field);
                $curl = parent::getGetCurl(Basemanager::$baseURL."/lists/$list/contacts/$id");
                $result = curl_exec($curl);
                $result = json_decode($result);
                $result->attributes->$fid = $value;
                $curl = parent::getPutCurl(Basemanager::$baseURL."/lists/$list/contacts/$id", json_encode($result));
                $result = curl_exec($curl);
                curl_close($curl);
            } else {
            echo "Nom de liste invalide !";
            }
        }
    }
?>
