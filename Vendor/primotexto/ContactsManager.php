<?php
    require_once 'BaseManager.php';
    class ContactsManager extends BaseManager {

        public static function addContact ($listId, $identifier) {
            CredentialsHelper::ensureLogin();
            $data = array ('identifier' => "$identifier", 'listId' => "$listId");
            $curl = parent::getPostCurl(BaseManager::$baseURL."/lists/$listId/contacts", json_encode($data));
            $result = curl_exec($curl);
            echo $result;
            curl_close($curl);
        }
        
        public static function delContact ($id) {
            CredentialsHelper::ensureLogin();
            $curl = parent::getDeleteCurl(BaseManager::$baseURL."/contacts/$id");
            $result = curl_exec($curl);
            return $result;
            curl_close($curl);
        }
        
        public static function getContactsCheck ($identifier, $campaignId) {
            CredentialsHelper::ensureLogin();
            $curl = parent::getGetCurl(BaseManager::$baseURL."/contacts/check?identifier=$identifier&campaignId=$campaignId");
            $result = curl_exec($curl);
            echo $result;
            curl_close($curl);
        }
        
        public static function getContactId ($listid, $id) {
            CredentialsHelper::ensureLogin();
            $curl = parent::getGetCurl(BaseManager::$baseURL."/lists/$listid/contacts/$id");
            $result = curl_exec($curl);
            echo $result;
            curl_close($curl);
        }
        
        public static function getContacts ($listid) {
            CredentialsHelper::ensureLogin();
            $curl = parent::getGetCurl(BaseManager::$baseURL."/lists/$listid/contacts");
            $result = curl_exec($curl);
            echo $result;
            curl_close($curl);
        }
        
        
        public static function getContactsCount ($listId) {
            CredentialsHelper::ensureLogin();
            $curl = parent::getGetCurl(BaseManager::$baseURL."/lists/$listId/contacts/count");
            $result = curl_exec($curl);
            echo $result;
            curl_close($curl);
        }
    }
?>
